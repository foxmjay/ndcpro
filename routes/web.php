<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Auth::routes();
Auth::routes(['register'=>false]);


//Route::view('/frontend3','/frontend3.home');
Route::get('/', 'FrontendController@index');
Route::get('/item/{id}', 'FrontendController@item');
Route::get('/list/{id}', 'FrontendController@list');
Route::get('/tags/{tag}', 'FrontendController@tags');
Route::get('/menutags/{id}', 'FrontendController@menutags');
Route::get('/contact', 'FrontendController@contact');
Route::get('/aboutus', 'FrontendController@aboutus');
Route::post ('contact/send', 'MessageController@store');
Route::get('/contact_produit', 'FrontendController@contact_produit');
Route::post('/add_produit', 'FrontendController@add_produit');
Route::get('/remove_produit/{item_id}', 'FrontendController@remove_produit');
Route::post('/contact_produit/send', 'MessageController@produit_store');
Route::get('/contact/message', 'FrontendController@contact_message');
Route::get('/mobile_cart', 'FrontendController@mobile_cart');
Route::get('/search', 'FrontendController@search');
Route::get('/{keyword}-maroc', 'FrontendController@searching');
Route::get('/{keyword}-kenitra', 'FrontendController@searching');

Route::get('/magasin-informatique', 'FrontendController@searchingCombo');
Route::get('/ordinateur-portable', 'FrontendController@searchingCombo');
Route::get('/onduleur-eaton', 'FrontendController@searchingCombo');
Route::get('/reseaux-telecommunication', 'FrontendController@searchingCombo');
Route::get('/pc-hp', 'FrontendController@searchingCombo');
Route::get('/pc-portable', 'FrontendController@searchingCombo');


//Route::get('/grandstream-maroc', 'FrontendController@avaya');
//Route::get('/alcatel-maroc', 'FrontendController@avaya');
//Route::get('/cisco-maroc', 'FrontendController@avaya');
//Route::get('/logitech-maroc', 'FrontendController@avaya');
//Route::get('/jabra-maroc', 'FrontendController@avaya');
//Route::get('/dell-maroc', 'FrontendController@avaya');
//Route::get('/promotions', 'FrontendController@promotions');




/*Route::get('/', 'FrontendController@index');
Route::get('/frontend2', 'FrontendController@index2');
Route::get('/contact', 'FrontendController@contact');
Route::get('/aboutus', 'FrontendController@aboutus');
Route::get('/onecar', 'FrontendController@onecar');
Route::get('/oneteeth', 'FrontendController@oneteeth');
Route::get('/onedoc', 'FrontendController@onedoc');
Route::get('/groups/{id}', 'FrontendController@group');
Route::get('/tags/{id}', 'FrontendController@tag');
Route::get('/items/{id}', 'FrontendController@item');*/




Route::redirect('/dashboard', '/dashboard/home');


Route::get('/dashboard/home', 'DashboardController@home')->middleware('auth');

Route::post ('dashboard/search', 'DashboardController@search')->middleware('auth');


Route::get ('dashboard/menus/{id}', 'MenuController@index')->middleware('auth');
Route::get ('dashboard/menus/{id}/create', 'MenuController@create')->middleware('auth');
Route::post ('dashboard/menus/{id}/store', 'MenuController@store')->middleware('auth');
Route::get ('dashboard/menus/{id}/edit', 'MenuController@edit')->middleware('auth');
Route::put ('dashboard/menus/{id}/update', 'MenuController@update')->middleware('auth');
Route::delete ('dashboard/menus/{id}/delete', 'MenuController@destory')->middleware('auth');


Route::get ('dashboard/groups', 'GroupController@index')->middleware('auth');
Route::get ('dashboard/groups/create', 'GroupController@create')->middleware('auth');
Route::post ('dashboard/groups/store', 'GroupController@store')->middleware('auth');
Route::get ('dashboard/groups/{id}/edit', 'GroupController@edit')->middleware('auth');
Route::put ('dashboard/groups/{id}/update', 'GroupController@update')->middleware('auth');
Route::delete ('dashboard/groups/{id}/delete', 'GroupController@destory')->middleware('auth');


Route::get ('dashboard/items/{id}', 'ItemController@index')->middleware('auth');
Route::get ('dashboard/items/{id}/create', 'ItemController@create')->middleware('auth');
Route::post ('dashboard/items/{id}/store', 'ItemController@store')->middleware('auth');
Route::get ('dashboard/items/{id}/edit', 'ItemController@edit')->middleware('auth');
Route::put ('dashboard/items/{id}/update', 'ItemController@update')->middleware('auth');
Route::delete ('dashboard/items/{id}/delete', 'ItemController@destory')->middleware('auth');


Route::get ('dashboard/banners/', 'BannerController@index')->middleware('auth');
Route::get ('dashboard/banners/create', 'BannerController@create')->middleware('auth');
Route::post ('dashboard/banners/store', 'BannerController@store')->middleware('auth');
Route::get ('dashboard/banners/{id}/edit', 'BannerController@edit')->middleware('auth');
Route::put ('dashboard/banners/{id}/update', 'BannerController@update')->middleware('auth');
Route::delete ('dashboard/banners/{id}/delete', 'BannerController@destory')->middleware('auth');


Route::get ('dashboard/tags/', 'TagController@index')->middleware('auth');
Route::get ('dashboard/tags/create', 'TagController@create')->middleware('auth');
Route::post ('dashboard/tags/store', 'TagController@store')->middleware('auth');
Route::get ('dashboard/tags/{id}/edit', 'TagController@edit')->middleware('auth');
Route::put ('dashboard/tags/{id}/update', 'TagController@update')->middleware('auth');
Route::delete ('dashboard/tags/{id}/delete', 'TagController@destory')->middleware('auth');


Route::get ('dashboard/categories/{id}', 'CategorieController@index')->middleware('auth');
Route::get ('dashboard/categories/{id}/create', 'CategorieController@create')->middleware('auth');
Route::post ('dashboard/categories/{id}/store', 'CategorieController@store')->middleware('auth');
Route::get ('dashboard/categories/{id}/edit', 'CategorieController@edit')->middleware('auth');
Route::put ('dashboard/categories/{id}/update', 'CategorieController@update')->middleware('auth');
Route::delete ('dashboard/categories/{id}/delete', 'CategorieController@destory')->middleware('auth');

Route::get ('dashboard/messages/', 'MessageController@index')->middleware('auth');

Route::get ('dashboard/messages/{id}/show', 'MessageController@show')->middleware('auth');

/*route::get('email-test',function(){
    $details['data'] = "http://newweb.ndcpromaroc.com/dashboard/messages/153/show";
    $details['email'] = ['foxmjay@gmail.com','oussamaousmoi@yahoo.fr'];
    dispatch(new App\Jobs\SendEmailJob($details));
    dd('done');
});*/

/*route::get('email-view',function(){
    $data="http://newweb.ndcpromaroc.com/dashboard/messages/166/show";
    return view('backend.email',['data' => $data]);
});*/

/*Route::get('/', function () {
    return view('welcome');
});*/
