@extends('layouts.frontend')


@section('content')

<!-- Sub banner start -->
<div class="sub-banner overview-bgi"  style="background: rgba(0, 0, 0, 0.04) url({{ URL::asset('storage/'.$group->image)}}) top left repeat !important ;">
    <div class="container">
        <div class="breadcrumb-area">
            <!--<h1>{{$item->title}}</h1> -->
            <ul class="breadcrumbs">
                <!--<li><a href="index.html">Home</a></li>
                <li class="active">Shop Details</li>-->
            </ul>
        </div>
    </div>
</div>
<!-- Sub banner end -->

<!-- Shop single start -->
<div class="shop-single content-area-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="shop-slider mb-60">
                    <div id="ProjectDetailsSlider" class="carousel Project-details-sliders slide">
                        <!-- main slider carousel items -->
                        <div class="carousel-inner">
                            @for($i=1; $i<=5 ; $i++)
                            <div class="item carousel-item {{ $i ==1 ? 'active' : ''}}" data-slide-number="{{$i-1}}">
                                @if($item['image'.$i] != null || $item['image'.$i] != "" )
                                    <img src="{{ URL::asset('storage/'.$item['image'.$i])}}" class="img-fluid" alt="shop-1">
                                @else
                                    <img src="{{ URL::asset('assets/images/noimage.png')}}" class="img-fluid" alt="shop-1">
                                @endif
                            </div>
                            @endfor

                            <a class="carousel-control left" href="#ProjectDetailsSlider" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                            <a class="carousel-control right" href="#ProjectDetailsSlider" data-slide="next"><i class="fa fa-angle-right"></i></a>
                        </div>
                        <!-- main slider carousel nav controls -->
                        <ul class="carousel-indicators smail-properties list-inline nav nav-justified">

                            @for($i=1; $i<=5 ; $i++)
    
                                <li class="list-inline-item {{ $i ==1 ? 'active' : ''}}">
                                    <a id="carousel-selector-0" class="{{ $i ==1 ? 'selected' : ''}}" data-slide-to="{{$i-1}}" data-target="#ProjectDetailsSlider">
                                        @if($item['image'.$i] != null || $item['image'.$i] != "" )
                                            <img src="{{ URL::asset('storage/'.$item['image'.$i])}}" class="img-fluid" alt="shop-1">
                                        @else
                                            <img src="{{ URL::asset('assets/images/noimage_small.png')}}" class="img-fluid" alt="shop-1">
                                        @endif
                                    </a>
                                </li>
                            @endfor
                           
                            
                        </ul>
                    </div>
                </div>
                
                <div class="shop-details-content cmn-mrg-btm">
                    <h3 class="title-2">Description</h3>

                    <p>{!!$item->description!!}</p>
                    <br>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="sidebar mb-30">
                    <div class="widget shop-info  d-none d-xl-block d-lg-block">
                        <h3>{{$item->title}}</h3>
                        <div class="reviews-box clearfix">
                            <ul class="reviews-star-list">
                                <li><i class="fa fa-star"> </i></li>
                                <li><i class="fa fa-star"> </i></li>
                                <li><i class="fa fa-star"> </i></li>
                                <li><i class="fa fa-star"> </i></li>
                                <li><i class="fa fa-star"> </i></li>
                            </ul>
                            <!--<div class="reviews">
                                <a href="#">15 Review(s)</a>
                                <a href="#">
                                    <i class="fa fa-pencil"></i>write a review
                                </a>
                            </div>-->
                        </div>
                        <!--<div class="price">
                            <del>$2100</del>$3624
                        </div>-->
                        <div class="horizontal">
                           
                        </div>
                        <p class="lead">{{$item->side_description}}</p>

                        <!-- <form>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <select class="form-control">
                                        <option>Large select</option>
                                        <option>Large select</option>
                                        <option>Large select</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-sm-5">
                                    <input class="form-control" type="text" placeholder="Quantity">
                                </div>

                                <div class="col-md-7 col-sm-7">
                                    <button class="btn btn-block btn-color btn-md">Add to cart</button>
                                </div>
                            </div>
                        </form> -->
                    </div>

                    <!-- Recent posts start 
                    <div class="widget recent-posts">
                        <h3 class="sidebar-title">Recent Posts</h3>
                        <div class="media mb-4">
                            <a class="pr-3" href="portfolio-details.html">
                                <img src="http://placehold.it/75x75" alt="recent-portfolio">
                            </a>
                            <div class="media-body align-self-center">
                                <p>Lectus tristique lacinia non in diam mauris ultricies.</p>
                                <p><i class="fa fa-calendar"></i>27 Feb 2018</p>
                            </div>
                        </div>
                        <div class="media mb-4">
                            <a class="pr-3" href="portfolio-details.html">
                                <img src="http://placehold.it/75x75" alt="recent-portfolio">
                            </a>
                            <div class="media-body align-self-center">
                                <p>Lectus tristique lacinia non in diam mauris ultricies.</p>
                                <p><i class="fa fa-calendar"></i>27 Feb 2018</p>
                            </div>
                        </div>
                        <div class="media">
                            <a class="pr-3" href="portfolio-details.html">
                                <img src="http://placehold.it/75x75" alt="recent-portfolio">
                            </a>
                            <div class="media-body align-self-center">
                                <p>Lectus tristique lacinia non in diam mauris ultricies.</p>
                                <p><i class="fa fa-calendar"></i>27 Feb 2018</p>
                            </div>
                        </div>
                    </div>-->
                    <!-- Tags start -->
                    <div class="widget tags clearfix">
                        <h3 class="sidebar-title">Tags</h3>
                        <ul class="tags">
                            @php 
                                $tags = explode(',',$item->tags);
                            @endphp
                            @foreach($tags as $tag)
                                <li><a href="#">{{$tag}}</a></li>
                            @endforeach
                            
                        </ul>
                    </div>

                </div>
            </div>
        </div>
        <br>
        <br>
        <h3 class="title-2">Produits similaire</h3>
        <div class="row">
            @foreach($randomItems as $ritem)
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="shop-box">
                    @if($ritem['image1'] != null || $ritem['image1'] != "" )
                        <img src="{{ URL::asset('storage/'.$ritem['image1'])}}"  class="img-fluid" alt="shop-3">
                    @else
                        <img src="{{ URL::asset('assets/images/noimage255.png')}}"  class="img-fluid" alt="shop-3">
                    @endif
                    <div class="shop-details">
                        <h5><a href="{{$ritem->image1}}">{{$ritem->title}}</a></h5>
                        <a class="btn btn-border btn-sm" href="/items/{{$ritem->id}}" role="button">Details</a>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</div>
<!-- Shop single end -->

@endsection

