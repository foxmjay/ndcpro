@extends('layouts.frontend')

@section('content')

<!-- Sub banner start -->
<div class="sub-banner overview-bgi" style="background: rgba(0, 0, 0, 0.04) url({{ URL::asset('assets/images/aboutus2.png')}}) top left repeat !important ;">
    <div class="container">
        <div class="breadcrumb-area">
            <!--<h1>Contact</h1>
            <ul class="breadcrumbs">
                <li><a href="index.html">Home</a></li>
                <li class="active">Contact Us</li>
            </ul>-->
        </div>
    </div>
</div>
<!-- Sub banner end -->

<div class="typography-2 content-area-12">
    <div class="container">
        
        <div class="row">
            <div class="col-md-12 sd">
                <h4 class="text-center heading"><span style="color: #FF214F"><strong>Présentation</strong></span></h4>
                <p class="text-center">
                Nous sommes une PME pour les PME ! Comment pouvons nous répondre à vos besoins en matière de téléphonie? Tout d'abord, parce que nous vous comprenons!
NDCPRO MAROC a pour but de vous assister et de vous aider à faire le meilleur choix. Nos compétences en matière de nouvelles technologies nous permettent de vous proposer le meilleur produit du marché avec la qualité du service que vous attendez.
Pour ce faire, nous travaillons en étroite collaboration avec Avaya et cisco qui ne sont autre que les leaders mondiaux en télécom. Nos compétences et les expériences positives qui émanent de la part de nos clients, nous permettent de croire."
                </p>
            </div>
        </div>
    </div>
</div>

<!-- Managment area start -->
<div class="managment-area-2 content-area-10">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-5">
                <div class="managment-info">
                    <h1><span>Notre</span> force</h1>
                    <div class="managment-border-"></div>
                    <p>Notre force est fondée sur nos ressources humaines inouïs, grâce à leur
                    professionnalisme et leur qualification nos solliciteurs deviennent
                    de plus en plus nombreux. On vous livre ci-dessus chers clients un aperçu sur
                    des activités et le travail satisfaisant que nous proposons à nos partenaires
                    actuels et potentiels.</p>
                    <ul>
                        <li><i class="flaticon-up-arrow"></i>Travaux de câblage plus de 2300 positions de travail</li>
                        <li><i class="flaticon-up-arrow"></i>Installation plus de 90 IPBX (Cisco, Avaya , Alcatel Lucent, Grandstream ..)
                                                            dont 25 des systèmes qui supporte plus de 200 utilisateurs</li>
                        <li><i class="flaticon-up-arrow"></i>Déploiement plus de 200 solution de sécurité ( vidéo-surveillance, contrôle
                                                        d’accès)</li>
                        <li><i class="flaticon-up-arrow"></i>Livraison de plus de 5000 postes téléphoniques annuellement hors mode
                                        projet ( Avaya, Cisco...)</li>
                        <li><i class="flaticon-up-arrow"></i>Déploiement plus de 900 points d’accès Wifi</li>
                        <li><i class="flaticon-up-arrow"></i>Installation plus de 700 équipements réseaux</li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-6 col-md-7 offset-lg-1">
                <div class="managment-slider">
                    <div id="carouselExampleIndicators-managment" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators-managment" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators-managment" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators-managment" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100 img-fluid" src="{{ URL::asset('assets/images/mini_sliders/slider1.png')}}" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100 img-fluid" src="{{ URL::asset('assets/images/mini_sliders/slider2.png')}}" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100 img-fluid" src="{{ URL::asset('assets/images/mini_sliders/slider1.png')}}" alt="Third slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Managment area end -->


<!-- agent start 
<div class="agent content-area-2">
    <div class="container">
        <div class="main-title">
            <h1><span>Notre</span> Equipe</h1>
            <P>Comming soon</P>
        </div>
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="agent-2">
                    <div class="agent-photo">
                        <a href="team.html">
                            <img src="http://placehold.it/255x212" alt="avatar" class="img-fluid">
                        </a>
                    </div>
                    <div class="agent-details">
                        <h5><a href="team.html">Comming soon</a></h5>
                        <p>Comming soon</p>
                        <ul class="social-list clearfix">
                            <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="agent-2">
                    <div class="agent-photo">
                        <a href="team.html">
                            <img src="http://placehold.it/255x212" alt="avatar" class="img-fluid">
                        </a>
                    </div>
                    <div class="agent-details">
                        <h5><a href="team.html">Comming soon</a></h5>
                        <p>Comming soon</p>
                        <ul class="social-list clearfix">
                            <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="agent-2">
                    <div class="agent-photo">
                        <a href="team.html">
                            <img src="http://placehold.it/255x212" alt="avatar" class="img-fluid">
                        </a>
                    </div>
                    <div class="agent-details">
                        <h5><a href="team.html">Comming soon</a></h5>
                        <p>Comming soon</p>
                        <ul class="social-list clearfix">
                            <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="agent-2">
                    <div class="agent-photo">
                        <a href="team.html">
                            <img src="http://placehold.it/255x212" alt="avatar" class="img-fluid">
                        </a>
                    </div>
                    <div class="agent-details">
                        <h5><a href="team.html">Comming soon</a></h5>
                        <p>Comming soon</p>
                        <ul class="social-list clearfix">
                            <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div >
 agent end -->

<!-- Counters start
<div class="counters overview-bgi">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="counter-box">
                    <i class="flaticon-cup"></i>
                    <h1 class="counter">967</h1>
                    <h5>Awards</h5>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="counter-box">
                    <i class="flaticon-people-1"></i>
                    <h1 class="counter">254</h1>
                    <h5>Active Member</h5>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="counter-box">
                    <i class="flaticon-people"></i>
                    <h1 class="counter">130</h1>
                    <h5>Happy Clients</h5>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="counter-box">
                    <i class="flaticon-bars"></i>
                    <h1 class="counter">94</h1>
                    <h5>Total Projects</h5>
                </div>
            </div>
        </div>
    </div>
</div>
 Counters end -->


@endsection