@extends('layouts.frontend')

@section('content')

<!-- Banner start -->
<div class="banner" id="banner">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="{{ URL::asset('assets/images/sliders/slider1.png')}}" alt="AvayaBanner">
                <div class="carousel-caption banner-slider-inner d-flex h-100 text-center">
                    <div class="carousel-content container">
                        <div class="text-c">
                            <h1 data-animation="animated fadeInDown delay-05s">Solutions adequates a votre business</h1>
                            <p data-animation="animated fadeInUp delay-10s">Nos expertises et equipes sont à votre
                                disposition.</p>
                            <a data-animation="animated fadeInUp delay-10s" href="#"
                                class="btn btn-lg btn-round btn-theme">Decouvrir</a>
                            <a data-animation="animated fadeInUp delay-10s" href="/contact"
                                class="btn btn-lg btn-round btn-white-lg-outline">Contacter nous</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{ URL::asset('assets/images/sliders/slider2.png')}}"
                    alt="GrandStreamBanner">
                <div class="carousel-caption banner-slider-inner d-flex h-100 text-center">
                    <div class="carousel-content container">
                        <div class="text-l">
                            <h1 data-animation="animated fadeInDown delay-05s">Solutions adequates a votre business</h1>
                            <p data-animation="animated fadeInUp delay-10s">Nos expertises et equipes sont à votre
                                disposition.</p>
                            <a data-animation="animated fadeInUp delay-10s" href="#"
                                class="btn btn-lg btn-round btn-theme">Decouvrir</a>
                            <a data-animation="animated fadeInUp delay-10s" href="/contact"
                                class="btn btn-lg btn-round btn-white-lg-outline">Contacter nous</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{ URL::asset('assets/images/sliders/slider5.png')}}"
                    alt="visioconference">
                <div class="carousel-caption banner-slider-inner d-flex h-100 text-center">
                    <div class="carousel-content container">
                        <div class="text-r">
                            <h1 data-animation="animated fadeInDown delay-05s">Solutions adequates a votre business</h1>
                            <p data-animation="animated fadeInUp delay-10s">Nos expertises et equipes sont à votre
                                disposition.</p>
                            <a data-animation="animated fadeInUp delay-10s" href="#"
                                class="btn btn-lg btn-round btn-theme">Decouvrir</a>
                            <a data-animation="animated fadeInUp delay-10s" href="/contact"
                                class="btn btn-lg btn-round btn-white-lg-outline">Contacter nous</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="slider-mover-left" aria-hidden="true">
                <i class="fa fa-angle-left"></i>
            </span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="slider-mover-right" aria-hidden="true">
                <i class="fa fa-angle-right"></i>
            </span>
        </a>
    </div>
</div>
<!-- banner end -->

<!-- portfolio start -->
<div class="portfolio-area content-area-2">
    <div class="container">
        <div class="main-title">
            <h1><span>Parmi nos</span> partenaires</h1>
            <ul class="list-inline-listing filters filteriz-navigation">
                <li class="btn filtr-button filtr" data-filter="all">Tout</li>
                <li data-filter="1" class="active btn btn-inline filtr-button filtr">Telecom</li>
                <li data-filter="2" class="btn btn-inline filtr-button filtr">IT</li>
                <li data-filter="3" class="btn btn-inline filtr-button filtr">Informatique</li>
            </ul>
        </div>
        <div class="row filter-portfolio">
            <div class="cars">

                @foreach($banners as $banner)
                <div class="col-lg-4 col-md-4 col-sm-6 filtr-item" data-category="3, 2, 1">
                    <div class="property-box mb-30">
                        <div class="property-thumbnail">
                            <a href="#" class="property-img">
                                <img src="{{ URL::asset('storage/'.$banner->image)}}" alt="voip" class="img-fluid">
                            </a>
                            <div class="property-overlay">

                                <a href="{{ $banner->type == 'Link' ? $banner->url : '/groups/'.$banner->group_id}}"
                                    class="overlay-link">
                                    <i class="fa fa-link"></i>
                                </a>

                                <div class="property-magnify-gallery">

                                    <a href="" class="hidden"></a>
                                    <a href="" class="hidden"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach


            </div>
        </div>
    </div>
</div>
<!-- portfolio end -->


<!-- Managment area start -->
<div class="managment-area-2 content-area-10">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-5">
                <div class="managment-info">
                    <h1><span>Notre</span> force</h1>
                    <div class="managment-border-"></div>
                    <p>Notre force est fondée sur nos ressources humaines inouïs, grâce à leur
                        professionnalisme et leur qualification nos solliciteurs deviennent
                        de plus en plus nombreux. On vous livre ci-dessus chers clients un aperçu sur
                        des activités et le travail satisfaisant que nous proposons à nos partenaires
                        actuels et potentiels.</p>
                    <ul>
                        <li><i class="flaticon-up-arrow"></i>Travaux de câblage plus de 2300 positions de travail</li>
                        <li><i class="flaticon-up-arrow"></i>Installation plus de 90 IPBX (Cisco, Avaya , Alcatel
                            Lucent, Grandstream ..)
                            dont 25 des systèmes qui supporte plus de 200 utilisateurs</li>
                        <li><i class="flaticon-up-arrow"></i>Déploiement plus de 200 solution de sécurité (
                            vidéo-surveillance, contrôle
                            d’accès)</li>
                        <li><i class="flaticon-up-arrow"></i>Livraison de plus de 5000 postes téléphoniques annuellement
                            hors mode
                            projet ( Avaya, Cisco...)</li>
                        <li><i class="flaticon-up-arrow"></i>Déploiement plus de 900 points d’accès Wifi</li>
                        <li><i class="flaticon-up-arrow"></i>Installation plus de 700 équipements réseaux</li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-6 col-md-7 offset-lg-1">
                <div class="managment-slider">
                    <div id="carouselExampleIndicators-managment" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators-managment" data-slide-to="0" class="active">
                            </li>
                            <li data-target="#carouselExampleIndicators-managment" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators-managment" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100 img-fluid"
                                    src="{{ URL::asset('assets/images/mini_sliders/slider1.png')}}" alt="avaya">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100 img-fluid"
                                    src="{{ URL::asset('assets/images/mini_sliders/slider2.png')}}" alt="grandstream">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100 img-fluid"
                                    src="{{ URL::asset('assets/images/mini_sliders/slider1.png')}}"
                                    alt="visioconférence">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                            data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                            data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Managment area end -->


<!-- Testimonial 1 start
<div class="testimonial-1 overview-bgi">
    <div class="container">
        <div class="row">
            <div class="offset-lg-2 col-lg-8">
                <div class="testimonial-inner">
                    <header class="testimonia-header">
                        <h1><span>Nos</span> Articles</h1>
                    </header>
                    <div id="carouselExampleIndicators7" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="row">
                                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <div class="avatar">
                                            <img src="http://placehold.it/160x160" alt="avatar-2" class="img-fluid rounded">
                                        </div>
                                    </div>
                                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                        <p class="lead">
                                                Comming soon.
                                        </p>
                                        <div class="author-name">
                                                Comming soon
                                        </div>
                                        <ul class="rating">
                                            <li>
                                                <i class="fa fa-star"></i>
                                            </li>
                                            <li>
                                                <i class="fa fa-star"></i>
                                            </li>
                                            <li>
                                                <i class="fa fa-star"></i>
                                            </li>
                                            <li>
                                                <i class="fa fa-star"></i>
                                            </li>
                                            <li>
                                                <i class="fa fa-star-half-full"></i>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <div class="avatar">
                                            <img src="http://placehold.it/160x160" alt="avatar" class="img-fluid rounded">
                                        </div>
                                    </div>
                                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                        <p class="lead">
                                                Comming soon.
                                        </p>
                                        <div class="author-name">
                                                Comming soon
                                        </div>
                                        <ul class="rating">
                                            <li>
                                                <i class="fa fa-star"></i>
                                            </li>
                                            <li>
                                                <i class="fa fa-star"></i>
                                            </li>
                                            <li>
                                                <i class="fa fa-star"></i>
                                            </li>
                                            <li>
                                                <i class="fa fa-star"></i>
                                            </li>
                                            <li>
                                                <i class="fa fa-star-half-full"></i>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <div class="avatar">
                                            <img src="http://placehold.it/160x160" alt="avatar-3" class="img-fluid rounded">
                                        </div>
                                    </div>
                                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                        <p class="lead">
                                                Comming soon.
                                        </p>
                                        <div class="author-name">
                                                Comming soon
                                        </div>
                                        <ul class="rating">
                                            <li>
                                                <i class="fa fa-star"></i>
                                            </li>
                                            <li>
                                                <i class="fa fa-star"></i>
                                            </li>
                                            <li>
                                                <i class="fa fa-star"></i>
                                            </li>
                                            <li>
                                                <i class="fa fa-star"></i>
                                            </li>
                                            <li>
                                                <i class="fa fa-star-half-full"></i>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a class="carousel-control-prev" href="#carouselExampleIndicators7" role="button" data-slide="prev">
                            <span class="slider-mover-left" aria-hidden="true">
                                <i class="fa fa-angle-left"></i>
                            </span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators7" role="button" data-slide="next">
                            <span class="slider-mover-right" aria-hidden="true">
                                <i class="fa fa-angle-right"></i>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 Testimonial 1 end -->

<!-- services start -->
<div class="services content-area-2 bg-grea">
    <div class="container">
        <div class="main-title">
            <h1><span>Nos</span> Produits</h1>
            <!--<p>Comming soon.</p> -->
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="media services-info">
                    <i class="flaticon-up-arrow"></i>
                    <div class="media-body">
                        <h5>Courant faible</h5>
                        <ul style="list-style-type: circle;"></ul>
                        <li>Electricité</li>
                        <li>Fibre Optique</li>
                        <li>Pré-câblage</li>
                        <li>Câblage informatique</li>
                        <li>Electricité</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="media services-info">
                    <i class="flaticon-commerce"></i>
                    <div class="media-body">
                        <h5>Informatique</h5>
                        <ul style="list-style-type: circle;"></ul>
                        <li>Routage</li>
                        <li>Point d’accés</li>
                        <li>Switching</li>
                        <li>Sécurité réseaux</li>
                        <li>Sauvegarde de donnés</li>
                        <li>Matériel Ordinateurs</li>
                        <li>Imprimantes</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="media services-info">
                    <i class="flaticon-graphic"></i>
                    <div class="media-body">
                        <h5>Téléphonie</h5>
                        <ul style="list-style-type: circle;"></ul>
                        <li>Analogique</li>
                        <li>Numérique</li>
                        <li>Hybride</li>
                        <li>Terminaux</li>
                        <li>Taxation</li>
                        <li>Messagerie vocale/ SVI</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="media services-info">
                    <i class="flaticon-social"></i>
                    <div class="media-body">
                        <h5 class="mt-0">Sécurite électronique</h5>
                        <ul style="list-style-type: circle;"></ul>
                        <li>Onduleur</li>
                        <li>Contrôle d’accès</li>
                        <li>Système d’alarme</li>
                        <li>Caméra de surveillance</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="media services-info">
                    <i class="flaticon-connection-1"></i>
                    <div class="media-body">
                        <h5>Audio visuel</h5>
                        <ul style="list-style-type: circle;"></ul>
                        <li>Sonorisation</li>
                        <li>Visio conférence</li>
                        <li>Projection & accessoires</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="media services-info">
                    <i class="flaticon-commerce-1"></i>
                    <div class="media-body">
                        <h5>Développement</h5>
                        <ul style="list-style-type: circle;"></ul>
                        <li>CRM</li>
                        <li>ERP</li>
                        <li>Application Mobile</li>
                        <li>Site web</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- services end -->


<!-- partner start -->
<div class="container partner">
    <div class="main-title">
        <h1><span>Nos</span> Références</h1>

    </div>
    <div class="row">
        <div class="multi-carousel" data-items="1,3,5,6" data-slide="1" id="multiCarousel" data-interval="1000">
            <div class="multi-carousel-inner">
                <div class="item">
                    <div class="pad15">
                        <img src="{{ URL::asset('assets/images/references/anapec.png')}}" alt="anapec">
                    </div>
                </div>
                <div class="item">
                    <div class="pad15">
                        <img src="{{ URL::asset('assets/images/references/webhelp.png')}}" alt="webhelp">
                    </div>
                </div>
                <div class="item">
                    <div class="pad15">
                        <img src="{{ URL::asset('assets/images/references/sgmb.png')}}" alt="sgmb">
                    </div>
                </div>
                <div class="item">
                    <div class="pad15">
                        <img src="{{ URL::asset('assets/images/references/smtl.png')}}" alt="smtl">
                    </div>
                </div>
                <div class="item">
                    <div class="pad15">
                        <img src="{{ URL::asset('assets/images/references/dxc.png')}}" alt="dxc">
                    </div>
                </div>
                <div class="item">
                    <div class="pad15">
                        <img src="{{ URL::asset('assets/images/references/gs.png')}}" alt="gs">
                    </div>
                </div>
                <div class="item">
                    <div class="pad15">
                        <img src="{{ URL::asset('assets/images/references/chaabi.png')}}" alt="chaabi">
                    </div>
                </div>
                <div class="item">
                    <div class="pad15">
                        <img src="{{ URL::asset('assets/images/references/casaamengament.png')}}" alt="casaamengament">
                    </div>
                </div>
                <div class="item">
                    <div class="pad15">
                        <img src="{{ URL::asset('assets/images/references/axa.png')}}" alt="axa">
                    </div>
                </div>
                <div class="item">
                    <div class="pad15">
                        <img src="{{ URL::asset('assets/images/references/ald.png')}}" alt="ald">
                    </div>
                </div>
                <div class="item">
                    <div class="pad15">
                        <img src="{{ URL::asset('assets/images/references/copisa1.png')}}" alt="copisa1">
                    </div>
                </div>
                <div class="item">
                    <div class="pad15">
                        <img src="{{ URL::asset('assets/images/references/pr.png')}}" alt="pr">
                    </div>
                </div>


            </div>
            <a class="multi-carousel-indicator leftLst" aria-hidden="true">
                <i class="fa fa-angle-left"></i>
            </a>
            <a class="multi-carousel-indicator rightLst" aria-hidden="true">
                <i class="fa fa-angle-right"></i>
            </a>
        </div>
    </div>
</div>
<!-- partner end -->

<!-- intro section start -->
<div class="intro-section">
    <div class="container">
        <div class="row">
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12">
                <img src="{{ URL::asset('assets/images/mini_logo.png')}}" alt="NDCPRO MAROC">
            </div>
            <div class="col-xl-8 col-lg-7 col-md-7 col-sm-12">
                <div class="intro-text">
                    <h3>vous avez besoin de plus d'informations?</h3>
                </div>
            </div>
            <div class="col-xl-2 col-lg-3 col-md-3 col-sm-12">
                <a href="/contact" class="btn btn-md">Contacter nous</a>
            </div>
        </div>
    </div>
</div>
<!-- intro section end -->

@endsection
