@extends('layouts.frontend')

@section('content')

<!-- Sub banner start -->
<div class="sub-banner overview-bgi" style="background: rgba(0, 0, 0, 0.04) url({{ URL::asset('assets/images/contact.png')}}) top left repeat !important ;">
    <div class="container">
        <div class="breadcrumb-area">
            <!--<h1>Contact</h1>
            <ul class="breadcrumbs">
                <li><a href="index.html">Home</a></li>
                <li class="active">Contact Us</li>
            </ul>-->
        </div>
    </div>
</div>
<!-- Sub banner end -->

<!-- Contact 1 start -->
<div class="contact-1 content-area-7">
    <div class="container">
        <div class="main-title">
            <h1><span>Contacter</span> nous</h1>
            <!--<p>We waited until we could do it right. Then we did! Instead of creating a carbon copy.</p>-->
        </div>
        <div class="row">
            <div class="col-lg-7 col-md-7 col-md-7">
                <form action="#" method="GET" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group name">
                                <input type="text" name="name" class="form-control" placeholder="Nom">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group email">
                                <input type="email" name="email" class="form-control" placeholder="Email">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group subject">
                                <input type="text" name="subject" class="form-control" placeholder="Subjet">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group number">
                                <input type="text" name="phone" class="form-control" placeholder="Numero">
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group message">
                                <textarea class="form-control" name="message" placeholder="Message"></textarea>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                            <div class="send-btn">
                                <button type="submit" class="btn btn-color btn-md btn-message">Envoyer</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class=" offset-lg-1 col-lg-4 offset-md-0 col-md-5">
                <div class="contact-info">
                    <h3>Infos</h3>
                    <div class="media">
                        <i class="fa fa-map-marker"></i>
                        <div class="media-body">
                            <h5 class="mt-0">Address</h5>
                            <p>étage,appt 5 29 Rue Augustin Sourzac Casablanca 20250</p>
                        </div>
                    </div>
                    <div class="media">
                        <i class="fa fa-phone"></i>
                        <div class="media-body">
                            <h5 class="mt-0">Telephone</h5>
                            <p>Tel<a href="tel:0477-0477-8556-552">:(+212) 522 608 892 </a> </p>
                            <p>Fax<a href="tel:+0477-85x6-552">: (+212) 522 600 695 </a> </p>
                        </div>
                    </div>
                    <div class="media mrg-btn-0">
                        <i class="fa fa-envelope"></i>
                        <div class="media-body">
                            <h5 class="mt-0">Email</h5>
                            <p><a href="#">contact@ndcpromaroc.com</a></p>
                            <!--<p><a href="#">http://themevessel.com</a></p>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Contact 1 end -->

<!-- Google map start -->
<div class="section">
    <div class="map">
        <div id="contactMap" class="contact-map" >
                <iframe style="width: 100%; height: 350px;"  src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3323.1433452660394!2d-7.581462000000001!3d33.601585!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xda7cd0f030a6fab%3A0x913997df0a4a4a1c!2s%C3%A9tage%2Cappt%205%2C%2029%20Rue%20Augustin%20Sourzac%2C%20Casablanca%2020250!5e0!3m2!1sfr!2sma!4v1581527383528!5m2!1sfr!2sma"  frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
    </div>
</div>
<!-- Google map end -->


@endsection