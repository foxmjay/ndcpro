@extends('layouts.frontend')

@section('content')

@isset($group)
<!-- Sub banner start -->
<div class="sub-banner overview-bgi" style="background: rgba(0, 0, 0, 0.04) url({{ URL::asset('storage/'.$group->image)}}) top left repeat !important ;" >
    <div class="container">
        <div class="breadcrumb-area">
            <!--<h1>{{$group->title}}</h1> -->
            <ul class="breadcrumbs">
                <!--<li><a href="index.html"></a></li>
                <li class="active">Shop List</li>-->
            </ul>
        </div>
    </div>
</div>
<!-- Sub banner end -->

@endisset

@isset($menu)
<!-- Sub banner start -->
<div class="sub-banner overview-bgi" style="background: rgba(0, 0, 0, 0.04) url({{ URL::asset('assets/images/nocover.png')}}) top left repeat !important ;" >
    <div class="container">
        <div class="breadcrumb-area">
            <!--<h1>{{--$group->title--}}</h1> -->
            <ul class="breadcrumbs">
                <!--<li><a href="index.html"></a></li>
                <li class="active">Shop List</li>-->
            </ul>
        </div>
    </div>
</div>
<!-- Sub banner end -->

@endisset

<!-- Shop list start -->
<div class="shop-list content-area-2">
    <div class="container">
        <div class="main-title">
            @isset($group)
                <h1><span>{{$group->title}}</span></h1>
                <p>{{$group->description}}</p>
            @endisset

            @isset($menu)
                <h1><span>{{$menu->title}}</span></h1>
                <p>{{$menu->description}}</p>
            @endisset


        </div>
        <div class="row">
            @foreach($items as $item)
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="shop-box">
                        @if($item->image1 != null || $item->image1 != "" )
                            <img src="{{ URL::asset('storage/'.$item->image1)}}" class="img-fluid" alt="shop-3">
                        @else
                            <img src="{{ URL::asset('assets/images/noimage255.png')}}"  class="img-fluid" alt="shop-3">
                        @endif
                        <div class="shop-details">
                            <!--<h6>{{$item->title}}</h6> -->
                            <h5><a href="/items/{{$item->id}}">{{$item->title}}</a></h5>
                            <a class="btn btn-border btn-sm" href="/items/{{$item->id}}" role="button">Details</a>
                        </div>
                    </div>
                </div>
            @endforeach
            
        </div>
    </div>
</div>
<!-- Shop list end -->

@endsection