@extends('layouts.frontend')

@section('content')

<!-- Sub banner start -->
<div class="sub-banner overview-bgi" style="background: rgba(0, 0, 0, 0.04) url({{ URL::asset('assets/images/apps/oneteeth/oneteeth.png')}}) top left repeat !important ;">
    <div class="container">
        <div class="breadcrumb-area">
            <!--<h1>Contact</h1>
            <ul class="breadcrumbs">
                <li><a href="index.html">Home</a></li>
                <li class="active">Contact Us</li>
            </ul>-->
        </div>
    </div>
</div>
<!-- Sub banner end -->


<!-- Managment area start -->
<div class="managment-area-2 content-area-10">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-5">
                <div class="managment-info">
                    <h1><span>ONE</span>Teeth</h1>
                    <div class="managment-border-"></div>
                    <a class="btn btn-sm btn-theme btn-round signup-link" href="/contact">Contactez Nous</a>

                </div>
            </div>
            <div class="col-lg-6 col-md-7 offset-lg-1">
                <div class="managment-slider">
                    <div id="carouselExampleIndicators-managment" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators-managment" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators-managment" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators-managment" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100 img-fluid" src="{{ URL::asset('assets/images/apps/oneteeth/oneteeth1.png')}}" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100 img-fluid" src="{{ URL::asset('assets/images/apps/oneteeth/oneteeth2.png')}}" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100 img-fluid" src="{{ URL::asset('assets/images/apps/oneteeth/oneteeth3.png')}}" alt="Third slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Managment area end -->




@endsection