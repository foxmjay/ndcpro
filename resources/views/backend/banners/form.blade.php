
              <div class="box-body">

                <div class="col-md-12">
                  <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="title" value="{{ old('title', optional($banner)->title) }}" required>
                    {!! $errors->first('title', '<p class="text-danger ">:message</p>') !!}
                  </div>

                  <div class="form-group" >
                    <label for="url">Description</label>
                    <textarea  class="form-control" id="description" name="description" >{{ old('description', optional($banner)->description) }}</textarea>
                  </div>

                  <div class="form-group" >
                    <label for="pays_id">Type</label>
                    @if (!empty($banner))
                      <select class="form-control" name="type" id="type">
                      @foreach (['Link', 'Group'] as $object)
                        @if ($object === $banner->type)
                        <option value="{{$object}}" selected>{{$object}}</option>
                        @else
                        <option value="{{$object}}">{{$object}}</option>
                        @endif
                      @endforeach
                      </select>
                    @else
                      <select class="form-control" name="type" id="type">
                      @foreach (['Link', 'Group'] as $object)
                        <option value="{{$object}}">{{$object}}</option>
                      @endforeach
                      </select>
                    @endif
                  </div>


                  <div class="form-group" id="url_block">
                    <label for="url">URL</label>
                    <input type="text" class="form-control" id="url" name="url" placeholder="url" value="{{ old('url', optional($banner)->url) }}">
                  </div>

                  <div class="form-group" id="group_block">
                    <label for="pays_id">Group</label>
                    @if (!empty($banner))
                      <select class="form-control" name="group_id" id="group_id">
                      @foreach ($groups as $object)
                        @if ($object->id === $banner->group_id)
                        <option value="{{$object->id}}" selected>{{$object->title}}</option>
                        @else
                        <option value="{{$object->id}}">{{$object->title}}</option>
                        @endif
                      @endforeach
                      </select>
                    @else
                      <select class="form-control" name="group_id" id="group_id">
                      @foreach ($groups as $object)
                        <option value="{{$object->id}}">{{$object->title}}</option>
                      @endforeach
                      </select>
                    @endif
                  </div>


                  <div class="form-group ">
                      <label for="image" >Image</label>
                      <input name="image" type="file" id="image" value=""  class="imagefile">
                      @if(!empty($banner->image))
                      <a  href="{{ URL::asset(  'storage/'.$banner->image  )}}" target="_blank"><img height="50px" src="{{ URL::asset('storage/'.$banner->image  ) }}" alt=""/></a>    
                      @endif
                  </div>

                  <div class="input-group margin">
                      <div class="input-group-btn">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Tags
                          <span class="fa fa-caret-down"></span></button>
                        <ul class="dropdown-menu">
                            @if (!empty($tags))
                              @foreach ($tags as $tag)
                              <li><a href="javascript:void(0);" class="addTag" tagid="{{$tag->id}}" tag-labelid="{{$tag->display}}_{{$tag->id}}">{{$tag->display}}</a></li>
                                @endforeach
                            @endif
                        </ul>
                      </div>
                      <!-- /btn-group -->
                      <div class="col-md-11">
                          <ul id="mytags" class="primary">
                          </ul>
                          <input type="hidden"  name="tags" id="tags" value="">
                      </div>
                    </div>
                 
                
                </div>
            
              </div>

          