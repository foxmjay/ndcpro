@extends('layouts.admin')

@section('css')

<meta name="csrf-token" content="{{ csrf_token() }}">

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="contact-vue-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Banners
        <small>gestion des banners</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Banners</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Modification banner</h3>
            </div>

            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="POST" action="{{ url('/dashboard/banners/'.$banner->id.'/update') }}" accept-charset="UTF-8" id="mainForm" name="mainForm" class="form-sample" enctype="multipart/form-data"> 
              <input name="_method" type="hidden" value="PUT">
             {{ csrf_field() }}

                @include ('backend.banners.form', ['banner' => $banner,])
            
              <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right" onSubmit="enableSpinner();">Save</button>
              </div>

            </form>
            
          </div>
          <!-- /.box -->
            
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->


@endsection

@section('js')



<script>


      $('.imagefile').on('change', function() {
            if(this.files[0].size > 1048576){
              alert("File is too big!");
              this.value = "";
            };
      });


    if($('#type').val() == "Link"){
      $('#group_block').hide();
      $('#url_block').show();
    }

    if($('#type').val() == "Group"){
          $('#group_block').show();
          $('#url_block').hide();
     }



     $('#type').change(function(){
        console.log($(this).val());
        if($(this).val() == "Link"){

          $('#group_block').hide();
          $('#url_block').show();

        }

        if($(this).val() == "Group"){
          $('#group_block').show();
          $('#url_block').hide();
        }
        
      });

      var tagids=[];
      $(document).ready(function() {

          function findTagid(tagid_str){
              console.log(this);
              if(tagid_str == this) return true;
              else return false;
          }
        
          $("#mytags").tagit({
          singleField: true,
          //singleFieldNode: $('#tags'),
          beforeTagRemoved: function(event, ui) {
            index = tagids.findIndex(findTagid,ui.tagLabelId)
            tagids.splice(index,1);
          }
          
          });

          <?php 
          $tags =\App\Tag::getTags($banner->tags);
          $tagids = array();
          $tagids = explode(';',$banner->tags);
          $tagsNoEmpty=[];
          foreach($tagids as $t){
            if($t != "")
                array_push($tagsNoEmpty,$t);
          }

          ?>
          
          var tags = {!! json_encode($tags) !!};
          tagids = {!! json_encode($tagsNoEmpty) !!};

          console.log(tagids);
          for(var i=0;i<tags.length;i++){
          $("#mytags").tagit("createTag", tags[i]);
        }


          $('.addTag').on('click', function(ret){
            
            tagids.push($(this).attr('tagid'));
            $("#mytags").tagit("createTag", $(this).attr('tag-labelid'));
            //$("#tags").text(tags+";"+)
          });

          $('#mainForm').submit(function(event) {

                event.preventDefault(); //this will prevent the default submit
                //console.log('okok');

                $('#tags').val(tagids.join(';'));

                $(this).unbind('submit').submit(); // continue the submit unbind preventDefault

            })
      });

 
</script> 


@endsection
