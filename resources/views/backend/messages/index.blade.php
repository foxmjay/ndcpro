@extends('layouts.admin')

@section('css')
<!--<link rel="stylesheet" href="{{ URL::asset('assets/admin/admin2/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">-->
<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Menus
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Menus</li>
      </ol>
    </section>


<section >
    <br>
    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif
</section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
   
            </div>
           
            <!-- /.box-header -->
            <div class="box-body table-responsive">

              @if(count($messages) > 0)
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>Nom & Prenom</th>
                      <th>Sujet</th>
                      <th>Type</th>
                      <th>Paiement</th>
                      <th>Date</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($messages as $message)
                        <tr style="{{ (Illuminate\Support\Str::contains($message->tag,'viewed') )? 'font-weight: normal' :'font-weight: bold' }}">
                          <td>{{ $message->nom }} {{ $message->prenom }}</td>
                          <td>{{ $message->sujet }}</td>
                          <td>{{ $message->type }}</td>
                          <td>{{ ($message->type != null)? $message->payment : '' }}</td>
                          <td>{{ $message->created_at}}</td>
                          <td>
                             
                                <div class="btn-group btn-group-sm" role="group">
                                    <a href="{{ url('/dashboard/messages/' . $message->id . '/show') }}" class="btn btn-info" title="Show">
                                        <span class="fa fa-eye" aria-hidden="true"></span>
                                    </a>
                                </div>

                          </td>
                        </tr>
                        @endforeach

                    </tbody>
                   
                  </table>
              @endif
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection

@section('js')

  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script>
    $(function() {
      $('#example1').DataTable({
        searching: true,
        "order": [[ 4, "desc" ]]

      })



    })
  </script>
@endsection
