@extends('layouts.admin')

@section('css')

<meta name="csrf-token" content="{{ csrf_token() }}">

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="contact-vue-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <h1>
        Messages
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Messages</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
          <div class="col-md-12">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#activity" data-toggle="tab">Messages</a></li>

                </ul>
   
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    <!-- Post -->
                    <div class="post">
                      <div class="user-block">
                        <img class="img-circle img-bordered-sm" src="{{ URL::asset('assets/admin/dist/img/user.png')}}" alt="user image">
                            <span class="username">
                              <a href="#">{{$message->nom}} {{$message->nom}}</a>
                            </span>
                        <span class="description">{{$message->created_at}}</span>
                      </div>
                      <!-- /.user-block -->
                      <p><b>Tel :</b> {{$message->tel}}</p>
                      <p><b>Email : </b>{{$message->email}}</p>

                      <p><b>Sujet : </b> {{$message->sujet}}</p>
                      <p>
                        <b>Message : </b> {{$message->message}}
                      </p>
                      <p>
                        <b>Type de Paimenet : </b> {{($message->type != null)? $message->payment : ''}}
                      </p>

                      @if( !empty($itemMsgs) )
                        <p>
                            <b>Produit :</b>
                        </p>
                        @foreach($itemMsgs as $itemMsg)
                          <p>
                              <a href="/item/{{$itemMsg->item->id}}" target="_blank" >{{ $itemMsg->item->title}}</a> x {{ $itemMsg->quantity}}
                          </p>
                        @endforeach
                      @endif

                         
                    </div>
                    <!-- /.post -->
        
                  </div>

                  <a href="/dashboard/messages" class="btn btn-warning ">
                      <i class="fa fa-mail-reply"></i> Retour
                  </a>
                  
                </div>
                
                <!-- /.tab-content -->
              </div>
              <!-- /.nav-tabs-custom -->
            </div>
            <!-- /.col -->
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

@endsection

@section('js')



<script>

    if($('#type').val() == "Link"){
      $('#group_block').hide();
      $('#tag_block').hide();
      $('#url_block').show();
    }

    if($('#type').val() == "Group"){
          $('#group_block').show();
          $('#url_block').hide();
          $('#tag_block').hide();
     }


     if($('#type').val() == "Tag"){
          $('#group_block').hide();
          $('#url_block').hide();
          $('#tag_block').hide();
     }


     $('#type').change(function(){
        console.log($(this).val());
        if($(this).val() == "Link"){

        $('#group_block').hide();
        $('#url_block').show();
        $('#tag_block').hide();


        }

        if($(this).val() == "Group"){
        $('#group_block').show();
        $('#url_block').hide();
        $('#tag_block').hide();

        }


        if($(this).val() == "Tag"){
        $('#group_block').hide();
        $('#url_block').hide();
        $('#tag_block').hide();
        }
        
      });

 
</script> 


@endsection
