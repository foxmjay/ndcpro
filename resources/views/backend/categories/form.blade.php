
              <div class="box-body">

                <div class="col-md-12">
                  <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="title" value="{{ old('title', optional($categorie)->title) }}" required>
                    {!! $errors->first('title', '<p class="text-danger ">:message</p>') !!}
                  </div>

                  <!--<div class="form-group" >
                    <label for="pays_id">Type</label>
                    @if (!empty($categorie))
                      <select class="form-control" name="type" id="type">
                      @foreach (['Link', 'Group','Tag'] as $object)
                        @if ($object === $categorie->type)
                        <option value="{{$object}}" selected>{{$object}}</option>
                        @else
                        <option value="{{$object}}">{{$object}}</option>
                        @endif
                      @endforeach
                      </select>
                    @else
                      <select class="form-control" name="type" id="type">
                      @foreach (['Link', 'Group', 'Tag'] as $object)
                        <option value="{{$object}}">{{$object}}</option>
                      @endforeach
                      </select>
                    @endif
                  </div> -->


                  <div class="form-group" id="url_block">
                    <label for="url">URL</label>
                    <input type="text" class="form-control" id="url" name="url" placeholder="url" value="{{ old('url', optional($categorie)->url) }}">
                  </div>

                  <div class="form-group" id="group_block">
                    <label for="tag">Tag</label>
                    @if (!empty($tags))
                      <select class="form-control" name="tag" id="tag">
                      @foreach ($tags as $object)
                        @if ($object->id == $categorie->tag)
                        <option value="{{$object->id}}" selected>{{$object->display}}</option>
                        @else
                        <option value="{{$object->id}}">{{$object->display}}</option>
                        @endif
                      @endforeach
                      </select>
                    @else
                      <select class="form-control" name="tag" id="tag">
                      @foreach ($tags as $object)
                        <option value="{{$object->id}}">{{$object->display}}</option>
                      @endforeach
                      </select>
                    @endif
                  </div>
                  
                  <div class="form-group ">
                      <label for="image_small" >Image small</label>
                      <input name="image_small" type="file" id="image_small" value=""  class="imagefile">
                      @if(!empty($categorie->image_small))
                      <a  href="{{ URL::asset(  'storage/'.$categorie->image_small  )}}" target="_blank"><img height="50px" src="{{ URL::asset('storage/'.$categorie->image_small  ) }}" alt=""/></a>    
                      @endif
                  </div>

                  <div class="form-group ">
                      <label for="image_big" >Image big </label>
                      <input name="image_big" type="file" id="image_big" value=""  class="imagefile">
                      @if(!empty($categorie->image_big))
                      <a  href="{{ URL::asset(  'storage/'.$categorie->image_big  )}}" target="_blank"><img height="50px" src="{{ URL::asset('storage/'.$categorie->image_big  ) }}" alt=""/></a>    
                      @endif
                  </div>
                
                
                </div>
            
              </div>

          