@extends('layouts.admin')

@section('css')

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Categories
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Categories</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Creation Categorie</h3>
            </div>

            <!-- /.box-header -->
            <!-- form start -->

            <form role="form" method="POST" action="{{ url('/dashboard/categories/'.$parent.'/store') }}" accept-charset="UTF-8" id="create_client_form" name="create_form" class="form-sample" enctype="multipart/form-data">
             {{ csrf_field() }}

                @include ('backend.categories.form', ['categorie' => null,])

              <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right" onSubmit="enableSpinner();">Add</button>
              </div>

            </form>
            
          </div>
          <!-- /.box -->
            
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection

@section('js')



<script>


    $('.imagefile').on('change', function() {
            if(this.files[0].size > 1048576){
              alert("File is too big!");
              this.value = "";
            };
      });


    if($('#type').val() == "Link"){
      $('#group_block').hide();
      $('#tag_block').hide();
      $('#url_block').show();
    }

    if($('#type').val() == "Group"){
          $('#group_block').show();
          $('#url_block').hide();
          $('#tag_block').hide();
     }


     if($('#type').val() == "Tag"){
          $('#group_block').hide();
          $('#url_block').hide();
          $('#tag_block').hide();
     }

     $('#type').change(function(){
        //console.log($(this).val());

        if($(this).val() == "Link"){

          $('#group_block').hide();
          $('#url_block').show();
          $('#url_block').hide();


        }

        if($(this).val() == "Group"){
          $('#group_block').show();
          $('#url_block').hide();
          $('#url_block').hide();

        }


        if($(this).val() == "Tag"){
          $('#group_block').hide();
          $('#url_block').hide();
          $('#tag_block').hide();
        }
        
      });

 
</script> 

@endsection
