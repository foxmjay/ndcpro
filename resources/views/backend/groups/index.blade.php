@extends('layouts.admin')

@section('css')
<!--<link rel="stylesheet" href="{{ URL::asset('assets/admin/admin2/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">-->
<link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Groups
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Groups</li>
      </ol>
    </section>


<section >
    <br>
    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif
</section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
             
              <a href="{{ url('dashboard/groups/create') }}" class="btn btn-success ">
                <i class="fa fa-plus"></i> Add
              </a>

            </div>
           
            <!-- /.box-header -->
            <div class="box-body table-responsive">

              @if(count($groups) > 0)
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>Title</th>
                      <th>Description</th>
                      <th>Image</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($groups as $group)
                        <tr>
                          <td>{{ $group->title }}</td>
                          <td>{{ $group->description }}</td>
                          <td><img src="{{ URL::asset('storage/'.$group->image)}}" width="50" alt="No Image"></td>

                          <td>

                              <form method="POST" action="{{ url('/dashboard/groups/'.$group->id.'/delete') }}" accept-charset="UTF-8">
                              <input name="_method" value="DELETE" type="hidden">
                              {{ csrf_field() }}
                                <div class="btn-group btn-group-sm" role="group">

                                    <a href="{{ url('/dashboard/groups/' . $group->id . '/edit') }}" class="btn btn-info" title="Edit">
                                        <span class="fa fa-edit" aria-hidden="true"></span>
                                    </a>
                                    <a href="{{ url('/dashboard/items/'. $group->id)}}" title="Sub menu" type="button" class="btn btn-success">
                                          <span class="fa fa-align-center" aria-hidden="true"></span>
                                      </a>

                                      <button type="submit" class="btn btn-danger" title="Supprimer" onclick="return confirm(&quot;Are you sure you want to delete?, all Items in this group will be deleted !&quot;)">
                                        <span class="fa fa-trash" aria-hidden="true"></span>
                                     </button>


                                </div>
                              </form>

                          </td>
                        </tr>
                        @endforeach

                    </tbody>
                   
                  </table>
              @endif
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  </div>
  <!-- /.content-wrapper -->

@endsection

@section('js')

  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script>
    $(function() {
      $('#example1').DataTable({
        searching: true
      })



    })
  </script>
@endsection
