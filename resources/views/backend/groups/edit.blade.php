@extends('layouts.admin')

@section('css')

<meta name="csrf-token" content="{{ csrf_token() }}">

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="contact-vue-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Groups
        <small>gestion des groups</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Groups</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Modification group</h3>
            </div>

            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="POST" action="{{ url('/dashboard/groups/'.$group->id.'/update') }}" accept-charset="UTF-8" id="create_form" name="edit_form" class="form-sample" enctype="multipart/form-data">
              <input name="_method" type="hidden" value="PUT">
             {{ csrf_field() }}

                @include ('backend.groups.form', ['group' => $group,])
            
              <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right" onSubmit="enableSpinner();">Save</button>
              </div>

            </form>
            
          </div>
          <!-- /.box -->
            
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->


@endsection

@section('js')



<script>

    var uploadField = document.getElementById("image");

    uploadField.onchange = function() {
        if(this.files[0].size > 1048576){
          alert("File is too big!");
          this.value = "";
        };
    };

   /* if($('#type').val() == "Link"){
      $('#group_block').hide();
      $('#url_block').show();
    }

    if($('#type').val() == "Group"){
          $('#group_block').show();
          $('#url_block').hide();
     }



     $('#type').change(function(){
        console.log($(this).val());
        if($(this).val() == "Link"){

          $('#group_block').hide();
          $('#url_block').show();

        }

        if($(this).val() == "Group"){
          $('#group_block').show();
          $('#url_block').hide();
        }
        
      });*/

 
</script> 


@endsection
