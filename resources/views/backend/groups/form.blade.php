
              <div class="box-body">

                <div class="col-md-12">
                  <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="title" value="{{ old('title', optional($group)->title) }}" required>
                    {!! $errors->first('title', '<p class="text-danger ">:message</p>') !!}
                  </div>

              

                  <div class="form-group" id="url_block">
                    <label for="url">Description</label>
                    <textarea  class="form-control" id="description" name="description" >{{ old('description', optional($group)->description) }}</textarea>
                  </div>

                  <div class="form-group" id="group_block">
                    <label for="type">Group</label>
                    @if (!empty($group))
                      <select class="form-control" name="type" id="type">
                      @foreach (['Group','Partner'] as $object)
                        @if ($object === $group->type)
                        <option value="{{$object}}" selected>{{$object}}</option>
                        @else
                        <option value="{{$object}}">{{$object}}</option>
                        @endif
                      @endforeach
                      </select>
                    @else
                      <select class="form-control" name="type" id="type">
                      @foreach (['Group','Partner'] as $object)
                        <option value="{{$object}}">{{$object}}</option>
                      @endforeach
                      </select>
                    @endif
                  </div>
                 

                  <div class="form-group ">
                      <label for="image" >Image</label>
                      <input name="image" type="file" id="image" value="" >
                      @if(!empty($group->image))
                      <a  href="{{ URL::asset(  'storage/'.$group->image  )}}" target="_blank"><img height="50px" src="{{ URL::asset('storage/'.$group->image  ) }}" alt=""/></a>    
                      @endif
                  </div>

                
                </div>
            
              </div>

          