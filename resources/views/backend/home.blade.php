@extends('layouts.admin')

@section('css')
<!--<link rel="stylesheet" href="{{ URL::asset('assets/admin/admin2/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">-->


@endsection

@section('content')




<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   

<section >
    <br>
    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif
</section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
             

            </div>
            <div class="box-body">

             <form role="form" method="POST" action="{{ url('/dashboard/search') }}" accept-charset="UTF-8" id="search_form" name="search_form" class="form-sample">
              {{ csrf_field() }}

                <div class="input-group input-group-sm">
                  <input type="text" id="keyword" name="keyword" class="form-control">
                      <span class="input-group-btn">
                        <button type="submit" class="btn btn-info btn-flat">Chercher</button>
                      </span>
                </div>

                  <div class="input-group input-group-sm">
                    <br>
                    <a href="javascript:void(0);" id="href-filter">Filtre</a>
                  </div>

                    <!-- checkbox -->
                <div class="form-group" id="filters">
                    <div class="radio">
                        <label>
                          <input type="radio" name="type_search" id="item" value="items" checked>
                          Produits
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="type_search" id="group" value="groups">
                          Groups
                        </label>
                      </div>

                      <div class="radio">
                          <label>
                            <input type="radio" name="type_search" id="menu" value="menus">
                            Menu
                          </label>
                        </div>

                      <hr>
  
                    <div class="checkbox" id="div_title">
                      <label>
                        <input type="checkbox" id="checkbox_title" name="checkbox_title" checked >
                        Titre
                      </label>
                    </div>

                    <div class="checkbox" id="div_description">
                        <label>
                          <input type="checkbox" id="checkbox_description" name="checkbox_description" >
                          Description
                        </label>
                    </div>

                    <div class="checkbox" id="div_mini_description">
                        <label>
                          <input type="checkbox" id="checkbox_mini_description" name="checkbox_mini_description" >
                          Mini Description
                        </label>
                    </div>


                    <div class="checkbox" id="div_tag">
                        <label>
                          <input type="checkbox" id="checkbox_tag" name="checkbox_tag" >
                          Tag
                        </label>
                    </div>

                  </div>
              
               </form>

              </div>

              
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              @if(!empty($result))
                @if( count($result) > 0)
                    <hr>
                    <br>
                    <br>
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Image</th>
                        <th></th>
                      </tr>
                      </thead>
                      <tbody>
                          @foreach($result as $r)
                          <tr>
                              <td>{{ $r->id }}</td>
                            <td>{{ $r->title }}</td>
                            <td><img src="{{ URL::asset('storage/'.$r->image)}}" width="50" alt="No Image"></td>
                            <td>
  
                                <form method="POST" action="{{ url('/dashboard/'.$type_search.'/'.$r->id.'/delete') }}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}
                                  <div class="btn-group btn-group-sm" role="group">
  
                                      <a href="{{ url('/dashboard/'.$type_search.'/'.$r->id.'/edit') }}" class="btn btn-info" title="Edit">
                                          <span class="fa fa-edit" aria-hidden="true"></span>
                                      </a>
                                      @if($type_search == 'menus')
                                      <a href="{{ url('/dashboard/menus/'.$r->id)}}" title="Sub menu" type="button" class="btn btn-success">
                                            <span class="fa fa-align-center" aria-hidden="true"></span>
                                      </a>
                                      @endif

                                      @if($type_search == 'groups')
                                      <a href="{{ url('/dashboard/items/'.$r->id)}}" title="Sub menu" type="button" class="btn btn-success">
                                            <span class="fa fa-align-center" aria-hidden="true"></span>
                                      </a>
                                      @endif


  
                                        <button type="submit" class="btn btn-danger" title="Supprimer" onclick="return confirm(&quot;Are you sure you want to delete? !&quot;)">
                                          <span class="fa fa-trash" aria-hidden="true"></span>
                                       </button>
  
  
                                  </div>
                                </form>
  
                            </td>
                          </tr>
                          @endforeach

                      </tbody>
                    
                    </table>
                @endif
              @endif
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



@endsection


@section('js')

<script src="{{ URL::asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>

  $('#example1').DataTable({
   })




  $('#filters').hide();
  $("#item").prop("checked", true);

  $('#href-filter').on('click',function(){
      $('#filters').toggle();

  });

  $('input[type=radio][name=type_search]').change(function() {

    if (this.value == 'items') {        
        $('#div_description').show();
        $('#div_mini_description').show();
        $('#div_tag').show();

        $('#checkbox_description').prop( "disabled", false );
        $('#checkbox_mini_description').prop( "disabled", false );
        $('#checkbox_tag').prop( "disabled", false );

    }

    if (this.value == 'groups') {
        $('#div_description').show();
        $('#div_mini_description').hide();
        $('#div_tag').hide();


        $('#checkbox_description').prop( "disabled", false );
        $('#checkbox_mini_description').prop( "disabled", true );
        $('#checkbox_tag').prop( "disabled", true );

    } 
    
    if(this.value == 'menus'){

        $('#div_description').hide();
        $('#div_mini_description').hide();
        $('#div_tag').hide();

        $('#checkbox_description').prop( "disabled", true );
        $('#checkbox_mini_description').prop( "disabled", true );
        $('#checkbox_tag').prop( "disabled", true );

    }
});


</script>
@endsection
