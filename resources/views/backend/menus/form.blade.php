
              <div class="box-body">

                <div class="col-md-12">
                  <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="title" value="{{ old('title', optional($menu)->title) }}" required>
                    {!! $errors->first('title', '<p class="text-danger ">:message</p>') !!}
                  </div>

                  <div class="form-group" >
                    <label for="pays_id">Type</label>
                    @if (!empty($menu))
                      <select class="form-control" name="type" id="type">
                      @foreach (['Link', 'Group','Tag'] as $object)
                        @if ($object === $menu->type)
                        <option value="{{$object}}" selected>{{$object}}</option>
                        @else
                        <option value="{{$object}}">{{$object}}</option>
                        @endif
                      @endforeach
                      </select>
                    @else
                      <select class="form-control" name="type" id="type">
                      @foreach (['Link', 'Group', 'Tag'] as $object)
                        <option value="{{$object}}">{{$object}}</option>
                      @endforeach
                      </select>
                    @endif
                  </div>


                  <div class="form-group" id="url_block">
                    <label for="url">URL</label>
                    <input type="text" class="form-control" id="url" name="url" placeholder="url" value="{{ old('url', optional($menu)->url) }}">
                  </div>

                  <div class="form-group" id="tag_block">
                    <label for="tag">Tag</label>
                    <input type="text" class="form-control" id="tag" name="tag" placeholder="tag" value="{{ old('tag', optional($menu)->tag) }}">
                  </div>

                  <div class="form-group" id="group_block">
                    <label for="group_id">Group</label>
                    @if (!empty($menu))
                      <select class="form-control" name="group_id" id="group_id">
                      @foreach ($groups as $object)
                        @if ($object->id === $menu->group_id)
                        <option value="{{$object->id}}" selected>{{$object->title}}</option>
                        @else
                        <option value="{{$object->id}}">{{$object->title}}</option>
                        @endif
                      @endforeach
                      </select>
                    @else
                      <select class="form-control" name="group_id" id="group_id">
                      @foreach ($groups as $object)
                        <option value="{{$object->id}}">{{$object->title}}</option>
                      @endforeach
                      </select>
                    @endif
                  </div>

                 
                
                </div>
            
              </div>

          