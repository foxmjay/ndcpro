@extends('layouts.admin')

@section('css')

<meta name="csrf-token" content="{{ csrf_token() }}">

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="contact-vue-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Menus
        <small>gestion des menus</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Menus</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Modification menu</h3>
            </div>

            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="POST" action="{{ url('/dashboard/menus/'.$menu->id.'/update') }}" accept-charset="UTF-8" id="create_client_form" name="edit_form" class="form-sample">
              <input name="_method" type="hidden" value="PUT">
             {{ csrf_field() }}

                @include ('backend.menus.form', ['menu' => $menu,])
            
              <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right" onSubmit="enableSpinner();">Save</button>
              </div>

            </form>
            
          </div>
          <!-- /.box -->
            
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->


@endsection

@section('js')



<script>

    if($('#type').val() == "Link"){
      $('#group_block').hide();
      $('#tag_block').hide();
      $('#url_block').show();
    }

    if($('#type').val() == "Group"){
          $('#group_block').show();
          $('#url_block').hide();
          $('#tag_block').hide();
     }


     if($('#type').val() == "Tag"){
          $('#group_block').hide();
          $('#url_block').hide();
          $('#tag_block').hide();
     }


     $('#type').change(function(){
        console.log($(this).val());
        if($(this).val() == "Link"){

        $('#group_block').hide();
        $('#url_block').show();
        $('#tag_block').hide();


        }

        if($(this).val() == "Group"){
        $('#group_block').show();
        $('#url_block').hide();
        $('#tag_block').hide();

        }


        if($(this).val() == "Tag"){
        $('#group_block').hide();
        $('#url_block').hide();
        $('#tag_block').hide();
        }
        
      });

 
</script> 


@endsection
