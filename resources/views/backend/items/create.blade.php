@extends('layouts.admin')

@section('css')

<link href="{{ URL::asset('assets/admin/plugins/summernote/summernote.css')}}" rel="stylesheet">

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Items
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Items</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Creation Item</h3>
            </div>

            <!-- /.box-header -->
            <!-- form start -->

            <form role="form" method="POST" action="{{ url('/dashboard/items/'.$group_id.'/store') }}" accept-charset="UTF-8" id="mainForm" name="create_form" class="form-sample" enctype="multipart/form-data">
             {{ csrf_field() }}

                @include ('backend.items.form', ['item' => null,])

              <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right" onSubmit="enableSpinner();">Add</button>
              </div>

            </form>
            
          </div>
          <!-- /.box -->
            
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection


@section('js')
    <script src="{{ URL::asset('assets/admin/plugins/summernote/summernote.min.js')}}"></script>

    <script>


        $('.imagefile').on('change', function() {
            if(this.files[0].size > 1048576){
              alert("File is too big!");
              this.value = "";
            };
        });

        $(document).ready(function() {

          if( $('#promotion').prop('checked') == false ){
            $('#prix_promotion_form').hide();
           }

          $('#promotion').on('ifChanged',function(event){
            if(this.checked){
              $('#prix_promotion_form').show();
            }else{
              $('#prix_promotion_form').hide();
            }
          })

          function findTagid(tagid_str){
                console.log(this);
               if(tagid_str == this) return true;
               else return false;
          }

          var tagids=[];

          $("#mytags").tagit({
            singleField: true,
            //singleFieldNode: $('#tags'),
            beforeTagRemoved: function(event, ui) {
              index = tagids.findIndex(findTagid,ui.tagLabelId)
              tagids.splice(index,1);
            }
           });

            $('#description').summernote({
                height: 250,
                                        
            });

            $('.addTag').on('click', function(ret){
              tagids.push($(this).attr('tagid'));
              $("#mytags").tagit("createTag", $(this).attr('tag-labelid'));
              //console.log(tagids);
              
            });


            $('#mainForm').submit(function(event) {

            event.preventDefault(); //this will prevent the default submit
            //console.log('okok');

            var selected = [];
            $('.checkedMenus:checked').each(function() {
                selected.push($(this).attr('name'));
            });

            //console.log(selected.join(';'));
            $('#menu').val(selected.join(';'));
            $('#tags').val(tagids.join(';'));


            $(this).unbind('submit').submit(); // continue the submit unbind preventDefault

            })
        });

    </script>



@endsection