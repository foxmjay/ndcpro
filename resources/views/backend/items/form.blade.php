
              <div class="box-body">

                <div class="col-md-12">
                  <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="title" value="{{ old('title', optional($item)->title) }}" required>
                    {!! $errors->first('title', '<p class="text-danger ">:message</p>') !!}
                  </div>

                  <div class="form-group">
                    <label for="side_description">Side description</label>
                    <textarea  class="form-control" id="side_description" name="side_description" >{{ old('side_description', optional($item)->side_description) }}</textarea>
                  </div>

                  <!--<div class="form-group" >
                    <label for="description">Full description</label>
                    <textarea  class="form-control" id="description" name="description" >{{ old('description', optional($item)->description) }}</textarea>
                  </div>-->

                  <div class="form-group" >
                    <label for="description">Full description</label>
                    <textarea id="description" name="description" >{{ old('description', optional($item)->description) }}</textarea>
                  </div>


                  <div class="form-group ">
                      <label for="image1" >Image 1 ( 730 x 730)</label>
                      <input name="image1" type="file" id="image1" value=""  class="imagefile">
                      @if(!empty($item->image1))
                      <a  href="{{ URL::asset(  'storage/'.$item->image1  )}}" target="_blank"><img height="50px" src="{{ URL::asset('storage/'.$item->image1  ) }}" alt=""/></a>    
                      @endif
                  </div>
                  <div class="form-group ">
                      <label for="image2" >Image 2 ( 730 x 730)</label>
                      <input name="image2" type="file" id="image2" value="" class="imagefile">
                      @if(!empty($item->image2))
                      <a  href="{{ URL::asset(  'storage/'.$item->image2  )}}" target="_blank"><img height="50px" src="{{ URL::asset('storage/'.$item->image2  ) }}" alt=""/></a>    
                      @endif
                  </div>    

                  <div class="form-group ">
                      <label for="image3" >Image 3 ( 730 x 730)</label>
                      <input name="image3" type="file" id="image3" value="" class="imagefile" >
                      @if(!empty($item->image3))
                      <a  href="{{ URL::asset(  'storage/'.$item->image3  )}}" target="_blank"><img height="50px" src="{{ URL::asset('storage/'.$item->image3  ) }}" alt=""/></a>    
                      @endif
                  </div>    
                  <div class="form-group ">
                      <label for="image4" >Image 4 ( 730 x 730)</label>
                      <input name="image4" type="file" id="image4" value="" class="imagefile">
                      @if(!empty($item->image4))
                      <a  href="{{ URL::asset(  'storage/'.$item->image4  )}}" target="_blank"><img height="50px" src="{{ URL::asset('storage/'.$item->image4  ) }}" alt=""/></a>    
                      @endif
                  </div>    
                  <div class="form-group ">
                      <label for="image5" >Image 5 ( 730 x 730)</label>
                      <input name="image5" type="file" id="image5" value="" class="imagefile" >
                      @if(!empty($item->image5))
                      <a  href="{{ URL::asset(  'storage/'.$item->image5 )}}" target="_blank"><img height="50px" src="{{ URL::asset('storage/'.$item->image5  ) }}" alt=""/></a>    
                      @endif
                  </div>                        
                
                  <!--<div class="form-group">
                    <label for="video">Video URL</label>
                    <input type="text" class="form-control" id="video" name="video" placeholder="video" value="{{ old('video', optional($item)->video) }}" >
                    {!! $errors->first('title', '<p class="text-danger ">:message</p>') !!}
                  </div>-->

                  <!--<div class="form-group">
                    <label>
                          @if(!empty($item))
                            @if($item->new == 1)
                              <input type="checkbox" id="new" name="new" checked>  Nouveau
                            @else
                              <input type="checkbox" id="new" name="new">  Nouveau
                            @endif
                          @else
                            <input type="checkbox" id="new" name="new" >  Nouveau
                          @endif
                    </label>
                  </div> -->

                  <div class="input-group margin">
                      <div class="input-group-btn">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Tags
                          <span class="fa fa-caret-down"></span></button>
                        <ul class="dropdown-menu">
                            @if (!empty($tags))
                              @foreach ($tags as $tag)
                                 <li><a href="javascript:void(0);" class="addTag" tagid="{{$tag->id}}" tag-labelid="{{$tag->display}}_{{$tag->id}}">{{$tag->display}}</a></li>
                                @endforeach
                            @endif
                        </ul>
                      </div>
                      <!-- /btn-group -->
                      <div class="col-md-11">
                          <ul id="mytags" class="primary">
                          </ul>
                          <input type="hidden"  name="tags" id="tags" value="">
                      </div>
                    </div>

                    <div class="form-group">
                        <label>
                          @if (!empty($item))
                            @if($item->price_start == 1)
                              <input type="checkbox"  class="minimal" name="price_start" id="price_start" checked>A partir de
                            @else 
                              <input type="checkbox"  class="minimal" name="price_start" id="price_start">A partir de
                            @endif
                          @else
                            <input type="checkbox"  class="minimal" name="price_start" id="price_start">A partir de
                          @endif
                        </label>
                        {!! $errors->first('price_start', '<p class="text-danger ">:message</p>') !!}
                    </div>
                    
                    <div class="form-group">
                      <label for="price">Prix</label>
                      <input type="number" step="0.01" class="form-control" id="price" name="price" placeholder="price" value="{{ old('price', optional($item)->price) }}" >
                      {!! $errors->first('price', '<p class="text-danger ">:message</p>') !!}
                    </div>

                    <div class="form-group">
                        <label>
                          @if (!empty($item))
                            @if($item->promotion == 1)
                              <input type="checkbox"  class="minimal" name="promotion" id="promotion" checked>Promotion
                            @else 
                              <input type="checkbox"  class="minimal" name="promotion" id="promotion">Promotion
                            @endif
                          @else
                            <input type="checkbox"  class="minimal" name="promotion" id="promotion">Promotion
                          @endif
                        </label>
                        {!! $errors->first('promotion', '<p class="text-danger ">:message</p>') !!}
                    </div>

                    <div class="form-group" id="prix_promotion_form">
                      <label for="price_promotion">Prix Promotion</label>
                      <input type="number" step="0.01" class="form-control" id="price_promotion" name="price_promotion" placeholder="price_promotion" value="{{ old('price_promotion', optional($item)->price_promotion) }}" >
                      {!! $errors->first('price_promotion', '<p class="text-danger ">:message</p>') !!}
                    </div>

                    

                  <!--<div class="form-group">
                    <div class="row">
                      <div class="col-md-4">
                        <label for="tagList">Tags</label>
                        @if (!empty($tags))
                          <select class="form-control" name="tagList" id="tagList">
                          @foreach ($tags as $tag)
                            <option value="{{$tag->id}}">{{$tag->display}}</option>
                          @endforeach
                          </select>
                              <button type="submit" class="btn btn-info btn-flat pull-right">Add</button>
                        @endif
                      </div>
                      <div class="col-md-12">
                        <input type="text" class="form-control" id="tags" name="tags" placeholder="tag,tag,tag" value="{{ old('tags', optional($item)->tags) }}" >
                        {!! $errors->first('title', '<p class="text-danger ">:message</p>') !!}
                      </div>
                    </div>

                  </div>-->



                  <!--<div class="input-group input-group-sm">
                    <select class="form-control" id="menuList">
                      @foreach ($menus as $menu)
                        <option value="{{$menu->title}}">{{$menu->title}}</option>
                      @endforeach
                      </select>
                        <span class="input-group-btn">
                          <button id="addButton" type="button" class="btn btn-info btn-flat">Add</button>
                        </span>
                  </div>-->

                  

                  <div class="form-group">
                  <hr>

                    @if (!empty($item))
                    
                      @foreach ($menus as $menu)
                          @if(strpos($item->menu,$menu->title) !== false )
                            <input class="checkedMenus minimal" name="{{$menu->title}}" type="checkbox" checked  > {{ empty($menu->menu)? '' : $menu->menu->title.' -- '}} {{$menu->title}} <br>
                          @else
                            <input class="checkedMenus minimal" name="{{$menu->title}}" type="checkbox" >{{ empty($menu->menu)? '' : $menu->menu->title.' -- '}} {{$menu->title}} <br>
                          @endif
                      @endforeach  
                    @else
                      @foreach ($menus as $menu)
                          <input class="checkedMenus minimal" name="{{$menu->title}}" type="checkbox" >{{ empty($menu->menu)? '' : $menu->menu->title.' -- '}} {{$menu->title}} <br>
                      @endforeach  
                    
                    @endif
                 </div>

                 <div class="form-group">
                    <input type="hidden" class="form-control" id="menu" name="menu" value="{{ old('menu', optional($item)->menu) }}"  >
                    {!! $errors->first('title', '<p class="text-danger ">:message</p>') !!}

                  </div>


                </div>
            
              </div>

          