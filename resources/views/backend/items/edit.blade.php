@extends('layouts.admin')

@section('css')

<link href="{{ URL::asset('assets/admin/plugins/summernote/summernote.css')}}" rel="stylesheet">

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="contact-vue-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Items
        <small>gestion des items</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Items</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Modification item</h3>
            </div>

            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="POST" action="{{ url('/dashboard/items/'.$item->id.'/update') }}" accept-charset="UTF-8" id="mainForm" name="edit_form" class="form-sample" enctype="multipart/form-data">
              <input name="_method" type="hidden" value="PUT">
             {{ csrf_field() }}

                @include ('backend.items.form', ['item' => $item,])
            
              <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right" onSubmit="enableSpinner();">Save</button>
              </div>

            </form>
            
          </div>
          <!-- /.box -->
            
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->


@endsection


@section('js')
    <script src="{{ URL::asset('assets/admin/plugins/summernote/summernote.min.js')}}"></script>

    <script>

        //var uploadField = document.getElementsByClassName("imagefile");

        $('.imagefile').on('change', function() {
            if(this.files[0].size > 1048576){
              alert("File is too big!");
              this.value = "";
            };
        });


        var menuCount =0 ;
        var tagids=[];
        $(document).ready(function() {
          //alert( $('#promotion').prop('checked') == false )

          if( $('#promotion').prop('checked') == false ){
            $('#prix_promotion_form').hide();
           }


          $('#promotion').on('ifChanged',function(event){
            if(this.checked){
              $('#prix_promotion_form').show();
            }else{
              $('#prix_promotion_form').hide();
            }
          })

            function findTagid(tagid_str){
                //console.log(this);
               if(tagid_str == this) return true;
               else return false;
            }
         
           $("#mytags").tagit({
            singleField: true,
            //singleFieldNode: $('#tags'),
            beforeTagRemoved: function(event, ui) {
              index = tagids.findIndex(findTagid,ui.tagLabelId)
              tagids.splice(index,1);
            }
            
           });

           <?php 
            $tags =\App\Tag::getTags($item->tags);
            $tagids = array();
            $tagids = explode(';',$item->tags);
            $tagsNoEmpty=[];
            foreach($tagids as $t){
              if($t != "")
                  array_push($tagsNoEmpty,$t);
            }

            ?>
            
           var tags = {!! json_encode($tags) !!};
           tagids = {!! json_encode($tagsNoEmpty) !!};

           //console.log(tagids);
           for(var i=0;i<tags.length;i++){
            $("#mytags").tagit("createTag", tags[i]);
          }


            $('#description').summernote({
                height: 250,
                                        
            });

            $('.addTag').on('click', function(ret){
              

              tagids.push($(this).attr('tagid'));
              $("#mytags").tagit("createTag", $(this).attr('tag-labelid'));
              //$("#tags").text(tags+";"+)
            });

            $('#mainForm').submit(function(event) {

                  event.preventDefault(); //this will prevent the default submit
                  //console.log('okok');

                  var selected = [];
                  $('.checkedMenus:checked').each(function() {
                      selected.push($(this).attr('name'));
                  });

                  //console.log(selected.join(';'));
                  $('#menu').val(selected.join(';'));

                  $('#tags').val(tagids.join(';'));

                  $(this).unbind('submit').submit(); // continue the submit unbind preventDefault

              })
        });
    </script>



@endsection
