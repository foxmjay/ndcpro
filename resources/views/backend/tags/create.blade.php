@extends('layouts.admin')

@section('css')

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tags
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tags</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Creation Tag</h3>
            </div>

            <!-- /.box-header -->
            <!-- form start -->

            <form role="form" method="POST" action="{{ url('/dashboard/tags/store') }}" accept-charset="UTF-8" id="create_client_form" name="create_form" class="form-sample" enctype="multipart/form-data">
             {{ csrf_field() }}

                @include ('backend.tags.form', ['tag' => null,])

              <div class="box-footer">
                  <button type="submit" class="btn btn-primary pull-right" onSubmit="enableSpinner();">Add</button>
              </div>

            </form>
            
          </div>
          <!-- /.box -->
            
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection

@section('js')



<script>

  $('#form_id').submit(function() {
        var re = new RegExp("^[a-z0-9-_]*$");
        if(re.test($('#name').val())== false){
          $('#error_name').show()
          return false;
        }
        return true; // return false to cancel form action
    });

    /*if($('#type').val() == "Link"){
      $('#group_block').hide();
      $('#url_block').show();
    }

    if($('#type').val() == "Tag"){
          $('#group_block').show();
          $('#url_block').hide();
     }



     $('#type').change(function(){
        console.log($(this).val());
        if($(this).val() == "Link"){

          $('#group_block').hide();
          $('#url_block').show();

        }

        if($(this).val() == "Tag"){
          $('#group_block').show();
          $('#url_block').hide();
        }
        
      });*/

 
</script> 

@endsection
