
              <div class="box-body">

                <div class="col-md-12">
                  <div class="form-group">
                    <label for="title">Name (exp : Sécurite électronique)</label>
                    <input type="text" class="form-control" id="display" name="display" placeholder="display" value="{{ old('display', optional($tag)->display) }}" required>
                    {!! $errors->first('display', '<p class="text-danger ">:message</p>') !!}
                  </div>

                  <div class="form-group">
                      <label for="title">System Ref (exp : securite_electronique)</label>
                      <input type="text" class="form-control" id="name" name="name" placeholder="name" value="{{ old('name', optional($tag)->name) }}" required>
                      {!! $errors->first('name', '<p class="text-danger ">:message</p>') !!}
                      <p style="display: none" id="error_name" class="text-danger ">Make sure to use only lower case Alphanumeric characters and _ . no spaces</p>
                    </div>
                
                </div>
            
              </div>

          