<div class="techmarket-sticky-wrap">
    <div class="row">
        <nav id="handheld-navigation" class="handheld-navigation" aria-label="Handheld Navigation">
            <button class="btn navbar-toggler" type="button">
                <i class="tm tm-departments-thin"></i>
                <span>Menu</span>
            </button>
            <div class="handheld-navigation-menu">
                <span class="tmhm-close">Close</span>
                <ul id="menu-departments-menu-1" class="nav">
                    <!--
                    <li class="highlight menu-item animate-dropdown">
                        <a title="Value of the Day" href="shop.html">Value of the Day</a>
                    </li>
                    <li class="highlight menu-item animate-dropdown">
                        <a title="Top 100 Offers" href="shop.html">Top 100 Offers</a>
                    </li>
                    <li class="highlight menu-item animate-dropdown">
                        <a title="New Arrivals" href="shop.html">New Arrivals</a>
                    </li>-->


                @foreach($menuList as $mlist )
                @php
                    $subm = \App\Menu::getMenu($mlist->id);
                @endphp

                @if(count($subm)<=0)
                  <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown-submenu">
                        <a title="Computers &amp; Laptops" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true" href="#">{{$mlist->title}}<span class="caret"></span></a>
                  </li>   
                @else

                    <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown-submenu">
                        <a title="Computers &amp; Laptops" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true" href="#">{{$mlist->title}}<span class="caret"></span></a>
                        <ul role="menu" class=" dropdown-menu">
                            <li class="menu-item menu-item-object-static_block animate-dropdown">
                                <div class="yamm-content">
                                    <div class="bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                        <div class="kc-col-container">
                                            <div class="kc_single_image">
                                                <img src="{{ URL::asset('assets/frontend2/images/megamenu.jpg')}}" class="" alt="" />
                                            </div>
                                            <!-- .kc_single_image -->
                                        </div>
                                        <!-- .kc-col-container -->
                                    </div>
                                    <!-- .bg-yamm-content -->
                                    <div class="row yamm-content-row">
                                        @foreach($subm as $sub)
                                        @php
                                            $subm2 = \App\Menu::getMenu($sub->id);
                                        @endphp
                                        <div class="col-md-6 col-sm-12">
                                            <div class="kc-col-container">
                                                <div class="kc_text_block">
                                                    <ul>
                                                            @if($sub->type == 'Link')
                                                            <li class="nav-title">
                                                                <a href="{{$sub->url}}" style="color: #bb2131">{{$sub->title}}</a>
                                                            </li>
                                                            @elseif($sub->type == 'Group')
                                                                <li class="nav-title">
                                                                    <a href="{{'/list/'.$sub->group_id}}" style="color: #bb2131">{{$sub->title}}</a>
                                                                </li>
                                                            @else
                                                                <li class="nav-title">
                                                                    <a href="{{ '/menutags/'.$sub->id}}" style="color: #bb2131">{{$sub->title}}</a>
                                                                </li>
                                                            @endif
                                                        @if( count($subm2) > 0 )                               
                                                            <li class="nav-title" style="color: #bb2131">{{$sub->title}}</li>
                                                            @foreach($subm2 as $sub2)
                                                                @if($sub2->type == 'Link')
                                                                <li >
                                                                    <a href="{{$sub2->url}}">{{$sub2->title}}</a>
                                                                </li>
                                                                @elseif($sub2->type == 'Group')
                                                                    <li >
                                                                        <a href="{{'/list/'.$sub2->group_id}}">{{$sub2->title}}</a>
                                                                    </li>
                                                                @else
                                                                    <li >
                                                                        <a href="{{ '/menutags/'.$sub2->id}}">{{$sub2->title}}</a>
                                                                    </li>
                                                                @endif
                                                                
                                                            @endforeach
                                                    
                                                        @endif
                                                        <li class="nav-divider"></li>
                                                        <!--<li>
                                                            <a href="#">
                                                                <span class="nav-text">Voir tout</span>
                                                                <span class="nav-subtext">Decouvrir plus de produits</span>
                                                            </a>
                                                        </li>-->
                                                    </ul>
                                                </div>
                                                <!-- .kc_text_block -->
                                            </div>
                                            <!-- .kc-col-container -->
                                        </div>
                                        @endforeach
                                    </div>
                                    <!-- .kc_row -->
                                </div>
                                <!-- .yamm-content -->
                            </li>
                        </ul>
                    </li>
                @endif
                @endforeach

                    <li class="menu-item animate-dropdown">
                        <a title="A Propos de nous" href="/aboutus">A Propos de nous</a>
                    </li>
                    <li class="menu-item animate-dropdown">
                        <a title="Contact" href="/contact">Contact</a>
                    </li>
                    
                </ul>
            </div>
            <!-- .handheld-navigation-menu -->
        </nav>
        <!-- .handheld-navigation -->
        <div class="site-search">
            <div class="widget woocommerce widget_product_search">
                <form role="search" method="get" class="woocommerce-product-search" action="#">
                    <label class="screen-reader-text" for="woocommerce-product-search-field-0">Chercher :</label>
                    <input type="search" id="woocommerce-product-search-field-0" class="search-field" placeholder="Chercher un produit" value="" name="s" />
                    <input type="submit" value="Search" />
                    <input type="hidden" name="post_type" value="product" />
                </form>
            </div>
            <!-- .widget -->
        </div>
        <!-- .site-search -->
        <a class="handheld-header-cart-link has-icon" href="/mobile_cart" title="View your shopping cart">
            <i class="tm tm-shopping-bag"></i>
            <span class="count">@if( Session::has('cart')) {{count(Session::get('cart'))}} @else {{0}}@endif</span>
        </a>
    </div>
    <!-- /.row -->
</div>