
            <footer class="site-footer footer-v1">
                <div class="col-full">
                    <div class="before-footer-wrap">
                        <div class="col-full">
                            <div class="footer-newsletter">
                                <div class="media">
                                    <i class="footer-newsletter-icon tm tm-newsletter"></i>
                                    <div class="media-body">
                                        <div class="clearfix">
                                            <div class="newsletter-header">
                                                <h5 class="newsletter-title">Abonnez vous <br> à notre newsletter</h5>
                                                <span class="newsletter-marketing-text">Pour ne ratter aucune
                                                    <strong>Promotion !</strong>
                                                </span>
                                            </div>
                                            <!-- .newsletter-header -->
                                            <div class="newsletter-body">
                                                <form class="newsletter-form">
                                                    <input type="text" placeholder="Entrer votre Email">
                                                    <button class="button" type="button">S'abonner</button>
                                                </form>
                                            </div>
                                            <!-- .newsletter body -->
                                        </div>
                                        <!-- .clearfix -->
                                    </div>
                                    <!-- .media-body -->
                                </div>
                                <!-- .media -->
                                
                            </div>
                            <!-- .footer-newsletter -->
                            <div class="footer-social-icons">
                                <ul class="social-icons nav">
                                    <li class="nav-item">
                                        <a class="sm-icon-label-link nav-link" href="#">
                                            <i class="fa fa-facebook"></i> Facebook</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="sm-icon-label-link nav-link" href="#">
                                            <i class="fa fa-twitter"></i> Twitter</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="sm-icon-label-link nav-link" href="#">
                                            <i class="fa fa-google-plus"></i> Google+</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="sm-icon-label-link nav-link" href="#">
                                            <i class="fa fa-vimeo-square"></i> Vimeo</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="sm-icon-label-link nav-link" href="#">
                                            <i class="fa fa-rss"></i> RSS</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- .footer-social-icons -->
                        </div>
                        <!-- .col-full -->
                    </div>


                      <!-- Google map start -->
                      <div class="col-full">
                        <div class="stretch-full-width-map">
                            <div id="contactMap" class="contact-map" >
                                    <iframe style="width: 100%; height: 350px;"  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3297.6458272068285!2d-6.584374184779554!3d34.25757998055052!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xda75953e2560b0f%3A0x4b2bc64e493103b4!2sNDCPROMAROC!5e0!3m2!1sen!2sus!4v1609879685613!5m2!1sen!2sus"  frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                            </div>
                        </div>
                     </div>
                     <!-- Google map end -->

                    <!-- .before-footer-wrap -->
                    <div class="footer-widgets-block">
                        <div class="row">
                            <div class="footer-contact">
                                <div class="footer-logo">
                                    <a href="#" class="custom-logo-link" rel="home">
                                            <!-- Logo Footer -->
                                            <a href="#" class="custom-logo-link" rel="home">
                                                    <img src="{{ URL::asset('assets/frontend2/images/logo-large.png')}}">
                                                </a>
                                    </a>
                                </div>
                                <!-- .footer-logo -->
                                <div class="contact-payment-wrap">
                                    <div class="footer-contact-info">
                                        <div class="media">
                                            <span class="media-left icon media-middle">
                                                <i class="tm tm-call-us-footer"></i>
                                            </span>
                                            <div class="media-body">
                                                <span class="call-us-title">Vous avez besion de details sur l'un des produits?</span>
                                                <span class="call-us-text">Appelez nous au <br> (+212) 641 010 222 </span>
                                                <address class="footer-contact-address">Address: N° 10, Angle 115 rue Maamora et 55 rue Med El Amraoui Mag. N°10 KENITRA </address>
                                                <a href="https://goo.gl/maps/2DQCsmbMmVagWJ7Z7" target="_balnk" class="footer-address-map-link">
                                                    <i class="tm tm-map-marker"></i>Touvez nous sur Googlemaps</a>
                                            </div>
                                            <!-- .media-body -->
                                        </div>
                                        <!-- .media -->
                                    </div>
                                    <!-- .footer-contact-info -->
                                    <div class="footer-payment-info">
                                        <div class="media">
                                            <span class="media-left icon media-middle">
                                                <i class="tm tm-safe-payments"></i>
                                            </span>
                                            <div class="media-body">
                                                <h5 class="footer-payment-info-title">Methode de paiements</h5>
                                                <div class="footer-payment-icons">
                                                    <ul class="list-payment-icons nav">
                                                        <li class="nav-item">
                                                            <img class="payment-icon-image" src="{{ URL::asset('assets/frontend2/images/credit-cards/mastercard.svg') }}" alt="mastercard" />
                                                        </li>
                                                        <li class="nav-item">
                                                            <img class="payment-icon-image" src="{{ URL::asset('assets/frontend2/images/credit-cards/visa.svg') }}" alt="visa" />
                                                        </li>
                                                        <li class="nav-item">
                                                            <img class="payment-icon-image" src="{{ URL::asset('assets/frontend2/images/credit-cards/paypal.svg') }}" alt="paypal" />
                                                        </li>
                                                        <li class="nav-item">
                                                            <img class="payment-icon-image" src="{{ URL::asset('assets/frontend2/images/credit-cards/maestro.svg') }}" alt="maestro" />
                                                        </li>
                                                    </ul>
                                                </div>
                                                <!-- .footer-payment-icons -->
                                                <div class="footer-secure-by-info">
                                                    <h6 class="footer-secured-by-title">Secured by:</h6>
                                                    <ul class="footer-secured-by-icons">
                                                        <li class="nav-item">
                                                            <img class="secure-icons-image" src="{{ URL::asset('assets/frontend2/images/secured-by/norton.svg') }}" alt="norton" />
                                                        </li>
                                                        <li class="nav-item">
                                                            <img class="secure-icons-image" src="{{ URL::asset('assets/frontend2/images/secured-by/mcafee.svg') }}" alt="mcafee" />
                                                        </li>
                                                    </ul>
                                                </div>
                                                <!-- .footer-secure-by-info -->
                                            </div>
                                            <!-- .media-body -->
                                        </div>
                                        <!-- .media -->
                                    </div>
                                    <!-- .footer-payment-info -->
                                </div>
                                <!-- .contact-payment-wrap -->
                            </div>
                            <!-- .footer-contact -->
                            <div class="footer-widgets">
                                <div class="columns">
                                    <aside class="widget clearfix">
                                        <div class="body">
                                            <h4 class="widget-title">Liens Pratiques</h4>
                                            <div class="menu-footer-menu-1-container">
                                                <ul id="menu-footer-menu-1" class="menu">
                                                    <li class="menu-item">
                                                        <a href="/">Accueil</a>
                                                    </li>
                                                    <li class="menu-item">
                                                        <a href="#">Conditions Générales de vente</a>
                                                    </li>
        
                                                    <li class="menu-item">
                                                        <a href="/aboutus">A Prepos de nous</a>
                                                    </li>
                                                    <li class="menu-item">
                                                        <a href="/contact">Contact</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- .menu-footer-menu-1-container -->
                                        </div>
                                        <!-- .body -->
                                    </aside>
                                    <!-- .widget -->
                                </div>
                                <!-- .columns 
                                <div class="columns">
                                    <aside class="widget clearfix">
                                        <div class="body">
                                            <h4 class="widget-title">&nbsp;</h4>
                                            <div class="menu-footer-menu-2-container">
                                                <ul id="menu-footer-menu-2" class="menu">
                                                    <li class="menu-item">
                                                        <a href="#">Téléphonie</a>
                                                    </li>
                                                    <li class="menu-item">
                                                        <a href="#">Courant faible</a>
                                                    </li>
                                                    <li class="menu-item">
                                                        <a href="#">Sécurite électronique</a>
                                                    </li>
                                                    <li class="menu-item">
                                                        <a href="#">Audio visuel</a>
                                                    </li>
                                                    <li class="menu-item">
                                                        <a href="#">Informatique</a>
                                                    </li>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </aside>
                                </div>-->
                                <!-- .columns -->
                                <div class="columns">
                                    <aside class="widget clearfix">
                                        <div class="body">
                                            <h4 class="widget-title">Super</h4>
                                            <div class="menu-footer-menu-3-container">
                                                <ul id="menu-footer-menu-3" class="menu">
                                                    <li class="menu-item">
                                                        <a href="/promotions">Promotions</a>
                                                    </li>
                                                    <li class="menu-item">
                                                        <a href="#">Offres</a>
                                                    </li>
                                                    <li class="menu-item">
                                                        <a href="#">Nouveau</a>
                                                    </li>
                                                    <li class="menu-item">
                                                        <a href="#">Prix choque</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- .menu-footer-menu-3-container -->
                                        </div>
                                        <!-- .body -->
                                    </aside>
                                    <!-- .widget -->
                                </div>
                                <!-- .columns -->
                            </div>
                            <!-- .footer-widgets -->
                        </div>
                        <!-- .row -->
                    </div>

                  
                    <!-- .footer-widgets-block -->
                    <div class="site-info">
                        <div class="col-full">
                            <div class="copyright">Copyright &copy; 2020 <a href="http://sysfastdevelopment.com">Sysfast Development</a>  All rights reserved.</div>
                            <!-- .copyright -->
                            <!--<div class="credit">Made with
                                <i class="fa fa-heart"></i> by bcube.</div>-->
                            <!-- .credit -->
                        </div>
                        <!-- .col-full -->
                    </div>
                    <!-- .site-info -->
                </div>
                <!-- .col-full -->
            </footer>
            <!-- .site-footer -->