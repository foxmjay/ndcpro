
  <div class="row align-items-center">
                        <div id="departments-menu" class="dropdown departments-menu">
                            <button class="btn dropdown-toggle btn-block" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="tm tm-departments-thin"></i>
                                <span>Categories</span>
                            </button>
                            <ul id="menu-departments-menu" class="dropdown-menu yamm departments-menu-dropdown">
                                <!--
                                <li class="highlight menu-item animate-dropdown">
                                    <a title="Value of the Day" href="home-v2.html">Value of the Day</a>
                                </li>
                                <li class="highlight menu-item animate-dropdown">
                                    <a title="Top 100 Offers" href="home-v3.html">Top 100 Offers</a>
                                </li>
                                <li class="highlight menu-item animate-dropdown">
                                    <a title="New Arrivals" href="home-v4.html">New Arrivals</a>
                                </li>-->

                                @foreach($categories as $cat )
                                @php
                                    $subCat = \App\Categorie::getCategorie($cat->id);
                                @endphp
                                <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown-submenu">
                                    <a title="Computers &amp; Laptops" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true" href="#">{{$cat->title}}<span class="caret"></span></a>
                                    <ul role="menu" class=" dropdown-menu">
                                        <li class="menu-item menu-item-object-static_block animate-dropdown">
                                            <div class="yamm-content">
                                                <div class="bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                                    <div class="kc-col-container">
                                                        <div class="kc_single_image">
                                                            <img src="{{ URL::asset('assets/frontend2/images/megamenu.jpg')}}" class="" alt="" />
                                                        </div>
                                                        <!-- .kc_single_image -->
                                                    </div>
                                                    <!-- .kc-col-container -->
                                                </div>
                                                <!-- .bg-yamm-content -->
                                                <div class="row yamm-content-row">
                                                    <div class="col-md-6 col-sm-12">
                                                        <div class="kc-col-container">
                                                            <div class="kc_text_block">
                                                                <ul>
                                                                    <li class="nav-title">{{$cat->title}}</li>
                                                                    @foreach($subCat as $scat )
                                                                    <li><a href="{{'/frontend3/tags/'.$scat->tag}}">{{$scat->title}}</a></li>
                                                                    @endforeach
                                                                   
                                                                    <li class="nav-divider"></li>
                                                                    
                                                                    <li>
                                                                        <a href="#">
                                                                            <span class="nav-text">Voir tout</span>
                                                                            <span class="nav-subtext">Decouvrir plus de produits</span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <!-- .kc_text_block -->
                                                        </div>
                                                        <!-- .kc-col-container -->
                                                    </div>
                                                   
                                                </div>
                                                <!-- .kc_row -->
                                            </div>
                                            <!-- .yamm-content -->
                                        </li>
                                    </ul>
                                </li>
                                @endforeach
                                <!--
                               
                                <li class="menu-item menu-item-type-custom animate-dropdown">
                                    <a title="Gadgets" href="landing-page-v1.html">Gadgets</a>
                                </li>
                                <li class="menu-item menu-item-type-custom animate-dropdown">
                                    <a title="Virtual Reality" href="landing-page-v2.html">Virtual Reality</a>
                                </li>
                                -->
                            </ul>
                        </div>
                        <!-- .departments-menu -->
                        <form class="navbar-search" method="get" action="home-v1.html">
                            <label class="sr-only screen-reader-text" for="search">Chercher:</label>
                            <div class="input-group">
                                <input type="text" id="search" class="form-control search-field product-search-field" dir="ltr" value="" name="s" placeholder="Chercher un produit" />
                                <div class="input-group-addon search-categories popover-header">
                                    <select name='product_cat' id='product_cat' class='postform resizeselect'>
                                        <option value='0' selected='selected'>Categories</option>
                                        @foreach($categories as $cat )
                                            <option class="level-0" value="television">{{$cat->title}}</option>
                                       @endforeach
                                    </select>
                                </div>
                                <!-- .input-group-addon -->
                                <div class="input-group-btn input-group-append">
                                    <input type="hidden" id="search-param" name="post_type" value="product" />
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-search"></i>
                                        <span class="search-btn">Chercher</span>
                                    </button>
                                </div>
                                <!-- .input-group-btn -->
                            </div>
                            <!-- .input-group -->
                        </form>
                        <!-- .navbar-search -->
                        <ul class="header-compare nav navbar-nav">
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="tm tm-compare"></i>
                                    <span id="top-cart-compare-count" class="value">3</span>
                                </a>
                            </li>
                        </ul>
                        <!-- .header-compare -->
                        <ul class="header-wishlist nav navbar-nav">
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="tm tm-favorites"></i>
                                    <span id="top-cart-wishlist-count" class="value">3</span>
                                </a>
                            </li>
                        </ul>
                        <!-- .header-wishlist -->
                        <ul id="site-header-cart" class="site-header-cart menu">
                            <li class="animate-dropdown dropdown ">
                                <a class="cart-contents" href="cart.html" data-toggle="dropdown" title="View your shopping cart">
                                    <i class="tm tm-shopping-bag"></i>
                                    <span class="count">2</span>
                                    <span class="amount">
                                        <span class="price-label">Your Cart</span>&#036;136.99</span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-mini-cart">
                                    <li>
                                        <div class="widget woocommerce widget_shopping_cart">
                                            <div class="widget_shopping_cart_content">
                                                <ul class="woocommerce-mini-cart cart_list product_list_widget ">
                                                    <li class="woocommerce-mini-cart-item mini_cart_item">
                                                        <a href="#" class="remove" aria-label="Remove this item" data-product_id="65" data-product_sku="">×</a>
                                                        <a href="single-product-sidebar.html">
                                                            <img src="{{ URL::asset('assets/frontend2/images/products/mini-cart1.jpg')}}" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="">XONE Wireless Controller&nbsp;
                                                        </a>
                                                        <span class="quantity">1 ×
                                                            <span class="woocommerce-Price-amount amount">
                                                                <span class="woocommerce-Price-currencySymbol">$</span>64.99</span>
                                                        </span>
                                                    </li>
                                                    <li class="woocommerce-mini-cart-item mini_cart_item">
                                                        <a href="#" class="remove" aria-label="Remove this item" data-product_id="27" data-product_sku="">×</a>
                                                        <a href="single-product-sidebar.html">
                                                            <img src="{{ URL::asset('assets/frontend2/images/products/mini-cart2.jpg')}}" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="">Gear Virtual Reality 3D with Bluetooth Glasses&nbsp;
                                                        </a>
                                                        <span class="quantity">1 ×
                                                            <span class="woocommerce-Price-amount amount">
                                                                <span class="woocommerce-Price-currencySymbol">$</span>72.00</span>
                                                        </span>
                                                    </li>
                                                </ul>
                                                <!-- .cart_list -->
                                                <p class="woocommerce-mini-cart__total total">
                                                    <strong>Subtotal:</strong>
                                                    <span class="woocommerce-Price-amount amount">
                                                        <span class="woocommerce-Price-currencySymbol">$</span>136.99</span>
                                                </p>
                                                <p class="woocommerce-mini-cart__buttons buttons">
                                                    <a href="cart.html" class="button wc-forward">View cart</a>
                                                    <a href="checkout.html" class="button checkout wc-forward">Checkout</a>
                                                </p>
                                            </div>
                                            <!-- .widget_shopping_cart_content -->
                                        </div>
                                        <!-- .widget_shopping_cart -->
                                    </li>
                                </ul>
                                <!-- .dropdown-menu-mini-cart -->
                            </li>
                        </ul>
                        <!-- .site-header-cart -->
                    </div>