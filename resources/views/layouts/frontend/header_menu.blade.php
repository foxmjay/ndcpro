<div class="stretched-row">
        <div class="col-full">
            <div class="row">
                <nav id="navbar-primary" class="navbar-primary" aria-label="Navbar Primary" data-nav="flex-menu">
                    <ul id="menu-navbar-primary" class="nav yamm">
                        
   
@foreach($menuList as $mlist )
    @php
    $subm = \App\Menu::getMenu($mlist->id);
    @endphp

    @if(count($subm)<=0)
    <li class="menu-item animate-dropdown">
            <a title="{{$mlist->title}}" href="{{$mlist->url}}">{{$mlist->title}}</a>
    </li>
    @else 
    <li class="yamm-fw menu-item menu-item-has-children animate-dropdown dropdown">
        <a title="Pages" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true" href="#">{{$mlist->title}} <span class="caret"></span></a>
        <ul role="menu" class=" dropdown-menu">
            <li class="menu-item menu-item-object-static_block animate-dropdown">
                <div class="yamm-content">
                    <div class="tm-mega-menu">
                        @foreach($subm as $sub)
                            @php
                                $subm2 = \App\Menu::getMenu($sub->id);
                            @endphp

                            @if(count($subm2)>0)
                                <div class="widget widget_nav_menu">
                                    <ul class="menu">
                                         @if($sub->type == 'Link')
                                            <li class="nav-title menu-item">
                                                <a href="{{$sub->url}}" style="color: #bb2131">{{$sub->title}}</a>
                                            </li>
                                        @elseif($sub->type == 'Group')
                                            <li class="nav-title menu-item">
                                                <a href="{{'/list/'.$sub->group_id}}" style="color: #bb2131">{{$sub->title}}</a>
                                            </li>
                                        @else
                                            <li class="nav-title menu-item">
                                                <a href="{{ '/menutags/'.$sub->id}}" style="color: #bb2131">{{$sub->title}}</a>
                                            </li>
                                        @endif


                                        @foreach($subm2 as $sub2)
                                            @if($sub2->type == 'Link')
                                                <li class="menu-item">
                                                    <a href="{{$sub2->url}}">{{$sub2->title}}</a>
                                                </li>
                                            @elseif($sub2->type == 'Group')
                                                <li class="menu-item">
                                                    <a href="{{'/list/'.$sub2->group_id}}">{{$sub2->title}}</a>
                                                </li>
                                            @else
                                                <li class="menu-item">
                                                    <a href="{{ '/menutags/'.$sub2->id}}">{{$sub2->title}}</a>
                                                </li>
                                            @endif
                                            
                                        @endforeach
                                    </ul>
                                    
                                    <!-- .menu -->
                                </div>
                            @else

                                    <div class="widget widget_nav_menu">
                                        <ul class="menu">
                                            @if($sub->type == 'Link')
                                                <li class="nav-title menu-item">
                                                    <a href="{{$sub->url}}" style="color: #bb2131">{{$sub->title}}</a>
                                                </li>
                                            @elseif($sub->type == 'Group')
                                                <li class="nav-title menu-item">
                                                    <a href="{{'/list/'.$sub->group_id}}" style="color: #bb2131">{{$sub->title}}</a>
                                                </li>
                                            @else
                                                <li class="nav-title menu-item">
                                                    <a href="{{ '/menutags/'.$sub->id}}" style="color: #bb2131">{{$sub->title}}</a>
                                                </li>
                                            @endif
                                        </ul>
                                        
                                        <!-- .menu -->
                                    </div>

                            @endif
                        @endforeach
                        
                        <!-- .widget_nav_menu -->
                    </div>
                    <!-- .tm-mega-menu -->
                </div>
                <!-- .yamm-content -->
            </li>
            <!-- .menu-item -->
        </ul>
        <!-- .dropdown-menu -->
    </li>
    @endif

    @endforeach
                    <!-- .nav -->
                </nav>
                <!-- .navbar-primary -->
            </div>
            <!-- .row -->
        </div>
        <!-- .col-full -->
    </div>
    <!-- .stretched-row -->