<!DOCTYPE html>
<html lang="en-US" itemscope="itemscope" itemtype="http://schema.org/WebPage">
    <head>

        <title>NDCPRO MAROC{{ isset($groupDesc) ? ' -- '.$groupDesc->title : ''  }} Maroc</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <meta name="title" content="NDCPRO MAROC{{ isset($groupDesc) ? ' -- '.$groupDesc->title : ''  }} Maroc">
    @if (isset($groupDesc))
        <meta name="description" content="{{$groupDesc->description}}">
    @else
        <meta name="description" content="Spécialiste dans le domaine de la Sécurité informatique,VOIP et technologie de visioconférence ,NDCPRO MAROC vous propose les meilleurs Offre prix/qualité qui existe sur le Marché avec les leaders Avaya, Bosh, GrandStream...">
    @endif
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://ndcpromaroc.com/">
    <meta property="og:title" content="NDCPRO MAROC">
    @if (isset($groupDesc))
        <meta name="og:description" content="{{$groupDesc->description}}">
    @else
        <meta property="og:description"  content="Spécialiste dans le domaine de la Sécurité informatique,VOIP et technologie de visioconférence ,NDCPRO MAROC vous propose les meilleurs Offre prix/qualité qui existe sur le Marché avec les leaders Avaya, Bosh, GrandStream...">
    @endif

   
    <meta property="og:image" content="{{ URL::asset('assets/images/logoSC.png')}}">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="https://ndcpromaroc.com/">
    <meta property="twitter:title" content="NDCPRO MAROC">
    <meta property="twitter:description"
        content="Spécialiste dans le domaine de la Sécurité informatique,VOIP et technologie de visioconférence ,NDCPRO MAROC vous propose les meilleurs Offre prix/qualité qui existe sur le Marché avec les leaders Avaya, Bosh, GrandStream...">
    <meta property="twitter:image" content="{{ URL::asset('assets/images/logoSC.png')}}">
    <meta name="Keywords"
        content="Communication unifié et téléphonie,Contrôle d’accès suprema,Courant faible  et infrastructure,Detection d’incendie bosh,Développement informatique,écrans interactifs,électricité,équipements des  salles de conférences Bosch,Extra & accessoires,Grandstream,Logitech,Matériels Informatiques,OneDoc,OneCar,OneTeeth,Peplink,Polycom,projection sans fil,Rédaction,traduction Bosch,Réseaux informatiques,Scopia,Sécurité informatique,Solution de Centre de Contact,Sonorisation,bosh,Stockage NAS,Téléviseurs,Ubiquiti,ViewSonic,ViewSync,Visioconférence , salle de réunion,Watchguard,Wi-Fi centralisé,Yealink,Alcatel,Aménagement des salles serveurs,Aruba,Avaya,Avaya Call Reporting,Câblage informatique et téléphonique,Camera  de surveillance,Casques téléphoniques,Cisco,Clickshare de barco,Clickshare,Detection d’incendie,Detection d’incendie bosh,écran">


        <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/frontend2/css/bootstrap.min.css')}}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/frontend2/css/font-awesome.min.css')}}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/frontend2/css/bootstrap-grid.min.css')}}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/frontend2/css/bootstrap-reboot.min.css')}}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/frontend2/css/font-techmarket.css')}}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/frontend2/css/slick.css')}}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/frontend2/css/techmarket-font-awesome.css')}}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/frontend2/css/slick-style.css')}}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/frontend2/css/animate.min.css')}}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/frontend2/css/style.css')}}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/frontend2/css/colors/blue.css')}}" media="all" />
        
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,900" rel="stylesheet">
        <link rel="shortcut icon" href="{{ URL::asset('assets/frontend2/images/favicon.png')}}">


        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-178694203-2">
        </script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-178694203-2');
        </script>


        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '462488768246455');
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=462488768246455&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->
    </head>
    <body class="woocommerce-active single-product full-width normal">
        <div id="page" class="hfeed site">
            <div class="top-bar top-bar-v10">
                <div class="col-full">
                    <ul id="menu-top-bar-left" class="nav menu-top-bar-left">
                        <li class="menu-item animate-dropdown">
                            <a title="TechMarket eCommerce - Always free delivery" href="/">Acceuil</a>
                        </li>
                        <li class="menu-item animate-dropdown">
                            <a title="Quality Guarantee of products" href="#">Conditions General</a>
                        </li>
                        <li class="menu-item animate-dropdown">
                            <a title="Fast returnings program" href="/aboutus">A propos de nous</a>
                        </li>
                        <li class="menu-item animate-dropdown">
                            <a title="No additional fees" href="/contact">Contact</a>
                        </li>
                    </ul>
                    <!-- .nav 
                    <ul id="menu-top-bar-right" class="nav menu-top-bar-right">
                        <li class="hidden-sm-down menu-item animate-dropdown">
                            <a title="Track Your Order" href="track-your-order.html">
                                <i class="tm tm-order-tracking"></i>Track Your Order</a>
                        </li>
                        <li class="menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Dollar (US)" data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <i class="tm tm-dollar"></i>Dollar (US)
                                <span class="caret"></span>
                            </a>
                            <ul role="menu" class="dropdown-menu">
                                <li class="menu-item animate-dropdown">
                                    <a title="AUD" href="#">AUD</a>
                                </li>
                                <li class="menu-item animate-dropdown">
                                    <a title="INR" href="#">INR</a>
                                </li>
                                <li class="menu-item animate-dropdown">
                                    <a title="AED" href="#">AED</a>
                                </li>
                                <li class="menu-item animate-dropdown">
                                    <a title="SGD" href="#">SGD</a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item">
                            <a title="My Account" href="login-and-register.html">
                                <i class="tm tm-login-register"></i>Register or Sign in</a>
                        </li>
                    </ul>
                    .nav -->
                </div>
                <!-- .col-full -->
            </div>
            <!-- .top-bar-v2 -->
            <header id="masthead" class="site-header header-v10" style="background-image: none; ">
                <div class="col-full desktop-only">
                    <div class="techmarket-sticky-wrap">
                        <div class="row">
                            <div class="site-branding">
                            <a href="/" class="custom-logo-link" rel="home">
                                    <img src="{{ URL::asset('assets/frontend2/images/logo-large.png')}}">
                                </a>
                                <!-- /.custom-logo-link -->
                            </div>
                            <!-- /.site-branding -->
                            <!-- ============================================================= End Header Logo ============================================================= -->
                            @include('layouts.frontend.side_menu')

                            <!-- .departments-menu -->
                             <!-- .departments-menu -->
                             <form class="navbar-search" method="get" action="/search">
                                {{ csrf_field() }}
                                <label class="sr-only screen-reader-text" for="search">Chercher:</label>
                                <div class="input-group">
                                    <input type="text" id="keyqord" class="form-control search-field product-search-field" dir="ltr" value="" name="keyword" placeholder="Chercher un produit" />
                                    <div class="input-group-addon search-categories popover-header">
                                        <select name='categorie' id='categorie' class='postform resizeselect'>
                                            <option value='-1' selected='selected'>Tout les Categories</option>
                                            @foreach( App\Menu::getMenu(-1) as $catParent)
                                                @php
                                                    $subCat = App\Menu::getMenu($catParent->id);
                                                @endphp
                                                    <option value='{{$catParent->id}}'>{{$catParent->title}}</option>
                                                @foreach($subCat as $item)
                                                    <option value='{{$item->id}}'>-- {{$item->title}}</option>
                                                    @php
                                                        $subCat2 = App\Menu::getMenu($item->id);
                                                     @endphp
                                                     @foreach($subCat2 as $item2)
                                                        <option value='{{$item2->id}}'>---- {{$item2->title}}</option>
                                                     @endforeach

                                                @endforeach
                                                
                                            @endforeach
                                        </select>
                                    </div>
                                    <!-- .input-group-addon -->
                                    <div class="input-group-btn input-group-append">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fa fa-search"></i>
                                            <span class="search-btn">Chercher</span>
                                        </button>
                                    </div>
                                    <!-- .input-group-btn -->
                                </div>
                                <!-- .input-group -->
                            </form>
                            <!-- .navbar-search 
                            <ul class="header-compare nav navbar-nav">
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        <i class="tm tm-compare"></i>
                                        <span id="top-cart-compare-count" class="value">4</span>
                                    </a>
                                </li>
                            </ul>-->
                            <!-- .header-compare 
                            <ul class="header-wishlist nav navbar-nav">
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        <i class="tm tm-favorites"></i>
                                        <span id="top-cart-wishlist-count" class="value">3</span>
                                    </a>
                                </li>
                            </ul>-->

                               <!-- .header-wishlist -->
                               <ul id="site-header-cart" class="site-header-cart menu">
                                    <li class="animate-dropdown dropdown ">
                                        <a class="cart-contents" href="cart.html" data-toggle="dropdown" title="View your shopping cart">
                                            <i class="tm tm-shopping-bag"></i>
                                            <span class="count">@if( Session::has('cart')) {{count(Session::get('cart'))}} @else {{0}}@endif </span>
                                            <span class="amount">
                                                <!--<span class="price-label">Panier</span>&#036;136.99</span>-->
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-mini-cart">
                                            <li>
                                                <div class="widget woocommerce widget_shopping_cart">
                                                    <div class="widget_shopping_cart_content">
    
                                                        <ul class="woocommerce-mini-cart cart_list product_list_widget ">
                                                        @if( Session::has('cart'))
                                                            @foreach(Session::get('cart') as $cart)
                                                            <li class="woocommerce-mini-cart-item mini_cart_item">
                                                                <a href="/remove_produit/{{$cart['product']->id}}" class="remove" aria-label="Remove this item" data-product_id="65" data-product_sku="">×</a>
                                                                <a href="single-product-sidebar.html">
                                                                    <img src="{{ URL::asset('storage/'.$cart['product']->image1)}}" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="">{{$cart['product']->title}}&nbsp;
                                                                </a>
                                                                <span class="quantity">{{$cart['quantity']}} × 
                                                                    <span class="woocommerce-Price-amount amount">
                                                                        <span class="woocommerce-Price-currencySymbol">
                                                                        @if($cart['product']->promotion)
                                                                            </span>{{ $cart['product']->price_promotion}} DH</span>
                                                                        @else
                                                                            </span>{{$cart['product']->price}} DH</span>
                                                                        @endif
                                                                </span>
                                                            </li>
                                                            @endforeach
    
                                                        </ul>
                                                        <p class="woocommerce-mini-cart__total total">
                                                            <strong>Total:</strong>
                                                            <span class="woocommerce-Price-amount amount">
                                                                <span class="woocommerce-Price-currencySymbol"></span>{{ Session::get('total_price') }} DH</span>
                                                        </p>
                                                        @endif

                                                        @if( Session::has('cart'))
                                                            @if(count(Session::get('cart')) > 0)
                                                            <p class="woocommerce-mini-cart__buttons buttons">
                                                                <a href="/contact_produit" class="button wc-forward">Accéder au panier</a>
                                                                <!--<a href="checkout.html" class="button checkout wc-forward">Checkout</a>-->
                                                            </p>
                                                            @else
                                                                <p class="woocommerce-mini-cart__buttons buttons">
                                                                        Aucun produit dans le panier
                                                                </p>
                                                            @endif
                                                        @else
                                                            <p class="woocommerce-mini-cart__buttons buttons">
                                                                    Aucun produit dans le panier
                                                            </p>
                                                        @endif
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <!--dropdown-menu-mini-cart -->
                                    </li>
                                </ul>
                                <!-- .site-header-cart -->
                            
                                </li>
                            </ul>
                            <!-- .site-header-cart -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.techmarket-sticky-wrap -->
                
                    @include('layouts.frontend.header_menu')


                </div>
                <!-- .col-full -->
                <div class="col-full handheld-only">
                    <div class="handheld-header">
                        <div class="row">
                            <div class="site-branding">
                                <a href="/" class="custom-logo-link" rel="home">
                                    <a href="/" class="custom-logo-link" rel="home">
                                        <img src="{{ URL::asset('assets/frontend2/images/logo-large.png')}}">
                                    </a>
                                </a>
                                <!-- /.custom-logo-link -->
                            </div>
                            <!-- /.site-branding -->
                            <!-- ============================================================= End Header Logo ============================================================= -->
                            <div class="handheld-header-links">
                                <ul class="columns-3">
                                    <!--<li class="my-account">
                                        <a href="login-and-register.html" class="has-icon">
                                            <i class="tm tm-login-register"></i>
                                        </a>
                                    </li>-->
                                    <li class="wishlist">
                                        <a href="#" class="has-icon">
                                            <i class="tm tm-favorites"></i>
                                            <span class="count">3</span>
                                        </a>
                                    </li>
                                    <li class="compare">
                                        <a href="#" class="has-icon">
                                            <i class="tm tm-compare"></i>
                                            <span class="count">3</span>
                                        </a>
                                    </li>
                                </ul>
                                <!-- .columns-3 -->
                            </div>
                            <!-- .handheld-header-links -->
                        </div>
                        <!-- /.row -->

                        @include('layouts.frontend.side_menu_mobile')

                        <!-- .techmarket-sticky-wrap -->
                    </div>
                    <!-- .handheld-header -->
                </div>
                <!-- .handheld-only -->
            </header>
            <!-- .header-v10 -->
            <!-- ============================================================= Header End ============================================================= -->
            @yield('content')
            <!-- #content -->
            @include('layouts.frontend.footer')

        </div>
        
        <script type="text/javascript" src="{{ URL::asset('assets/frontend2/js/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/frontend2/js/tether.min.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/frontend2/js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/frontend2/js/jquery-migrate.min.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/frontend2/js/hidemaxlistitem.min.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/frontend2/js/jquery-ui.min.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/frontend2/js/hidemaxlistitem.min.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/frontend2/js/jquery.easing.min.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/frontend2/js/scrollup.min.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/frontend2/js/jquery.waypoints.min.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/frontend2/js/waypoints-sticky.min.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/frontend2/js/pace.min.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/frontend2/js/slick.min.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/frontend2/js/scripts.js')}}"></script>
       
    </body>
</html>
