

<nav id="primary-navigation" class="primary-navigation" aria-label="Primary Navigation" data-nav="flex-menu">
    <ul id="menu-primary-menu" class="nav yamm">


        <!--
        <li class="menu-item menu-item-has-children animate-dropdown dropdown">
            <a title="Mother`s Day" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true" href="#">Mother`s Day <span class="caret"></span></a>
            <ul role="menu" class=" dropdown-menu">
                <li class="menu-item animate-dropdown">
                    <a title="Wishlist" href="wishlist.html">Wishlist</a>
                </li>
                <li class="menu-item animate-dropdown">
                    <a title="Add to compare" href="compare.html">Add to compare</a>
                </li>
                <li class="menu-item animate-dropdown">
                    <a title="About Us" href="about.html">About Us</a>
                </li>
                <li class="menu-item animate-dropdown">
                    <a title="Track Order" href="track-your-order.html">Track Order</a>
                </li>
            </ul>
            
        </li> -->

        <li class="menu-item animate-dropdown">
                <a title="Logitech Sale" href="#">test</a>
        </li>
        <li class="menu-item animate-dropdown">
                <a title="Logitech Sale" href="#">test</a>
        </li>
        <li class="menu-item animate-dropdown">
                <a title="Logitech Sale" href="#">test</a>
        </li>

    @foreach($menuList as $mlist )
        @php
        $subm = \App\Menu::getMenu($mlist->id);
        @endphp
    
        @if(count($subm)<=0)
        <li class="menu-item animate-dropdown">
                <a title="Logitech Sale" href="{{$mlist->url}}">{{$mlist->title}}</a>
        </li>
        @else 
        <li class="yamm-fw menu-item menu-item-has-children animate-dropdown dropdown">
            <a title="Pages" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true" href="#">{{$mlist->title}} <span class="caret"></span></a>
            <ul role="menu" class=" dropdown-menu">
                <li class="menu-item menu-item-object-static_block animate-dropdown">
                    <div class="yamm-content">
                        <div class="tm-mega-menu">
                            @foreach($subm as $sub)
                                @php
                                    $subm2 = \App\Menu::getMenu($sub->id);
                                @endphp

                                @if(count($subm2)>0)
                                    <div class="widget widget_nav_menu">
                                        <ul class="menu">
                                            <li class="nav-title menu-item">
                                                <a href="#">{{$sub->title}}</a>
                                            </li>
                                            @foreach($subm2 as $sub2)
                                                @if($sub2->type == 'Link')
                                                    <li class="menu-item">
                                                        <a href="{{$sub2->url}}">{{$sub2->title}}</a>
                                                    </li>
                                                @elseif($sub2->type == 'Group')
                                                    <li class="menu-item">
                                                        <a href="{{'/frontend3/list/'.$sub2->group_id}}">{{$sub2->title}}</a>
                                                    </li>
                                                @else
                                                    <li class="menu-item">
                                                        <a href="{{ '/frontend3/menutags/'.$sub2->id}}">{{$sub2->title}}</a>
                                                    </li>
                                                @endif
                                                
                                            @endforeach
                                        </ul>
                                        
                                        <!-- .menu -->
                                    </div>
                                @endif
                            @endforeach
                            
                            <!-- .widget_nav_menu -->
                        </div>
                        <!-- .tm-mega-menu -->
                    </div>
                    <!-- .yamm-content -->
                </li>
                <!-- .menu-item -->
            </ul>
            <!-- .dropdown-menu -->
        </li>
        @endif

        @endforeach

        <li class="menu-item animate-dropdown">
                <a title="Logitech Sale" href="product-category.html">A
                        propos de nous</a>
        </li>
                
        <li class="sale-clr yamm-fw menu-item animate-dropdown">
                <a title="Super deals" href="/frontend3/contact">Contact</a>
        </li>

        <li class="techmarket-flex-more-menu-item dropdown">
            <a title="..." href="#" data-toggle="dropdown" class="dropdown-toggle">...</a>
            <ul class="overflow-items dropdown-menu"></ul>
            
        </li>
    </ul>
    <!-- .nav -->
</nav>