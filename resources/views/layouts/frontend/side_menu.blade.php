<div id="departments-menu" class="dropdown departments-menu">
    <button class="btn dropdown-toggle btn-block" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="tm tm-departments-thin"></i>
        <span>All Departments</span>
    </button>
    <ul id="menu-departments-menu" class="dropdown-menu yamm departments-menu-dropdown">
        <!--
        <li class="highlight menu-item animate-dropdown">
            <a title="Value of the Day" href="home-v2.html">Value of the Day</a>
        </li>
        <li class="highlight menu-item animate-dropdown">
            <a title="Top 100 Offers" href="home-v3.html">Top 100 Offers</a>
        </li>
        <li class="highlight menu-item animate-dropdown">
            <a title="New Arrivals" href="home-v4.html">New Arrivals</a>
        </li>-->


            @foreach($menuList as $mlist )
                @php
                    $subm = \App\Menu::getMenu($mlist->id);
                @endphp

                @if(count($subm)<=0)
                    <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown-submenu">
                        <a title="Computers &amp; Laptops" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true" href="#">{{$mlist->title}}<span class="caret"></span></a>
                @else

                <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown-submenu">
                    <a title="Computers &amp; Laptops" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true" href="#">{{$mlist->title}}<span class="caret"></span></a>
                    <ul role="menu" class=" dropdown-menu">
                        <li class="menu-item menu-item-object-static_block animate-dropdown">
                            <div class="yamm-content">
                                <div class="bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                    <div class="kc-col-container">
                                        <div class="kc_single_image">
                                           <!-- <img src="{{ URL::asset('assets/frontend2/images/megamenu.jpg')}}" class="" alt="" />-->
                                        </div>
                                        <!-- .kc_single_image -->
                                    </div>
                                    <!-- .kc-col-container -->
                                </div>
                                <!-- .bg-yamm-content -->
                                <div class="row yamm-content-row">

                                @foreach($subm as $sub)
                                    @php
                                        $subm2 = \App\Menu::getMenu($sub->id);
                                    @endphp
                                    <div class="col-md-6 col-sm-12">
                                        <div class="kc-col-container">
                                            <div class="kc_text_block">
                                                <ul>
                                                         @if($sub->type == 'Link')
                                                            <li class="nav-title">
                                                                <a href="{{$sub->url}}" style="color: #bb2131">{{$sub->title}}</a>
                                                            </li>
                                                        @elseif($sub->type == 'Group')
                                                            <li class="nav-title">
                                                                <a href="{{'/list/'.$sub->group_id}}" style="color: #bb2131">{{$sub->title}}</a>
                                                            </li>
                                                        @else
                                                            <li class="nav-title">
                                                                <a href="{{ '/menutags/'.$sub->id}}" style="color: #bb2131">{{$sub->title}}</a>
                                                            </li>
                                                        @endif
                                                    @if( count($subm2) > 0 )
                                 
                                                        @if($sub->type == 'Link')
                                                            <li class="nav-title">
                                                                <a href="{{$sub->url}}" style="color: #bb2131">{{$sub->title}}</a>
                                                            </li>
                                                        @elseif($sub->type == 'Group')
                                                            <li class="nav-title">
                                                                <a href="{{'/list/'.$sub->group_id}}" style="color: #bb2131">{{$sub->title}}</a>
                                                            </li>
                                                        @else
                                                            <li class="nav-title">
                                                                <a href="{{ '/menutags/'.$sub->id}}" style="color: #bb2131">{{$sub->title}}</a>
                                                            </li>
                                                        @endif

                                                        @foreach($subm2 as $sub2)
                                                            @if($sub2->type == 'Link')
                                                            <li >
                                                                <a href="{{$sub2->url}}">{{$sub2->title}}</a>
                                                            </li>
                                                            @elseif($sub2->type == 'Group')
                                                                <li >
                                                                    <a href="{{'/list/'.$sub2->group_id}}">{{$sub2->title}}</a>
                                                                </li>
                                                            @else
                                                                <li >
                                                                    <a href="{{ '/menutags/'.$sub2->id}}">{{$sub2->title}}</a>
                                                                </li>
                                                            @endif
                                                            
                                                        @endforeach
                                                    
                                                    @endif

                                                    
                                                
                                                
                                                    <li class="nav-divider"></li>
                                                    
                                                    <!--<li>
                                                        <a href="#">
                                                            <span class="nav-text">Voir tout</span>
                                                            <span class="nav-subtext">Decouvrir plus de produits</span>
                                                        </a>
                                                    </li>-->
                                                </ul>
                                            </div>
                                        <!-- .kc_text_block -->
                                        </div>
                                    <!-- .kc-col-container -->
                                    </div>

                                    @endforeach
                                </div>
                            <!-- .kc_row -->
                        </div>
                            <!-- .yamm-content -->
                        </li>
                    </ul>
                </li>
                @endif
            @endforeach
        
        <!--<li class="menu-item menu-item-type-custom animate-dropdown">
            <a title="Gadgets" href="landing-page-v1.html">Gadgets</a>
        </li>
        <li class="menu-item menu-item-type-custom animate-dropdown">
            <a title="Virtual Reality" href="landing-page-v2.html">Virtual Reality</a>
        </li>-->
    </ul>
</div>