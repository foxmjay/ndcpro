<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NDCPRO MAROC</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet"
        href="{{ URL::asset('assets/admin/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/Ionicons/css/ionicons.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::asset('assets/admin/dist/css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ URL::asset('assets/admin/dist/css/skins/_all-skins.min.css')}}">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/morris.js/morris.css')}}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/jvectormap/jquery-jvectormap.css')}}">
    <!-- Date Picker
  <link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">-->
    <!-- Daterange picker
  <link rel="stylesheet" href="{{ URL::asset('assets/admin/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">-->
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet"
        href="{{ URL::asset('assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

    <link rel="stylesheet"
        href="{{ URL::asset('assets/admin/bower_components/datetimepicker/jquery.datetimepicker.min.css')}}" />

    <link rel="shortcut icon" type="image/png" href="{{ URL::asset('assets/images/favicon.png')}}" />

    <link rel="stylesheet" href="{{ URL::asset('assets/admin/plugins/loader/loader-default.css')}}">

    <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/flick/jquery-ui.css">
    <link href="{{ URL::asset('assets/admin/plugins/tag-it/jquery.tagit.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="{{ URL::asset('assets/admin/plugins/iCheck/all.css')}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <style>
        .table td {
            text-align: center;
        }

        .table th {
            text-align: center;
        }
    </style>
    @yield('css')

</head>

<body class="hold-transition skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->
            <a href="/" class="logo" style="background-color: #3C8DBC">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>N</b>P</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"> <img src="{{ URL::asset('assets/images/logo-large.png')}}"> </span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
                      
                        <li class="dropdown messages-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-envelope-o"></i>
                                <span class="label label-success"></span>
                            </a>
                            <ul class="dropdown-menu">


                                <li class="header">Messages</li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                      
                                        <li>
                                            <!-- start message -->
                                            <a href="">
                                                <div class="pull-left">
                                                   
                                                </div>
                                                <h4>
                                                 

                                                </h4>
                                                <p></p>
                                            </a>
                                        </li>

                                        <!-- end message -->
                                    </ul>
                                </li>
                                <li class="footer"><a href="#">See All Messages</a></li>
                            </ul>
                        </li>
                        <!-- Notifications: style can be found in dropdown.less -->
                       
                        <!-- Tasks: style can be found in dropdown.less -->
                       
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="{{ URL::asset('assets/admin/dist/img/user.png')}}" class="user-image"
                                    alt="User Image">
                                <span class="hidden-xs">{{ Auth::user()->name }}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="{{ URL::asset('assets/admin/dist/img/user.png')}}" class="img-circle"
                                        alt="User Image">

                                    <p>
                                        {{ Auth::user()->name }}

                                        <small>  </small>
                                    </p>
                                </li>
                                <!-- Menu Body -->
                                <li class="user-body">
                                    <div class="row">
                                       
                                      <!--  <div class="col-xs-4 text-center">
                                            <a href="#">Parameters</a>
                                        </div>
                                      
                                        <div class="col-xs-4 text-center">
                                            <a href="#">Options</a>
                                        </div>
                                      
                                        <div class="col-xs-4 text-center">
                                          
                                            <a href="">Options</a>
                                        

                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <a href="#">Options</a>
                                        </div>-->
                                    </div>
                                    <!-- /.row -->
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <!--<div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                                    </div>-->
                                    <div class="pull-right">
                                        <a class="btn btn-default btn-flat" href="{{ route('logout') }}" onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                                            <i class="icon-logout menu-icon"></i>
                                            <span class="menu-title">Deconnexion</span>
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                            style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <!-- Control Sidebar Toggle Button
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>-->
                    </ul>
                </div>
            </nav>
        </header>

        <!-- =============================================== -->

        <!-- Left side column. contains the sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="{{ URL::asset('assets/admin/dist/img/user.png')}}" class="img-circle"
                            alt="User Image">
                    </div>
                    <div class="pull-left info">
                        <p>{{ Auth::user()->name }}</p>
                        <a href="#"><i class="fa fa-circle text-success"></i>  </a>
                    </div>
                </div>
              
              
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">MENU</li>
                    <li class="treeview">
                       
                
                    <li class="{{ Request::is('home') ? 'active' : '' }}"><a href="/dashboard/home">
                       <i class="fa fa-street-view"></i>  <span>Home</span></a>
                    </li>

                    <li class="{{ Request::is('menus') ? 'active' : '' }}"><a href="/dashboard/menus/-1">
                       <i class="fa fa-street-view"></i>  <span>Menus</span></a>
                    </li>

                    <li class="{{ Request::is('groups') ? 'active' : '' }}"><a href="/dashboard/groups/">
                       <i class="fa fa-street-view"></i>  <span>Groups</span></a>
                    </li>

                    <li class="{{ Request::is('banners') ? 'active' : '' }}"><a href="/dashboard/banners/">
                       <i class="fa fa-street-view"></i>  <span>Banners</span></a>
                    </li>

                    <li class="{{ Request::is('tags') ? 'active' : '' }}"><a href="/dashboard/tags/">
                       <i class="fa fa-street-view"></i>  <span>Tags</span></a>
                    </li>

                    <li class="{{ Request::is('categories') ? 'active' : '' }}"><a href="/dashboard/categories/-1">
                       <i class="fa fa-street-view"></i>  <span>Categories</span></a>
                    </li>

                    <li class="{{ Request::is('messages') ? 'active' : '' }}"><a href="/dashboard/messages">
                       <i class="fa fa-street-view"></i>  <span>Messages</span>
                       <span class="pull-right-container">
                            <small class="label pull-right bg-blue">{{ App\Message::getMessagesCount()}}</small>
                        </span>
                       </a>
                    </li>

                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- =============================================== -->
        <div class="loader loader-default" id="loader-spinner"></div>

        <div style="display: flex; justify-content: center;">


        @if(Session::has('success'))
            <div class="alert alert-success">
                <span class="glyphicon glyphicon-ok"></span>
                {!! session('success') !!}
                <button type="button" class="close" data-dismiss="alert" aria-label="close">
                    <span aria-hidden="true">&times;</span>
                </button>
    
            </div>
        @endif

        @if(Session::has('error'))
            <div class="alert alert-danger">
                <span class="glyphicon glyphicon-remove"></span>
                {!! session('error') !!}
                <button type="button" class="close" data-dismiss="alert" aria-label="close">
                    <span aria-hidden="true">&times;</span>
                </button>
    
            </div>
        @endif
        </div>

        @yield('content')

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 0.1
            </div>
            <strong>Copyright &copy; 2019 <a href="https://ndcpromaroc.com/" target="_blank">NDCPRO MAROC</a>.</strong> All rights
            reserved.
        </footer>


    </div>
    <!-- ./wrapper -->

    <!-- jQuery 3 -->
    <script src="{{ URL::asset('assets/admin/bower_components/jquery/jquery-2.1.1.min.js')}}"></script>

    <script src="{{ URL::asset('assets/admin/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ URL::asset('assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- SlimScroll -->
    <script src="{{ URL::asset('assets/admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{ URL::asset('assets/admin/bower_components/fastclick/lib/fastclick.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{ URL::asset('assets/admin/dist/js/adminlte.min.js')}}"></script>

    <script src="{{ URL::asset('assets/admin/bower_components/datetimepicker/jquery.datetimepicker.full.min.js')}}"></script>

    <script src="{{ URL::asset('assets/admin/plugins/tag-it/tag-it.js')}}"></script>
    
    <script src="{{ URL::asset('assets/admin/plugins/iCheck/icheck.min.js')}}"></script>

    <script>

        function enableSpinner(){
            $('#loader-spinner').addClass('is-active');
        }


        function disableSpinner(){
            $('#loader-spinner').removeClass('is-active');
        }

        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass   : 'iradio_minimal-blue'
        })

    </script>

    @yield('js')
</body>

</html>
