@extends('layouts.frontend.frontend')

@section('content')


<div id="content" class="site-content">
 <div class="col-full">
    <div class="row">
        <nav class="woocommerce-breadcrumb">
            <a href="home-v1.html">Home</a>
            <span class="delimiter">
                <i class="tm tm-breadcrumbs-arrow-right"></i>
            </span>
            contact
        </nav>
        <!-- .woocommerce-breadcrumb -->
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
                <div class="type-page hentry">
                    <header class="entry-header">
                        <div class="page-header-caption">
                            <h1 class="entry-title">Message Envoye</h1>
                        </div>
                    </header>
                    <!-- .entry-header -->
                    <div class="entry-content">
                        <div class="woocommerce">
                                <p>Nous vous contacterons sous peu. Merci.</p>
                        </div>
                        <!-- .woocommerce -->
                    </div>
                    <!-- .entry-content -->
                </div>
                <!-- .hentry -->
            </main>
            <!-- #main -->
        </div>
        <!-- #primary -->
    </div>
    <!-- .row -->
</div>
<!-- .col-full -->
</div>


@endsection