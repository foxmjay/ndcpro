@extends('layouts.frontend.frontend')

@section('content')
<div id="content" class="site-content">
    <div class="col-full">
        <div class="row">

            <!-- .woocommerce-breadcrumb -->
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="type-page hentry">
                        <div class="entry-content">
                            
                            <!-- .stretch-full-width-map -->
                            <div class="row contact-info">
                                <div class="col-md-9 left-col">
                                    <div class="text-block">
                                        <h2 class="contact-page-title">Contactez nous</h2>
                                        <!--<p>N'hesitez pas a nous contacter </p>-->
                                    </div>

                                    @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        @foreach($errors->all() as $err )
                                            {{$err}} <br>
                                        @endforeach
                                    </div>
                                    @endif

                                    @if(Session::has('message'))
                                        <p class="alert alert-info">{{ Session::get('message') }}</p>
                                    @endif
                                    <div class="contact-form">
                                        <div role="form" class="wpcf7" id="wpcf7-f425-o1" lang="en-US" dir="ltr">
                                            <div class="screen-reader-response"></div>
                                            <form role="form" method="POST" action="{{ url('/contact/send') }}" accept-charset="UTF-8" id="mainForm" name="contact_form" class="form-sample" required >
                                              {{ csrf_field() }}

                                                <!-- <div style="display: none;">
                                                    <input type="hidden" name="_wpcf7" value="425" />
                                                    <input type="hidden" name="_wpcf7_version" value="4.5.1" />
                                                    <input type="hidden" name="_wpcf7_locale" value="en_US" />
                                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f425-o1" />
                                                    <input type="hidden" name="_wpnonce" value="e6363d91dd" />
                                                </div>-->
                                                <div class="form-group row">
                                                    <div class="col-xs-12 col-md-6">
                                                        <label>Nom
                                                            <abbr title="required" class="required">*</abbr>
                                                        </label>
                                                        <br>
                                                        <span class="wpcf7-form-control-wrap first-name">
                                                            <input type="text" aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required input-text" size="40" value="" name="nom" id="nom" >
                                                        </span>
                                                    </div>
                                                    <!-- .col -->
                                                    <div class="col-xs-12 col-md-6">
                                                        <label>Prenom
                                                            <abbr title="required" class="required">*</abbr>
                                                        </label>
                                                        <br>
                                                        <span class="wpcf7-form-control-wrap last-name">
                                                            <input type="text" aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required input-text" size="40" value="" name="prenom" id="prenom" required>
                                                        </span>
                                                    </div>
                                                    <!-- .col -->
                                                    <div class="col-xs-12 col-md-6">
                                                        <label>Telephone
                                                            <abbr title="required" class="required">*</abbr>
                                                        </label>
                                                        <br>
                                                        <span class="wpcf7-form-control-wrap first-name">
                                                            <input type="text" aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required input-text" size="40" value="" name="tel" id="tel" required>
                                                        </span>
                                                    </div>
                                                    <!-- .col -->
                                                    <div class="col-xs-12 col-md-6">
                                                        <label>Email
                                                            <abbr title="required" class="required">*</abbr>
                                                        </label>
                                                        <br>
                                                        <span class="wpcf7-form-control-wrap last-name">
                                                            <input type="text" aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required input-text" size="40" value="" name="email" id="email" required>
                                                        </span>
                                                    </div>
                                                </div>
                                                <!-- .form-group -->
                                                <div class="form-group">
                                                    <label>Subjet
                                                            <abbr title="required" class="required">*</abbr>
                                                    </label>
                                                    <br>
                                                    <span class="wpcf7-form-control-wrap subject">
                                                        <input type="text" aria-invalid="false" class="wpcf7-form-control wpcf7-text input-text wpcf7-validates-as-required" size="40" value="" name="sujet" id="sujet" required>
                                                    </span>
                                                </div>
                                                <!-- .form-group -->
                                                <div class="form-group">
                                                    <label>Message
                                                            <abbr title="required" class="required">*</abbr>
                                                    </label>
                                                    <br>
                                                    <span class="wpcf7-form-control-wrap your-message">
                                                        <textarea aria-invalid="false" class="wpcf7-form-control wpcf7-textarea" rows="10" cols="40" name="message" id="message" required></textarea>
                                                    </span>
                                                </div>
                                                <!-- .form-group-->
                                                <div class="form-group clearfix">
                                                    <p>
                                                        <input type="submit" value="Envoyer" class="wpcf7-form-control wpcf7-submit" />
                                                    </p>
                                                </div>
                                                <!-- .form-group-->
                                                <div class="wpcf7-response-output wpcf7-display-none"></div>
                                            </form>
                                            <!-- .wpcf7-form -->
                                        </div>
                                        <!-- .wpcf7 -->
                                    </div>
                                    <!-- .contact-form7 -->
                                </div>
                                <!-- .col -->
                                <div class="col-md-3 store-info">
                                    <div class="text-block">
                                        <h2 class="contact-page-title">Adresse</h2>
                                        <address>
                                              <p>N° 10, Angle 115 rue Maamora et 55 rue Med El Amraoui Mag N°10 </p>
                                              <p>KENITRA </p>
                                         
                                             
                                        </address>
                                        <h3>Telephones</h3>
                                        <p class="inner-right-md">(+212) 641 010 222</a></p>
                                        <h3>Horaire d'ouverture</h3>
                                        <ul class="list-unstyled operation-hours inner-right-md">
                                            <li class="clearfix">
                                                <span class="day">Lundi:</span>
                                                <span class="pull-right flip hours">8h30-18h00 </span>
                                            </li>
                                            <li class="clearfix">
                                                <span class="day">Mardi:</span>
                                                <span class="pull-right flip hours">8h30-18h00</span>
                                            </li>
                                            <li class="clearfix">
                                                <span class="day">Mercredi:</span>
                                                <span class="pull-right flip hours">8h30-18h00</span>
                                            </li>
                                            <li class="clearfix">
                                                <span class="day">Jeudi:</span>
                                                <span class="pull-right flip hours">8h30-18h00</span>
                                            </li>
                                            <li class="clearfix">
                                                <span class="day">Vendredi:</span>
                                                <span class="pull-right flip hours">8h30-18h00</span>
                                            </li>
                                            <li class="clearfix">
                                                <span class="day">Samedi:</span>
                                                <span class="pull-right flip hours">8h30-12h30</span>
                                            </li>
                                            <li class="clearfix">
                                                <span class="day">Dimanche </span>
                                                <span class="pull-right flip hours">Fermé</span>
                                            </li>
                                        </ul>
                                        
                                    </div>
                                    <!-- .text-block -->
                                </div>
                                <!-- .col -->
                            </div>
                            <!-- .contact-info -->
                        </div>
                        <!-- .entry-content -->
                    </div>
                    <!-- .hentry -->
                </main>
                <!-- #main -->
            </div>
            <!-- #primary -->
        </div>
        <!-- .row -->
    </div>
    <!-- .col-full -->
</div>
<!-- #content -->


@endsection