@extends('layouts.frontend.frontend')

@section('content')

    <div id="content" class="site-content">
    <div class="col-full">
        <div class="row">
            <nav class="woocommerce-breadcrumb">
                <a href="home-v1.html">Home</a>
                <span class="delimiter">
                    <i class="tm tm-breadcrumbs-arrow-right"></i>
                </span>
                Cart
            </nav>
            <!-- .woocommerce-breadcrumb -->
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="type-page hentry">
                        <div class="entry-content">
                            <div class="woocommerce">
                                <div class="cart-wrapper">
                                    <form method="post" action="#" class="woocommerce-cart-form">
                                        <table class="shop_table shop_table_responsive cart">
                                            <thead>
                                                <tr>
                                                    <th class="product-remove">&nbsp;</th>
                                                    <th class="product-name">Produit</th>
                                                    <th class="product-price">Prix</th>
                                                    <th class="product-quantity">Quantite</th>
                                                    <th class="product-subtotal">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            @if( Session::has('cart'))
                                                @foreach(Session::get('cart') as $cart)
                                                <tr>
                                                    <td data-title="Produit" class="product-name">
                                                        <div class="media cart-item-product-detail">
                                                            <a href="#">
                                                                <img width="180" height="180" alt="" class="wp-post-image" src="{{ URL::asset('storage/'.$cart['product']->image1)}}">
                                                            </a>
                                                            <div class="media-body align-self-center">
                                                                <a href="#"> {{$cart['product']->title}}</a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td data-title="Prix" class="product-price">
                                                        <span class="woocommerce-Price-amount amount">
                                                            <span class="woocommerce-Price-currencySymbol"></span>
                                                              @php 
                                                                $price=0;
                                                                if($cart['product']->promotion)
                                                                  $price = $cart['product']->price_promotion;
                                                                else
                                                                    $price = $cart['product']->price;

                                                              @endphp
                                                             {{ $price }} DH
                                                        </span>
                                                    </td>
                                                    <td class="product-quantity" data-title="Quantite">
                                                        <div class="quantity">
                                                            <label for="quantity-input-1">Quantite</label>
                                                            <span id="quantity-input-1">{{$cart['quantity']}}</span>
                                                        </div>
                                                    </td>
                                                    <td data-title="Total" class="product-subtotal">
                                                        <span class="woocommerce-Price-amount amount">
                                                            <span class="woocommerce-Price-currencySymbol"></span>{{ $cart['quantity'] * $price }} DH
                                                        </span>
                                                        <a title="Remove this item" class="remove" href="/remove_produit/{{$cart['product']->id}}">×</a>

                                                    </td>
                                                </tr>
                                                @endforeach
                                                @endif
                                                  
                                             
                                            </tbody>
                                        </table>
                                        <!-- .shop_table shop_table_responsive -->
                                    </form>
                                    <!-- .woocommerce-cart-form -->
                                    <div class="cart-collaterals">
                                        <div class="cart_totals">

                                                <h2>Prix Total de la commande</h2>
                                                <table class="shop_table shop_table_responsive">
                                                    <tbody>
                                                        <tr class="order-total">
                                                            <th>Total</th>
                                                            <td data-title="Total">
                                                                <strong>
                                                                    <span class="woocommerce-Price-amount amount">
                                                                        <span class="woocommerce-Price-currencySymbol"></span>{{ Session::get('total_price') }}DH</span>
                                                                </strong>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            
                                            <!-- .shop_table shop_table_responsive -->
                                            <div class="wc-proceed-to-checkout">
                                                <!-- .wc-proceed-to-checkout -->
                                                <a class="checkout-button button alt wc-forward" href="/contact_produit">
                                                    Accéder au panier</a>
                                            </div>
                                            <!-- .wc-proceed-to-checkout -->
                                        </div>
                                        <!-- .cart_totals -->
                                    </div>
                                    <!-- .cart-collaterals -->
                                </div>
                                <!-- .cart-wrapper -->
                            </div>
                            <!-- .woocommerce -->
                        </div>
                        <!-- .entry-content -->
                    </div>
                    <!-- .hentry -->
                </main>
                <!-- #main -->
            </div>
            <!-- #primary -->
        </div>
        <!-- .row -->
    </div>
    <!-- .col-full -->
</div>
<!-- #content -->


@endsection