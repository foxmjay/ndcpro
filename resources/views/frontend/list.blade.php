@extends('layouts.frontend.items')

@section('content')
<div id="content" class="site-content" tabindex="-1">
    <div class="col-full">
        <div class="row">
            <nav class="woocommerce-breadcrumb">
                <a href="#">Home</a>
                <span class="delimiter">
                    <i class="tm tm-breadcrumbs-arrow-right"></i>
                </span>
                @if (isset($groupDesc))
                    <a href="#">{{$groupDesc->title}}</a>
                @else
                    <a href="#">Categorie</a>
                @endif

                <!--<span class="delimiter">
                    <i class="tm tm-breadcrumbs-arrow-right"></i>
                </span>-->
            </nav>
            <!-- .woocommerce-breadcrumb -->
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="shop-control-bar">
                        <div class="handheld-sidebar-toggle">
                            <button type="button" class="btn sidebar-toggler">
                                <i class="fa fa-sliders"></i>
                                <span>Filters</span>
                            </button>
                        </div>
                        <!-- .handheld-sidebar-toggle -->
                        @if (isset($groupDesc))
                            <h1 class="woocommerce-products-header__title page-title">{{$groupDesc->title}}</h1>
                        @else
                            <h1 class="woocommerce-products-header__title page-title">Shop</h1>
                        @endif
                        <ul role="tablist" class="shop-view-switcher nav nav-tabs">
                            <li class="nav-item">
                                <a href="#grid" title="Grid View" data-toggle="tab" class="nav-link active">
                                    <i class="tm tm-grid-small"></i>
                                </a>
                            </li>
                          
                            <li class="nav-item">
                                <a href="#list-view-large" title="List View Large" data-toggle="tab" class="nav-link ">
                                    <i class="tm tm-listing-large"></i>
                                </a>
                            </li>
                         
                            <li class="nav-item">
                                <a href="#list-view-small" title="List View Small" data-toggle="tab" class="nav-link ">
                                    <i class="tm tm-listing-small"></i>
                                </a>
                            </li>
                        </ul>
                        <!-- .shop-view-switcher 
                        <form class="form-techmarket-wc-ppp" method="POST">
                            <select class="techmarket-wc-wppp-select c-select" onchange="this.form.submit()" name="ppp">
                                <option value="20">Show 20</option>
                                <option value="40">Show 40</option>
                                <option value="-1">Show All</option>
                            </select>
                            <input type="hidden" value="5" name="shop_columns">
                            <input type="hidden" value="15" name="shop_per_page">
                            <input type="hidden" value="right-sidebar" name="shop_layout">
                        </form>-->
                        <!-- .form-techmarket-wc-ppp 
                        <form method="get" class="woocommerce-ordering">
                            <select class="orderby" name="orderby">
                                <option value="popularity">Sort by popularity</option>
                                <option value="rating">Sort by average rating</option>
                                <option selected="selected" value="date">Sort by newness</option>
                                <option value="price">Sort by price: low to high</option>
                                <option value="price-desc">Sort by price: high to low</option>
                            </select>
                            <input type="hidden" value="5" name="shop_columns">
                            <input type="hidden" value="15" name="shop_per_page">
                            <input type="hidden" value="right-sidebar" name="shop_layout">
                        </form>-->
                        <!-- .woocommerce-ordering 
                        <nav class="techmarket-advanced-pagination">
                            <form class="form-adv-pagination" method="post">
                                <input type="number" value="1" class="form-control" step="1" max="5" min="1" size="2" id="goto-page">
                            </form> of 5<a href="#" class="next page-numbers">→</a>
                        </nav>-->
                        <!-- .techmarket-advanced-pagination -->
                    </div>
                    <!-- .shop-control-bar -->
                    <div class="tab-content">
                        <div id="grid" class="tab-pane active" role="tabpanel">
                            <div class="woocommerce columns-7">
                                <div class="products">
                                    @foreach($products as $product)
                                    <div class="product first">
                                        <div class="yith-wcwl-add-to-wishlist">
                                            <a href="/item/{{$product->id}}" rel="nofollow" class="add_to_wishlist"> Add to Wishlist</a>
                                        </div>
                                        <!-- .yith-wcwl-add-to-wishlist -->
                                        <a class="woocommerce-LoopProduct-link woocommerce-loop-product__link" href="/item/{{$product->id}}">
                                            <img width="224" height="197" alt="" class="attachment-shop_catalog size-shop_catalog wp-post-image" src="{{ URL::asset('storage/'.$product->image1)}}">
                                            <span class="price">
                                                    @if($product->price_start)
                                                    <ins>
                                                        <span class="woocommerce-Price-amount amount">
                                                        <span class="woocommerce-Price-currencySymbol"></span>A Partir de {{$product->price}}DH</span>
                                                    </ins>
                                                    @else
                                                        @if($product->promotion)
                                                            <ins>
                                                                <span class="woocommerce-Price-amount amount">
                                                                        <span class="woocommerce-Price-currencySymbol"></span>{{$product->price_promotion}}DH</span>
                                                            </ins>
                                                            <del>        
                                                                    <span class="woocommerce-Price-amount amount">
                                                                    <span class="woocommerce-Price-currencySymbol"></span>{{$product->price}}DH</span>
                                                            </del>

                                                        @else    
                                                            @if($product->price !=0)   
                                                             <ins>
                                                                <span class="woocommerce-Price-amount amount">
                                                                        <span class="woocommerce-Price-currencySymbol"></span>{{$product->price}}DH</span>
                                                            </ins>
                                                            @endif

                                                        @endif
                                                    
                                                    @endif
                                                </span>
                                            <h2 class="woocommerce-loop-product__title">{{$product->title}}</h2>
                                        </a>
                                        <!-- .woocommerce-LoopProduct-link -->
                                        <div class="hover-area">
                                            <a class="button" href="/item/{{$product->id}}">Detailst</a>
                                            <!--<a class="add-to-compare-link" href="compare.html">Add to compare</a>-->
                                        </div>
                                        <!-- .hover-area -->
                                    </div>
                                    @endforeach
                                    <!-- .product -->
                                </div>
                                <!-- .products -->
                            </div>
                            <!-- .woocommerce -->
                        </div>
                       
                        <!-- .tab-pane -->
                        <div id="list-view-large" class="tab-pane" role="tabpanel">
                            <div class="woocommerce columns-1">
                                <div class="products">
                                    @foreach($products as $product)
                                    <div class="product list-view-large first ">
                                        <div class="media">
                                            <img width="224" height="197" alt="" class="attachment-shop_catalog size-shop_catalog wp-post-image" src="{{ URL::asset('storage/'.$product->image1)}}">
                                            <div class="media-body">
                                                <div class="product-info">
                                                    <div class="yith-wcwl-add-to-wishlist">
                                                        <a href="#" rel="nofollow" class="add_to_wishlist"> Add to Wishlist</a>
                                                    </div>
                                                    <!-- .yith-wcwl-add-to-wishlist -->
                                                    <a class="woocommerce-LoopProduct-link woocommerce-loop-product__link" href="/item/{{$product->id}}">
                                                        <h2 class="woocommerce-loop-product__title">{{$product->title}}</h2>
                                                        <div class="techmarket-product-rating">
                                                            <div title="Rated 5.00 out of 5" class="star-rating">
                                                                <span style="width:{{rand(50,100)}}%">
                                                                    <strong class="rating">5.00</strong> out of 5</span>
                                                            </div>
                                                            <span class="review-count">({{rand(1,100)}})</span>
                                                        </div>
                                                    </a>
                                                    <!-- .woocommerce-LoopProduct-link -->
                                                    <div class="brand">
                                                        <a href="/item/{{$product->id}}">
                                                            <img alt="galaxy" src="{{ URL::asset('storage/'.$product->image2)}}">
                                                        </a>
                                                    </div>
                                                    <!-- .brand -->
                                                    <div class="woocommerce-product-details__short-description">
                                                            {{$product->side_description}}
                                                    </div>
                                                    <!-- .woocommerce-product-details__short-description 
                                                    <span class="sku_wrapper">SKU:
                                                        <span class="sku">5487FB8/13</span>
                                                    </span>-->
                                                </div>
                                                <!-- .product-info -->
                                                <div class="product-actions">
                                                    <div class="availability">
                                                        Disponibilite:
                                                        <p class="stock in-stock">en Stock</p>
                                                    </div>
                                                    <span class="price">
                                                        @if($product->price_start)
                                                            <span class="woocommerce-Price-amount amount">
                                                                <span class="woocommerce-Price-currencySymbol"></span>A Partir de {{$product->price}}DH
                                                            </span>
                                                            @else

                                                            @if($product->promotion)
                                                                <ins>
                                                                    <span class="woocommerce-Price-amount amount">
                                                                        <span class="woocommerce-Price-currencySymbol"></span>{{$product->price_promotion}}DH
                                                                    </span>

                                                                </ins>
                                                                <del>      
                                                                    <span class="woocommerce-Price-amount amount">  
                                                                        <span class="woocommerce-Price-currencySymbol"></span>{{$product->price}}DH
                                                                    </span>
                                                                </del>

                                                            @else    
                                                                <ins>
                                                                    <span class="woocommerce-Price-amount amount">
                                                                       <span class="woocommerce-Price-currencySymbol"></span>{{$product->price}}DH
                                                                    </span>
                                                                
                                                                </ins>

                                                            @endif

                                                        @endif
                                                    </span>
                                                    <!-- .price -->
                                                    <a class="button add_to_cart_button" href="/add_produit/{{$product->id}}">Ajouter au panier</a>
                                                    <!-- <a class="add-to-compare-link" href="compare.html">Add to compare</a> -->
                                                </div>
                                                <!-- .product-actions -->
                                            </div>
                                            <!-- .media-body -->
                                        </div>
                                        <!-- .media -->
                                    </div>
                                    @endforeach
                                    <!-- .product -->
                                </div>
                                <!-- .products -->
                            </div>
                            <!-- .woocommerce -->
                        </div>
                        
                        <!-- .tab-pane -->
                        <div id="list-view-small" class="tab-pane" role="tabpanel">
                            <div class="woocommerce columns-1">
                                <div class="products">
                                    @foreach($products as $product)
                                    <div class="product list-view-small ">
                                        <div class="media">
                                            <img width="224" height="197" alt="" class="attachment-shop_catalog size-shop_catalog wp-post-image" src="{{ URL::asset('storage/'.$product->image1)}}">
                                            <div class="media-body">
                                                <div class="product-info">
                                                    <div class="yith-wcwl-add-to-wishlist">
                                                        <a href="#" rel="nofollow" class="add_to_wishlist"> Add to Wishlist</a>
                                                    </div>
                                                    <!-- .yith-wcwl-add-to-wishlist -->
                                                    <a class="woocommerce-LoopProduct-link woocommerce-loop-product__link" href="/item/{{$product->id}}">
                                                        <h2 class="woocommerce-loop-product__title">{{$product->title}}</h2>
                                                        <div class="techmarket-product-rating">
                                                            <div title="Rated 5.00 out of 5" class="star-rating">
                                                                <span style="width:{{rand(50,100)}}%">
                                                                    <strong class="rating">5.00</strong> out of 5</span>
                                                            </div>
                                                            <span class="review-count">({{rand(1,100)}})</span>
                                                        </div>
                                                    </a>
                                                    <!-- .woocommerce-LoopProduct-link -->
                                                    <div class="woocommerce-product-details__short-description">
                                                            {{$product->side_description}}
                                                    </div>
                                                    <!-- .woocommerce-product-details__short-description -->
                                                </div>
                                                <!-- .product-info -->
                                                <div class="product-actions">
                                                    <span class="price">
                                        

                                                            @if($product->price_start)
                                                            <span class="woocommerce-Price-amount amount">
                                                                <span class="woocommerce-Price-currencySymbol"></span>A Partir de {{$product->price}}DH
                                                            </span>
                                                            @else

                                                            @if($product->promotion)
                                                                <ins>
                                                                <span class="woocommerce-Price-amount amount">
                                                                    <span class="woocommerce-Price-currencySymbol"></span>{{$product->price_promotion}}DH
                                                                </span>

                                                                </ins>
                                                                <del>   
                                                                <span class="woocommerce-Price-amount amount">
                                                                    <span class="woocommerce-Price-currencySymbol"></span>{{$product->price}}DH
                                                                </span>
                                                                </del>

                                                            @else    
                                                                <ins>
                                                                <span class="woocommerce-Price-amount amount">
                                                                    <span class="woocommerce-Price-currencySymbol"></span>{{$product->price}}DH
                                                                </span>
                                                                
                                                                </ins>

                                                            @endif

                                                        @endif

                                                    </span>

                                                    
                                                    <!-- .price -->
                                                    <a class="button add_to_cart_button" href="/add_produit/{{$product->id}}">Ajouter au panier</a>
                                                    <!-- <a class="add-to-compare-link" href="compare.html">Add to compare</a> -->
                                                </div>
                                                <!-- .product-actions -->
                                            </div>
                                            <!-- .media-body -->
                                        </div>
                                        <!-- .media -->
                                    </div>
                                    @endforeach
                                    <!-- .product -->

                                </div>
                                <!-- .products -->
                            </div>
                            <!-- .woocommerce -->
                        </div>
                        <!-- .tab-pane -->
                    </div>
                    <!-- .tab-content -->
                    <div class="shop-control-bar-bottom">
                        <form class="form-techmarket-wc-ppp" method="POST">
                           <!-- <select class="techmarket-wc-wppp-select c-select" onchange="this.form.submit()" name="ppp">
                                <option value="20">Show 20</option>
                                <option value="40">Show 40</option>
                                <option value="-1">Show All</option>
                            </select> 
                            <input type="hidden" value="5" name="shop_columns">
                            <input type="hidden" value="15" name="shop_per_page">
                            <input type="hidden" value="right-sidebar" name="shop_layout">-->
                        </form>
                        <!-- .form-techmarket-wc-ppp
                        <p class="woocommerce-result-count">
                            Showing 1&ndash;15 of 73 results
                        </p> -->
                        <!-- .woocommerce-result-count -->
                        <nav class="woocommerce-pagination">
                        {{ $products->links() }}
                            <!-- .page-numbers -->
                        </nav>
                        <!-- .woocommerce-pagination -->
                    </div>
                    <!-- .shop-control-bar-bottom -->
                </main>
                <!-- #main -->
            </div>
            <!-- #primary -->
        </div>
        <!-- .row -->
    </div>
    <!-- .col-full -->
</div>


@endsection