@extends('layouts.frontend.frontend')

@section('content')
            
<div id="content" class="site-content" tabindex="-1">
<div class="col-full">
<div class="row">
    <div id="primary" class="content-area">
        <main id="main" class="site-main">
                <div class="homev12-slider-with-banners row">
                        <div class="slider-block column-1">
                            <div class="home-v12-slider">
                                <div class="home-v12-slider home-slider">
                                    @foreach($banners as $item)
                                    <div class="slider-1" style="background-image: url({{ URL::asset('storage/'.$item->image)}});background-size:100% 100%;">
                                        <div class="caption">
                                                <div class="title" style="color: #bb2131"> </div>
                                                <div class="sub-title"><br><br><br><br></div>
                                                
                                                @if($item->type == 'Link')
                                                    <a href="{{$item->url}}"><div class="button">Decouvrir
                                                            <i class="tm tm-long-arrow-right"></i>
                                                        </div></a>
                                                @elseif($item->type == 'Group')
                                                <a href="{{'/list/'.$item->group_id}}"><div class="button">Decouvrir
                                                        <i class="tm tm-long-arrow-right"></i>
                                                    </div></a>
                                                @endif
                                            </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <!-- .slider-block -->
                        <div class="banners-block column-2">
                            @foreach($smallBanners as $item)
                            <div class="banner text-in-left">
                                <a href="{{ ($item->type == 'Link')? $item->url : '/list/'.$item->group_id }}">
                                    <div style="background-size: cover; background-position: center center; background-image: url( {{ URL::asset('storage/'.$item->image)}} ); height: 210px;" class="banner-bg">
                                        <div class="caption">
                                                <div class="banner-info">
                                                    <h3 class="title">
                                                        <strong></strong>
                                                        <br> 
                                                </div>
                                                <span class="price">
                                                    <!--<span class="start_price">Starting at</span>
                                                    <br>$69.99</span>-->
                                            </div>
                                        <!-- .caption -->
                                    </div>
                                </a>
                            </div>
                            @endforeach
                           
                        </div>
                        <!-- .banners-block -->
                    </div>
           

                @if(!empty($promotions))
                <!-- .section-products-carousel-tabs -->
                <section class="section-3-2-3-product-cards-tabs-with-featured-product stretch-full-width">
                        <div class="col-full">
                            <header class="section-header">
                                <h2 class="section-title">Promotions
                                    <!--<span> Offres a ne pas rattez</span> -->
                                </h2>
                                <ul role="tablist" class="nav justify-content-center">
                                    {{--@foreach($promotions as $key => $value)
                                        @if($loop->index == 3)
                                            <li class="nav-item"><a class="nav-link active" href="#promTab{{$loop->index}}" data-toggle="tab">{{ $key }} </a></li>
                                        @else
                                            <li class="nav-item"><a class="nav-link" href="#promTab{{$loop->index}}" data-toggle="tab">{{ $key }}</a></li>
                                        @endif
                                    @endforeach--}}
                                    
                                </ul>
                            </header>
                            <!-- .section-header -->
                            
                            <div class="tab-content">

                            @foreach($promotions as $key => $value)
                                <div id="promTab{{$loop->index}}" class="tab-pane {{$loop->index == 0 ? 'active' : ''}}" role="tabpanel">
                                <div class="product-cards-3-2-3-with-featured-product">
                                    <div class="row">
                                        <div class="products-3">
                                            <div class="woocommerce columns-1">
                                                <div class="products">
                                                    
                                                    <!-- .woocommerce-LoopProduct-link -->
                                                    @foreach($value['left'] as $b)
                                                    <div class="landscape-product-card product">
                                                        <div class="media">
                                                            <div class="yith-wcwl-add-to-wishlist">
                                                                <a href="/item/{{$b->id}}" rel="nofollow" class="add_to_wishlist"> Add to Wishlist</a>
                                                            </div>
                                                            <a class="woocommerce-LoopProduct-link" href="/item/{{$b->id}}">
                                                                <img class="wp-post-image" src="{{ URL::asset('storage/'.$b->image1)}}" alt="">
                                                            </a>
                                                            <div class="media-body">
                                                                <a class="woocommerce-LoopProduct-link " href="/item/{{$b->id}}">
                                                                    <span class="price">
                                                                        @if($b->price_start)
                                                                        <ins>
                                                                            <span class="amount"> A Partir de {{$b->price}}DH</span>
                                                                        </ins>
                                                                        @else
                                                                            @if($b->promotion)
                                                                                <ins>
                                                                                    <span class="amount"> {{$b->price_promotion}}DH</span>
                                                                                </ins>
                                                                                <del>
                                                                                    <span class="amount">{{$b->price}}DH</span>
                                                                                </del>

                                                                            @else
                                                                                @if($b->price !=0)
                                                                                 <ins>
                                                                                    <span class="amount"> {{$b->price}}DH</span>
                                                                                </ins>
                                                                                @endif

                                                                            @endif
                                                                        
                                                                        @endif
                                                                    </span>
                                                                    <!-- .price -->
                                                                    
                                                                    <h2 class="woocommerce-loop-product__title">{{$b->title}}</h2>
                                                                    @if($b->promotion)
                                                                        <div class="ribbon green-label">
                                                                            <span>Promo !</span>
                                                                        </div>
                                                                    @endif
                                                                    <div class="techmarket-product-rating">
                                                                        <div title="Rated 5 out of 5" class="star-rating">
                                                                            <span style="width:{{rand(50,100)}}%">
                                                                                <strong class="rating">5</strong> out of 5</span>
                                                                        </div>
                                                                        <span class="review-count">({{rand(1,100)}})</span>
                                                                    </div>
                                                                    <!-- .techmarket-product-rating -->
                                                                </a>
                                                                <div class="hover-area">
                                                                    <a class="button add_to_cart_button" href="/item/{{$b->id}}">Details</a>
                                                                    <!--<a href="compare.html" class="add-to-compare-link">Add to compare</a>-->
                                                                </div>
                                                                <!-- .hover-area -->
                                                            </div>
                                                            <!-- .media-body -->
                                                        </div>
                                                        <!-- .media -->
                                                    </div>
                                                    @endforeach
                                                    <!-- .woocommerce-LoopProduct-link -->
                                                </div>
                                            </div>
                                        </div>

                                        <div class="products-3-with-featured">
                                            <div class="woocommerce columns-1">
                                                <div class="products">
                                                    <div class="landscape-product-card-featured product">
                                                        <div class="media">
                                                            <div class="techmarket-product-gallery images techmarket-3-2-3-gallery">
                                                                <figure class="techmarket-wc-product-gallery__wrapper" data-ride="tm-slick-carousel" data-wrap=".techmarket-wc-product-gallery__wrapper" data-slick="{&quot;infinite&quot;:false,&quot;slidesToShow&quot;:1,&quot;slidesToScroll&quot;:1,&quot;dots&quot;:false,&quot;arrows&quot;:false,&quot;asNavFor&quot;:&quot;.techmarket-3-2-3-gallery .techmarket-wc-product-gallery-thumbnails__wrapper&quot;}">
                                                                    <figure class="techmarket-wc-product-gallery__image" data-thumb="{{ URL::asset('storage/'.$value['special']->image1)}}">
                                                                        <img width="600" height="600" title="" alt="" class="wp-post-image1" src="{{ URL::asset('storage/'.$value['special']->image1)}}">
                                                                    </figure>
                                                                    <figure class="techmarket-wc-product-gallery__image" data-thumb="{{ URL::asset('storage/'.$value['special']->image2)}}">
                                                                        <img width="600" height="600" title="" alt="" class="wp-post-image2" src="{{ URL::asset('storage/'.$value['special']->image2)}}">
                                                                    </figure>
                                                                    <figure class="techmarket-wc-product-gallery__image" data-thumb="{{ URL::asset('storage/'.$value['special']->image3)}}">
                                                                        <img width="600" height="600" title="" alt="" class="wp-post-image3" src="{{ URL::asset('storage/'.$value['special']->image3)}}">
                                                                    </figure>
                                                                    <figure class="techmarket-wc-product-gallery__image" data-thumb="{{ URL::asset('storage/'.$value['special']->image4)}}">
                                                                        <img width="600" height="600" title="" alt="" class="wp-post-image" src="{{ URL::asset('storage/'.$value['special']->image4)}}">
                                                                    </figure>
                                                                    <figure class="techmarket-wc-product-gallery__image" data-thumb="{{ URL::asset('storage/'.$value['special']->image5)}}">
                                                                        <img width="600" height="600" title="" alt="" class="wp-post-image" src="{{ URL::asset('storage/'.$value['special']->image5)}}">
                                                                    </figure>
                                                                </figure>
                                                                <figure class="techmarket-wc-product-gallery-thumbnails__wrapper" data-ride="tm-slick-carousel" data-wrap=".techmarket-wc-product-gallery-thumbnails__wrapper" data-slick="{&quot;infinite&quot;:false,&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:1,&quot;dots&quot;:false,&quot;arrows&quot;:true,&quot;vertical&quot;:true,&quot;verticalSwiping&quot;:true,&quot;focusOnSelect&quot;:true,&quot;touchMove&quot;:true,&quot;prevArrow&quot;:&quot;&lt;a href=\&quot;#\&quot;&gt;&lt;i class=\&quot;tm tm-arrow-up\&quot;&gt;&lt;\/i&gt;&lt;\/a&gt;&quot;,&quot;nextArrow&quot;:&quot;&lt;a href=\&quot;#\&quot;&gt;&lt;i class=\&quot;tm tm-arrow-down\&quot;&gt;&lt;\/i&gt;&lt;\/a&gt;&quot;,&quot;asNavFor&quot;:&quot;.techmarket-3-2-3-gallery .techmarket-wc-product-gallery__wrapper&quot;,&quot;responsive&quot;:[{&quot;breakpoint&quot;:767,&quot;settings&quot;:{&quot;vertical&quot;:false,&quot;verticalSwiping&quot;:false,&quot;slidesToShow&quot;:1}}]}">
                                                                    <figure class="techmarket-wc-product-gallery__image">
                                                                        <img width="180" height="180" title="" alt="" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="{{ URL::asset('storage/'.$value['special']->image1)}}">
                                                                    </figure>
                                                                    <figure class="techmarket-wc-product-gallery__image">
                                                                        <img width="180" height="180" title="" alt="" class="attachment-shop_thumbnail size-shop_thumbnail" src="{{ URL::asset('storage/'.$value['special']->image2)}}">
                                                                    </figure>
                                                                    <figure class="techmarket-wc-product-gallery__image">
                                                                        <img width="180" height="180" title="" alt="" class="attachment-shop_thumbnail size-shop_thumbnail" src="{{ URL::asset('storage/'.$value['special']->image3)}}">
                                                                    </figure>
                                                                    <figure class="techmarket-wc-product-gallery__image">
                                                                        <img width="180" height="180" title="" alt="" class="attachment-shop_thumbnail size-shop_thumbnail" src="{{ URL::asset('storage/'.$value['special']->image4)}}">
                                                                    </figure>
                                                                    <figure class="techmarket-wc-product-gallery__image">
                                                                        <img width="180" height="180" title="" alt="" class="attachment-shop_thumbnail size-shop_thumbnail" src="{{ URL::asset('storage/'.$value['special']->image5)}}">
                                                                    </figure>
                                                                </figure>
                                                            </div>
                                                            <div class="media-body">
                                                                <a class="woocommerce-LoopProduct-link woocommerce-loop-product__link" href="/item/{{$value['special']->id}}">
                                                                  
                                                                    <span class="price">
                                                                            @if($b->price_start)
                                                                            <ins>
                                                                                <span class="woocommerce-Price-amount amount">
                                                                                <span class="woocommerce-Price-currencySymbol"></span>A Partir de {{$value['special']->price}}DH</span>
                                                                            </ins>
                                                                            @else
                                                                                @if($b->promotion)
                                                                                    <ins>
                                                                                        <span class="woocommerce-Price-amount amount">
                                                                                                <span class="woocommerce-Price-currencySymbol"></span>{{$value['special']->price_promotion}}DH</span>
                                                                                    </ins>
                                                                                    <del>       <span class="woocommerce-Price-amount amount">
                                                                                                <span class="woocommerce-Price-currencySymbol"></span>{{$value['special']->price}}DH</span>
                                                                                    </del>
    
                                                                                @else 
                                                                                    @if($value['special']->price !=0)   
                                                                                     <ins>
                                                                                        <span class="woocommerce-Price-amount amount">
                                                                                                <span class="woocommerce-Price-currencySymbol"></span>{{$value['special']->price}}DH</span>
                                                                                    </ins>
                                                                                    @endif
    
                                                                                @endif
                                                                            
                                                                            @endif
                                                                        </span>
                                                                    
                                                                    <h2 class="woocommerce-loop-product__title">{{$value['special']->title}}</h2>
                                                                    @if($b->promotion)
                                                                    <div class="ribbon green-label">
                                                                        <span>Promo !</span>
                                                                    </div>
                                                                    @endif
                                                                    <div class="techmarket-product-rating">
                                                                        <div title="Rated 0 out of 5" class="star-rating">
                                                                            <span style="width:{{rand(50,100)}}%">
                                                                                <strong class="rating">5</strong> out of 5</span>
                                                                        </div>
                                                                        <span class="review-count">({{rand(1,100)}})</span>
                                                                    </div>
                                                                </a>
                                                                <div class="woocommerce-product-details__short-description">
                                                                    <ul>
                                                                        {{$value['special']->side_description}}
                                                                    </ul>
                                                                </div>
                                                                <a class="button add_to_cart_button" href="/item/{{$value['special']->id}}">Details</a>
                                                                <!--<a class="add-to-compare-link" href="compare.html">Add to compare</a>-->
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="products-3">
                                            <div class="woocommerce columns-1">
                                                <div class="products">
                                                    
                                                        <!-- .woocommerce-LoopProduct-link -->
                                                        @foreach($value['right'] as $b)
                                                        <div class="landscape-product-card product">
                                                            <div class="media">
                                                                <div class="yith-wcwl-add-to-wishlist">
                                                                    <a href="/item/{{$b->id}}" rel="nofollow" class="add_to_wishlist"> Add to Wishlist</a>
                                                                </div>
                                                                <a class="woocommerce-LoopProduct-link" href="/item/{{$b->id}}">
                                                                    <img class="wp-post-image" src="{{ URL::asset('storage/'.$b->image1)}}" alt="">
                                                                </a>
                                                                <div class="media-body">
                                                                    <a class="woocommerce-LoopProduct-link " href="/item/{{$b->id}}">
                                                                        <span class="price">
                                                                                @if($b->price_start)
                                                                                <ins>
                                                                                    <span class="amount"> A Partir de {{$b->price}}DH</span>
                                                                                </ins>
                                                                                @else
                                                                                    @if($b->promotion)
                                                                                        <ins>
                                                                                            <span class="amount"> {{$b->price_promotion}}DH</span>
                                                                                        </ins>
                                                                                        <del>
                                                                                            <span class="amount">{{$b->price}}DH</span>
                                                                                        </del>
        
                                                                                    @else
                                                                                        @if($b->price !=0)   

                                                                                         <ins>
                                                                                            <span class="amount"> {{$b->price}}DH</span>
                                                                                        </ins>
                                                                                        @endif
        
                                                                                    @endif
                                                                                
                                                                                @endif
                                                                            </span>
                                                                        <!-- .price -->
                                                                        <h2 class="woocommerce-loop-product__title">{{$b->title}}</h2>
                                                                        @if($b->promotion)
                                                                        <div class="ribbon green-label">
                                                                            <span>Promo !</span>
                                                                        </div>
                                                                        @endif
                                                                        <div class="techmarket-product-rating">
                                                                            <div title="Rated 5 out of 5" class="star-rating">
                                                                                <span style="width:{{rand(50,100)}}%">
                                                                                    <strong class="rating">5</strong> out of 5</span>
                                                                            </div>
                                                                            <span class="review-count">({{rand(1,100)}})</span>
                                                                        </div>
                                                                        <!-- .techmarket-product-rating -->
                                                                    </a>
                                                                    <div class="hover-area">
                                                                        <a class="button add_to_cart_button" href="/item/{{$b->id}}">Details</a>
                                                                        <!-- <a href="compare.html" class="add-to-compare-link">Add to compare</a> -->
                                                                    </div>
                                                                    <!-- .hover-area -->
                                                                </div>
                                                                <!-- .media-body -->
                                                            </div>
                                                            <!-- .media -->
                                                        </div>
                                                        @endforeach
                                                        <!-- .woocommerce-LoopProduct-link -->

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                @endforeach
                                <!-- .products -->

                            </div>
                            <!-- .tab-content -->
                        </div>
                        <!-- .col-full -->
                    </section>

                    @endif

                    <section class="section-products-carousel" id="homev12-carousel-1">
                            <header class="section-header">
                                <h2 class="section-title">Parmis nos partenaires</h2>
                                <nav class="custom-slick-nav"></nav>
                                <!-- .custom-slick-nav -->
                            </header>
                            <!-- .section-header -->
                            <div class="products-carousel" id="new-arrival-carousel" data-ride="tm-slick-carousel" data-wrap=".products" data-slick="{&quot;infinite&quot;:false,&quot;slidesToShow&quot;:8,&quot;slidesToScroll&quot;:8,&quot;dots&quot;:true,&quot;arrows&quot;:true,&quot;prevArrow&quot;:&quot;&lt;a href=\&quot;#\&quot;&gt;&lt;i class=\&quot;tm tm-arrow-left\&quot;&gt;&lt;\/i&gt;&lt;\/a&gt;&quot;,&quot;nextArrow&quot;:&quot;&lt;a href=\&quot;#\&quot;&gt;&lt;i class=\&quot;tm tm-arrow-right\&quot;&gt;&lt;\/i&gt;&lt;\/a&gt;&quot;,&quot;appendArrows&quot;:&quot;#homev12-carousel-1 .custom-slick-nav&quot;,&quot;responsive&quot;:[{&quot;breakpoint&quot;:650,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesToScroll&quot;:1}},{&quot;breakpoint&quot;:780,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:3}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:4,&quot;slidesToScroll&quot;:4}},{&quot;breakpoint&quot;:1400,&quot;settings&quot;:{&quot;slidesToShow&quot;:6,&quot;slidesToScroll&quot;:6}}]}">
                                <div class="container-fluid">
                                    <div class="woocommerce columns-8">
                                        <div class="products">
            
                                            @foreach($partenaires as $item)
                                            <div class="product">
                                                <div class="yith-wcwl-add-to-wishlist">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist"> Add to Wishlist</a>
                                                </div>
                                                <a href="{{'/list/'.$item->id }}" class="woocommerce-LoopProduct-link">
                                                    <img src="{{ URL::asset('storage/'.$item->image)}}" width="224" height="197" class="wp-post-image" alt="">
                                                    <span class="price">
                                                        <ins>
                                                            <span class="amount"> </span>
                                                        </ins>
                                                        <span class="amount"></span>
                                                    </span>
                                                    <!-- /.price -->
                                                    <h2 class="woocommerce-loop-product__title">{{$item->title}}</h2>
                                                </a>
                                                <div class="hover-area">
                                                    <a class="button add_to_cart_button" href="{{'/list/'.$item->id }}" rel="nofollow">Decouvrir</a>
                                                </div>
                                            </div>
                                            @endforeach
                                            <!-- /.product-outer -->
                                        </div>
                                    </div>
                                </div>
                                <!-- .container-fluid -->
                            </div>
                            <!-- .products-carousel -->
                        </section>


                <div class="fullwidth-notice stretch-full-width">
                    <div class="col-full">
                        <p class="message"> &nbsp; </p>
                    </div>
                    <!-- .col-full -->
                </div>
                <!-- .fullwidth-notice -->
                <div class="banners">
                    <div class="row">
                        <div class="banner small-banner text-in-left">
                            <a href="#">
                                <div class="banner-bg half-banner" style="background-size: cover; background-position: center center; background-image: url( @if(!empty($bannerLeft)){{ URL::asset('storage/'.$bannerLeft->image)}} @endif ); height: 259px;">
                                    <div class="caption">
                                        <div class="banner-info">
                                            <!--<h3 class="title">New Arrivals
                                                <br> in
                                                <strong>Accessories</strong>
                                                <br> at Best Prices.</h3>-->
                                        </div>
                                        <a href="{{ ($bannerLeft->type == 'Link')? $bannerLeft->url : '/list/'.$bannerLeft->group_id }}"><span class="banner-action button">Decouvrir</span></a>
                                    </div>
                                    <!-- /.caption -->
                                </div>
                            </a>
                        </div>
                        <!-- /.banner -->
                        <div class="banner large-banner text-in-right">
                            <a href="#">
                                <div class="banner-bg center-banner" style="background-size: cover; background-position: center center; background-image: url( @if(!empty($bannerCenter)){{ URL::asset('storage/'.$bannerCenter->image)}} @endif ); height: 259px;">
                                    <div class="caption">
                                        <div class="banner-info">
                                           <!-- <h3 class="title">Catch Hottest
                                                <br>
                                                <strong>Deals</strong> on the
                                                <br> Curved Soundbars.</h3> -->
                                        </div>
                                        <a href="{{ ($bannerCenter->type == 'Link')? $bannerCenter->url : '/list/'.$bannerCenter->group_id }}"><span class="banner-action button">Decouvrir</span></a>
                                    </div>
                                    <!-- /.caption -->
                                </div>
                            </a>
                        </div>
                        <!-- /.banner -->
                        <div class="banner small-banner text-in-left">
                            <a href="#">
                                <div class="banner-bg half-banner" style="background-size: cover; background-position: center center; background-image: url( @if(!empty($bannerRight)){{ URL::asset('storage/'.$bannerRight->image)}} @endif ); height: 259px;">
                                    <div class="caption">
                                        <div class="banner-info">
                                           <!-- <h3 class="title">
                                                <strong>1000 mAh</strong>
                                                <br> Power Bank Pro</h3>-->
                                        </div>
                                        <!--<span class="price">$34.99</span> -->
                                        <a href="{{ ($bannerRight->type == 'Link')? $bannerRight->url : '/list/'.$bannerRight->group_id }}"><span class="banner-action button">Decouvrir</span></a>
                                    </div>
                                    <!-- /.caption -->
                                </div>
                            </a>
                        </div>
                        <!-- /.banner -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.banners -->
                

                <section class="section-products-carousel" id="section-product-carousel-8">
                        <header class="section-header">
                            <h2 class="section-title">Nouveaux Produits</h2>
                            <nav class="custom-slick-nav"></nav>
                        </header>
                        <!-- .section-header -->
                        <div class="products-carousel 8-column-carousel" data-ride="tm-slick-carousel" data-wrap=".products" data-slick="{&quot;infinite&quot;:false,&quot;slidesToShow&quot;:8,&quot;slidesToScroll&quot;:8,&quot;dots&quot;:true,&quot;arrows&quot;:true,&quot;prevArrow&quot;:&quot;&lt;a href=\&quot;#\&quot;&gt;&lt;i class=\&quot;tm tm-arrow-left\&quot;&gt;&lt;\/i&gt;&lt;\/a&gt;&quot;,&quot;nextArrow&quot;:&quot;&lt;a href=\&quot;#\&quot;&gt;&lt;i class=\&quot;tm tm-arrow-right\&quot;&gt;&lt;\/i&gt;&lt;\/a&gt;&quot;,&quot;appendArrows&quot;:&quot;#section-products-carousel-8 .custom-slick-nav&quot;,&quot;responsive&quot;:[{&quot;breakpoint&quot;:779,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesToScroll&quot;:2}},{&quot;breakpoint&quot;:780,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:3}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:4,&quot;slidesToScroll&quot;:4}},{&quot;breakpoint&quot;:1700,&quot;settings&quot;:{&quot;slidesToShow&quot;:6,&quot;slidesToScroll&quot;:6}}]}">
                            <div class="container-fluid">
                                <div class="woocommerce columns-8">
                                        <div class="products">
                                          @foreach($newProducts as $b)

                                            <div class="product">
                                                <div class="yith-wcwl-add-to-wishlist">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist"> Add to Wishlist</a>
                                                </div>
                                                <a href="/item/{{$b->id}}" class="woocommerce-LoopProduct-link">
                                                    <img src="{{ URL::asset('storage/'.$b->image1)}}" width="224" height="197" class="wp-post-image" alt="">
                                                    <span class="price">
                                                            @if($b->price_start)
                                                            <ins>
                                                                <span class="amount"> A Partir de {{$b->price}}DH</span>
                                                            </ins>
                                                            @else
                                                                @if($b->promotion)
                                                                    <ins>
                                                                        <span class="amount"> {{$b->price_promotion}}DH</span>
                                                                    </ins>
                                                                    <del>
                                                                        <span class="amount">{{$b->price}}DH</span>
                                                                    </del>

                                                                @else
                                                                    @if($b->price !=0)   

                                                                     <ins>
                                                                        <span class="amount"> {{$b->price}}DH</span>
                                                                    </ins>
                                                                    @endif

                                                                @endif
                                                            
                                                            @endif
                                                        </span>
                                                    <!-- /.price -->
                                                    <h2 class="woocommerce-loop-product__title">{{$b->title}}</h2>
                                                </a>
                                                <div class="hover-area">
                                                    <a class="button add_to_cart_button" href="/item/{{$b->id}}" rel="nofollow">Details</a>
                                                </div>
                                            </div>
                                            @endforeach
                                        
                                        </div>
                   
                                </div>
                                <!-- .woocommerce -->
                            </div>
                            <!-- .row -->
                        </div>
                        <!-- .products-carousel -->
                    </section>

                    <section class="section-products-carousel" id="section-product-carousel-8">
                            <header class="section-header">
                                <h2 class="section-title">Produits pouvant vous intéresser</h2>
                                <nav class="custom-slick-nav"></nav>
                            </header>
                            <!-- .section-header -->
                            <div class="products-carousel 8-column-carousel" data-ride="tm-slick-carousel" data-wrap=".products" data-slick="{&quot;infinite&quot;:false,&quot;slidesToShow&quot;:8,&quot;slidesToScroll&quot;:8,&quot;dots&quot;:true,&quot;arrows&quot;:true,&quot;prevArrow&quot;:&quot;&lt;a href=\&quot;#\&quot;&gt;&lt;i class=\&quot;tm tm-arrow-left\&quot;&gt;&lt;\/i&gt;&lt;\/a&gt;&quot;,&quot;nextArrow&quot;:&quot;&lt;a href=\&quot;#\&quot;&gt;&lt;i class=\&quot;tm tm-arrow-right\&quot;&gt;&lt;\/i&gt;&lt;\/a&gt;&quot;,&quot;appendArrows&quot;:&quot;#section-products-carousel-8 .custom-slick-nav&quot;,&quot;responsive&quot;:[{&quot;breakpoint&quot;:779,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesToScroll&quot;:2}},{&quot;breakpoint&quot;:780,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:3}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:4,&quot;slidesToScroll&quot;:4}},{&quot;breakpoint&quot;:1700,&quot;settings&quot;:{&quot;slidesToShow&quot;:6,&quot;slidesToScroll&quot;:6}}]}">
                                <div class="container-fluid">
                                    <div class="woocommerce columns-8">
                                            <div class="products">
                                              @foreach($interestingProducts as $b)
    
                                                <div class="product">
                                                    <div class="yith-wcwl-add-to-wishlist">
                                                        <a href="#" rel="nofollow" class="add_to_wishlist">Add to Wishlist</a>
                                                    </div>
                                                    <a href="/item/{{$b->id}}" class="woocommerce-LoopProduct-link">
                                                        <img src="{{ URL::asset('storage/'.$b->image1)}}" width="224" height="197" class="wp-post-image" alt="">
                                                        <span class="price">
                                                                @if($b->price_start)
                                                                <ins>
                                                                    <span class="amount"> A Partir de {{$b->price}}DH</span>
                                                                </ins>
                                                                @else
                                                                    @if($b->promotion)
                                                                        <ins>
                                                                            <span class="amount"> {{$b->price_promotion}}DH</span>
                                                                        </ins>
                                                                        <del>
                                                                            <span class="amount">{{$b->price}}DH</span>
                                                                        </del>

                                                                    @else
                                                                        @if($b->price !=0)   

                                                                         <ins>
                                                                            <span class="amount"> {{$b->price}}DH</span>
                                                                        </ins>
                                                                        @endif

                                                                    @endif
                                                                
                                                                @endif
                                                            </span>
                                                        <!-- /.price -->
                                                        <h2 class="woocommerce-loop-product__title">{{$b->title}}</h2>
                                                    </a>
                                                    <div class="hover-area">
                                                        <a class="button add_to_cart_button" href="/item/{{$b->id}}" rel="nofollow">Details</a>
                                                    </div>
                                                </div>
                                                @endforeach
                                            
                                            </div>
                       
                                    </div>
                                    <!-- .woocommerce -->
                                </div>
                                <!-- .row -->
                            </div>
                            <!-- .products-carousel -->
                        </section>

                <section class="section-products-carousel" id="section-product-carousel-8">
                        <header class="section-header">
                            <h2 class="section-title">Catégories Populaires</h2>
                            <nav class="custom-slick-nav"></nav>
                        </header>
                        <!-- .section-header -->
                        <div class="products-carousel 8-column-carousel" data-ride="tm-slick-carousel" data-wrap=".products" data-slick="{&quot;infinite&quot;:false,&quot;slidesToShow&quot;:8,&quot;slidesToScroll&quot;:8,&quot;dots&quot;:true,&quot;arrows&quot;:true,&quot;prevArrow&quot;:&quot;&lt;a href=\&quot;#\&quot;&gt;&lt;i class=\&quot;tm tm-arrow-left\&quot;&gt;&lt;\/i&gt;&lt;\/a&gt;&quot;,&quot;nextArrow&quot;:&quot;&lt;a href=\&quot;#\&quot;&gt;&lt;i class=\&quot;tm tm-arrow-right\&quot;&gt;&lt;\/i&gt;&lt;\/a&gt;&quot;,&quot;appendArrows&quot;:&quot;#section-products-carousel-8 .custom-slick-nav&quot;,&quot;responsive&quot;:[{&quot;breakpoint&quot;:779,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesToScroll&quot;:2}},{&quot;breakpoint&quot;:780,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:3}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:4,&quot;slidesToScroll&quot;:4}},{&quot;breakpoint&quot;:1700,&quot;settings&quot;:{&quot;slidesToShow&quot;:6,&quot;slidesToScroll&quot;:6}}]}">
                            <div class="container-fluid">
                                <div class="woocommerce columns-8">
                                        <div class="products">
                                          @foreach($categoriesProducts as $b)

                                            <div class="product">
                                                <div class="yith-wcwl-add-to-wishlist">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist"> Add to Wishlist</a>
                                                </div>
                                                <a href="/item/{{$b->id}}" class="woocommerce-LoopProduct-link">
                                                    <img src="{{ URL::asset('storage/'.$b->image1)}}" width="224" height="197" class="wp-post-image" alt="">
                                                    <span class="price">
                                                            @if($b->price_start)
                                                            <ins>
                                                                <span class="amount"> A Partir de {{$b->price}}DH</span>
                                                            </ins>
                                                            @else
                                                                @if($b->promotion)
                                                                    <ins>
                                                                        <span class="amount"> {{$b->price_promotion}}DH</span>
                                                                    </ins>
                                                                    <del>
                                                                        <span class="amount">{{$b->price}}DH</span>
                                                                    </del>

                                                                @else
                                                                    @if($b->price !=0)   
                                                                     <ins>
                                                                        <span class="amount"> {{$b->price}}DH</span>
                                                                    </ins>
                                                                    @endif

                                                                @endif
                                                            
                                                            @endif
                                                        </span>
                                                    <!-- /.price -->
                                                    <h2 class="woocommerce-loop-product__title">Watch Stainless with Grey Suture Leather Strap</h2>
                                                </a>
                                                <div class="hover-area">
                                                    <a class="button add_to_cart_button" href="/item/{{$b->id}}" rel="nofollow">Details</a>
                                                </div>
                                            </div>
                                            @endforeach
                                        
                                        </div>
                   
                                </div>
                                <!-- .woocommerce -->
                            </div>
                            <!-- .row -->
                        </div>
                        <!-- .products-carousel -->
                    </section>

                
                <!-- .section-3-2-3-product-cards-tabs-with-featured-product -->
                                                               

                <!-- .section-landscape-products-carousel -->
            </main>
            <!-- #main -->
        </div>
        <!-- #primary -->
    </div>
    <!-- .row -->
</div>
<!-- .col-full -->
</div>


@endsection