@extends('layouts.frontend.frontend')

@section('content')

    <div id="content" class="site-content">
    <div class="col-full">
        <div class="row">

            <!-- .woocommerce-breadcrumb -->
            <div class="content-area" id="primary">
                <main class="site-main" id="main">
                    <div class="type-page hentry">
                        <div class="entry-content">
                            <div class="woocommerce">    
                                <form role="form" method="POST" action="{{ url('/contact_produit/send') }}" accept-charset="UTF-8" id="mainForm" name="contact_form" class="checkout woocommerce-checkout" required >
                                    {{ csrf_field() }}
                                    <div id="customer_details" class="col2-set">
                                        <div class="col-1">
                                            <div class="woocommerce-billing-fields">
                                                <h3>Contact</h3>
                                                @if (count($errors) > 0)
                                                <div class="alert alert-danger">
                                                    @foreach($errors->all() as $err )
                                                        {{$err}} <br>
                                                    @endforeach
                                                </div>
                                                @endif
                                                <div class="woocommerce-billing-fields__field-wrapper-outer">
                                                    <div class="woocommerce-billing-fields__field-wrapper">
                                                        <p id="nom_field" class="form-row form-row-first validate-required ">
                                                            <label class="" for="billing_first_name">Nom
                                                                <abbr title="required" class="required">*</abbr>
                                                            </label>
                                                            <input type="text" value="" placeholder="" id="nom" name="nom" class="input-text " required>
                                                        </p>
                                                        <p id="prenom_field" class="form-row form-row-last validate-required">
                                                            <label class="" for="billing_last_name">Prenom
                                                                <abbr title="required" class="required">*</abbr>
                                                            </label>
                                                            <input type="text" value="" placeholder="" id="prenom" name="prenom" class="input-text " required>
                                                        </p>

                                                        <p id="tel_field" class="form-row form-row-first validate-required ">
                                                                <label class="" for="billing_first_name">Telephone
                                                                    <abbr title="required" class="required">*</abbr>
                                                                </label>
                                                                <input type="text" value="" placeholder="" id="tel" name="tel" class="input-text " required>
                                                            </p>
                                                            <p id="email_field" class="form-row form-row-last validate-required">
                                                                <label class="" for="billing_last_name">Email
                                                                    <abbr title="required" class="required">*</abbr>
                                                                </label>
                                                                <input type="text" value="" placeholder="" id="email" name="email" class="input-text " required>
                                                            </p>

                                                        <div class="clear"></div>
                                                        <p id="billing_company_field" class="form-row form-row-wide">
                                                            <label class="" for="billing_company">Sujet
                                                                    <abbr title="required" class="required">*</abbr>
                                                            </label>
                                                            <input type="text"  placeholder="" id="sujet" name="sujet" class="input-text " value="Demande de details sur ces produits" required>
                                                        </p>
                                                        
                                                        <p id="billing_email_field" class="form-row form-row-first validate-required validate-email">
                                                            <label class="" for="billing_email">Message
                                                                <abbr title="required" class="required">*</abbr>
                                                            </label>
                                                            <textarea aria-invalid="false" class="wpcf7-form-control wpcf7-textarea" rows="10" cols="40" name="message" id="message" required> Demande de details sur ces produits </textarea>
                                                        </p>

                                                         <!--<input type="hidden"  placeholder="" id="item_id" name="item_id" class="input-text " value="">-->

                                                    </div>
                                                </div>
                                                <!-- .woocommerce-billing-fields__field-wrapper-outer -->
                                            </div>
                                            <!-- .woocommerce-billing-fields -->
                                            
                                        </div>
                                    
                                    </div>
                                    <!-- .col2-set -->
                                    <h3 id="order_review_heading">Produit</h3>
                                    <div class="woocommerce-checkout-review-order" id="order_review">
                                        <div class="order-review-wrapper">
                                            <h3 class="order_review_heading">Produit</h3>
                                            <table class="shop_table woocommerce-checkout-review-order-table">
                                                <thead>
                                                    <tr>
                                                        <th class="product-name">Produits:</th>
                                                        <!--<th class="product-total">Prix</th>-->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                  @if( Session::has('cart'))
                                                    @foreach(Session::get('cart') as $cart)
                                                    <tr class="cart_item">
                                                        <td class="product-name">
                                                            <strong class="product-quantity"> {{$cart['quantity']}} x </strong> {{$cart['product']->title}}
                                                        </td>
                                                        <td class="product-total">
                                                            <span class="woocommerce-Price-amount amount">
                                               
                                                            @if($cart['product']->promotion)
                                                                    <span class="woocommerce-Price-currencySymbol"> {{$cart['product']->price_promotion}}DH</span>
                                                            @else
                                                                    <span class="woocommerce-Price-currencySymbol"> {{$cart['product']->price}}DH</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                   @endforeach
                                                  

                                                    </tbody>
                                                     <tfoot>
                                                        <tr class="order-total">
                                                            <th>Total</th>
                                                            <td>
                                                                    <strong>
                                                                        <span class="woocommerce-Price-amount amount">
                                                                            <span class="woocommerce-Price-currencySymbol"></span>{{ Session::get('total_price') }}DH</span>
                                                                    </strong>
                                                                </td>
                                                            </tr>
                                                      </tfoot>
                                                @endif
                                                   
                                             
                                              
                                            </table>

                                            <!-- /.woocommerce-checkout-review-order-table -->
                                            <div class="woocommerce-checkout-payment" id="payment">
                                                <ul class="wc_payment_methods payment_methods methods">
                                                    <li class="wc_payment_method payment_method_bacs">
                                                        <input type="radio" data-order_button_text="" checked="checked" value="virement" name="payment" class="input-radio" id="virement">
                                                        <label for="virement">Virement ou Versement bancaire</label>
                                                        <P>Effectuez un virement/versement bancaire sur le compte ci-dessous:

                                                            Banque: banque populaire 
                                                            Bénéficiaire: NDCPRO MAROC
                                                            Code Banque : 
                                                            Code Ville : 
                                                            N° de compte : 
                                                            Clé RIB : </P>
                                                        
                                                    </li>
                                                    <li class="wc_payment_method payment_method_cheque">
                                                        <input type="radio" data-order_button_text="" value="cheque" name="payment" class="input-radio" id="cheque">
                                                        <label for="cheque">Paiement par chèque</label>
                                                        <p>
                                                            Remplissez un chèque en dirhams et compensable au Maroc (ie tiré sur une banque marocaine) avec le montant de la commande  en le libellant à l'ordre de : NDCPRO MAROC .
                                                        </p>
                                                        
                                                    </li>
                                                  
                                                </ul>

                                                <div class="form-row place-order">
                                                    <p class="form-row terms wc-terms-and-conditions woocommerce-validated">
                                                        <label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
                                                            <span>N'oubliez pas de nous envoyer un scan du reçu par email à l'adresse suivante: commande@ndcpromaroc.com</span>
                                                        </label>
                                                        <input type="hidden" value="1" name="terms-field">
                                                    </p>
                                                </div>
                                               
                                            </div>
                                            <!-- /.woocommerce-checkout-payment -->

                                            <!-- /.woocommerce-checkout-review-order-table -->
                                            <div class="woocommerce-checkout-payment" id="payment">
                                                <div class="form-row place-order">
                                                    <input type="submit" value="Commander" class="button wc-forward text-center" />
                                                </div>
                                            </div>
                                            <!-- /.woocommerce-checkout-payment -->
                                        </div>
                                        <!-- /.order-review-wrapper -->
                                    </div>
                                    <!-- .woocommerce-checkout-review-order -->
                                </form>
                                <!-- .woocommerce-checkout -->
                            </div>
                            <!-- .woocommerce -->
                        </div>
                        <!-- .entry-content -->
                    </div>
                    <!-- #post-## -->
                </main>
                <!-- #main -->
            </div>
            <!-- #primary -->
        </div>
        <!-- .row -->
    </div>
    </div>
    <!-- .row -->


@endsection