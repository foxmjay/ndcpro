@extends('layouts.frontend.frontend')

@section('content')
<div id="content" class="site-content">
                <div class="col-full">
                    <div class="row">
                        <nav class="woocommerce-breadcrumb">
                            <a href="/">Home</a>
                            <span class="delimiter">
                                <i class="tm tm-breadcrumbs-arrow-right"></i>
                            </span>
                            A Propos de nous
                        </nav>
                        <!-- .woocommerce-breadcrumb -->
                        <div id="primary" class="content-area">
                            <main id="main" class="site-main">
                                <div class="type-page hentry">
                                    <header class="entry-header">
                                        <div class="page-featured-image">
                                            <img width="1920" height="1391" alt="" class="attachment-full size-full wp-post-image" src="{{ URL::asset('assets//images/contact.png')}}">
                                        </div>
                                        <!-- .page-featured-image -->
                                        <div class="page-header-caption">
                                            <h1 class="entry-title">Présentation</h1>
                                            <p class="entry-subtitle"> NDC PRO MAROC est reconnue sur le marché marocain, par la qualité de ses prestations fournies, et par la compétence de ses collaborateurs qui sont issus des meilleurs cursus de formation.
Nous accompagnons les entreprises, nous nous sentons pleinement concernés par les défis futurs de nos clients-partenaires. Nous bâtissons ensemble des relations de confiance et de collaboration sereine.
Grâce au soutien de nos partenaires, nous concevons et installons des solutions clé en main en mettant à votre disposition les compétences ci-dessous : </p>
                                        </div>
                                        <!-- .page-header-caption -->
                                    </header>
                                    <!-- .entry-header -->
                                    <div class="entry-content">
                                        <div class="about-features row">
                                            <div class="col-md-4">
                                                <div class="single-image">
                                                    <img alt="" class="" src="{{ URL::asset('assets/images/aboutus/2.png')}}">
                                                </div>
                                                <!-- .single_image -->
                                                <div class="text-block">
                                                    <h2 class="align-top"></h2>
                                                    <p>
                                                    Mise en place de solutions de télésurveillance, de contrôle d'accès, de pointage et de monitoring, Sécurisation des systèmes (antivirus, firewall, contrôleur d'accès, etc …),
Mise en place de solutions informatiques globales, Gestion et supervision des systèmes d'informations. 
Conception, implantation, configuration, test, assistance technique et maintenance de solutions informatiques câblées ou wifi
                                                    </p>
                                                </div>
                                                <!-- .text_block -->
                                            </div>
                                            <!-- .col -->
                                            <div class="col-md-4">
                                                <div class="single-image">
                                                    <img alt="" class="" src="{{ URL::asset('assets/images/aboutus/1.png')}}">
                                                </div>
                                                <!-- .single_image -->
                                                <div class="text-block">
                                                    <h2 class="align-top"></h2>
                                                    <p>
                                                    Réalisation des systèmes privées de transmission de données ADSL, LS, RNJS, VPN, y compris câblage intégrant voix, données, images sur câbles torsadées, fibre optique, coaxiale … .
Mise en place de solutions téléphoniques (PABX), interconnexion de réseaux locaux LAN et WAN (routeurs, modems, convertisseur, etc. …) 
                                                    </p>
                                                </div>
                                                <!-- .text_block -->
                                            </div>
                                            <!-- .col -->
                                            <div class="col-md-4">
                                                <div class="single-image">
                                                    <img alt="" class="" src="{{ URL::asset('assets/images/aboutus/3.png')}}">
                                                </div>
                                                <!-- .single_image -->
                                                <div class="text-block">
                                                    <h2 class="align-top"></h2>
                                                    <p>
                                                    Fournitures de matériels et accessoires informatiques et Développement de site web, d'applicatifs verticaux et systèmes.
REFERNCES CLIENTS : SEWS MAROC KENITRA, SEWS MAROC MFZ, COFOCAB, FUJUKURA, HIRCHMAN, MINISTERE DES AFFAIRES ISLAMIQUES HABOUSS
GERANT : WAHBI MOURAD
</p>
                                                </div>
                                                <!-- .text_block -->
                                            </div>
                                            <!-- .col -->
                                        </div>
                                       
                                    </div>
                                    <!-- .entry-content -->
                                </div>
                                <!-- .hentry -->
                            </main>
                            <!-- #main -->
                        </div>
                        <!-- #primary -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .col-full -->
            </div>

@endsection