<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model{
      /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'items';

    /**
* The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                'group_id',
                'title',
                'description',
                'side_description',
                'image1',
                'image2',
                'image3',
                'image4',
                'image5',
                'video',
                'tags',
                'menu',
                'price',
                'promotion',
                'price_promotion',
                'price_start',
                'created_at',
                'updated_at',
              ];

}
