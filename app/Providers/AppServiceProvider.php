<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

use App\Menu;
use App\Categorie;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //$categories = Menu::all();
        $menuList = Menu::getMenu(-1);

        //view::share('categories',$categories);
        view::share('menuList',$menuList);
        
    }
}
