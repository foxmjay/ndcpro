<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Message extends Model{
      /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'messages';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                'items',
                'type',
                'nom',
                'prenom',
                'email',
                'tel',
                'sujet',
                'message',
                'tag',
                'payment',
                'created_at',
                'updated_at',
              ];


    
    /*public function item(){
      return $this->belongsTo('App\Item','item_id');
    }*/

    public static function getMessagesCount(){

      $msgs = Message::where('tag','Like','Viewed')->get(['id']);
      return count($msgs);
    }

}
