<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class MessageItem extends Model{
      /**
     * The database table used by the model.
     *
     * @var string
     */
    public $timestamps = false;
    protected $table = 'message_items';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                'message_id',
                'item_id',
                'quantity'
              ];


    public function item(){
      return $this->belongsTo('App\Item','item_id');
    }
}
