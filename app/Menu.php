<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Menu extends Model{
      /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'menus';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                'title',
                'parent_id',
                'type',
                'url',
                'group_id',
                'tag',
                'created_at',
                'updated_at',
              ];


    public static function getMenu($id){
        if($id == -1)
          return Menu::where('parent_id',null)->orderby('id','asc')->get();
        else
          return Menu::where('parent_id',$id)->orderby('id','asc')->get();
    }

    public static function getMenuParent($id){
      if($id == -1)
        return "#";
      else{
          $menu = Menu::FindOrFail($id);
          if($menu->parent_id == null)
            return -1;
          else
            return $menu->parent_id;
      }
  }
    
    public function group(){
      return $this->belongsTo('App\Group','group_id');
    }


    public function menu(){
      return $this->belongsTo('App\Menu','parent_id');
    }
}
