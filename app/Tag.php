<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
      /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tags';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                'name',
                'display',
                'created_at',
                'updated_at',
              ];


    public static function getTags($string){
      $tagids = explode(';',$string);
      $tags =Tag::findMany($tagids);
      $tagsDisplay=[];
      foreach($tags as $t){
        array_push($tagsDisplay,$t->display."_".$t->id);
      }

      return $tagsDisplay;
      
    }
}
