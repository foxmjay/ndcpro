<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
      /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                'parent_id',
                'type',
                'title',
                'url',
                'tag',
                'image_small',
                'image_big',
                'created_at',
                'updated_at',
              ];


    public static function getCategorie($id){
        if($id == -1)
          return Categorie::where('parent_id',null)->get();
        else
          return Categorie::where('parent_id',$id)->get();
    }

    public static function getCategorieParent($id){
      if($id == -1)
        return "#";
      else{
          $categorie = Categorie::FindOrFail($id);
          if($categorie->parent_id == null)
            return -1;
          else
            return $categorie->parent_id;
      }
  }
    


    public function categorie(){
      return $this->belongsTo('App\Categorie','parent_id');
    }
}
