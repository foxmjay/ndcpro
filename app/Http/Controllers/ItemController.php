<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use App\Item;
use App\Group;
use App\Menu;
use App\Tag;

use File;

class ItemController extends Controller{
    
    

    public function index($id){

        $items = Collection::make(new Item);
        $group = Group::findOrFail($id);
        if($id != null)
            $items = Item::where('group_id',$id)->get();
      
        return view('backend.items.index',compact('items','group'));
    }


    public function create($id){
        $group_id=$id;
        $menus = Menu::all();
        $tags = Tag::orderBy('name','asc')->get();
        return view('backend.items.create',compact('group_id','menus','tags'));
    }

    public function store(Request $request, $id){

        $parent=$id;


        if($request->has('new'))
            $request['new']=1;
        else
            $request['new']=0;

        if($request->has('promotion'))
            $request['promotion']=1;
        else
            $request['promotion']=0;

        if($request->has('price_start'))
            $request['price_start']=1;
        else
            $request['price_start']=0;

        $this->validate($request, [
            'title' => 'nullable|string',
            'description' => 'nullable|string',
            'side_description' => 'nullable|string',
            'video' => 'nullable|string',
            'tags' => 'nullable|string',
            'price' => 'nullable',
            'price_promotion' => 'nullable',
            'promotion' => 'nullable',
            'price_start' => 'nullable',

        ]);
        $requestData = $request->all();
        $requestData['group_id']=$id;
        $requestData['tags']=';'.$requestData['tags'].';';

        for($i=1; $i<=5;$i++){
            if($request->hasFile('image'.$i)){
                $path = $request['image'.$i]->store('uploads','public');
                $requestData['image'.$i] = $path;
            }
        }


        Item::create($requestData);
        
        return redirect('dashboard/items/'.$id);
    }

    public function edit($id){
        $item = Item::findOrFail($id);
        $menus = Menu::all();
        $tags = Tag::orderBy('name','asc')->get();
        return view('backend.items.edit',compact('item','menus','tags'));
    }
    

    public function update(Request $request, $id){

        $item = Item::findOrFail($id);
        
        if($request->has('new'))
            $request['new']=1;
        else
            $request['new']=0;

        if($request->has('promotion'))
            $request['promotion']=1;
        else
            $request['promotion']=0;

        if($request->has('price_start'))
            $request['price_start']=1;
        else
            $request['price_start']=0;

        $this->validate($request, [
            'title' => 'nullable|string',
            'description' => 'nullable|string',
            'side_description' => 'nullable|string',
            //'video' => 'nullable|max:1000',
            'tags' => 'nullable|string',
            'menu' => 'nullable',
            'price' => 'nullable',
            'price_promotion' => 'nullable',
            'promotion' => 'nullable',
            'price_start' => 'nullable',
            

            
        ]);
        $requestData = $request->all();
        $requestData['tags']=';'.$requestData['tags'].';';

        for($i=1; $i<=5;$i++){
            if($request->hasFile('image'.$i)){

                $oldImagePath=public_path()."/storage/".$item['image'.$i];
                File::delete($oldImagePath);

                
                $path = $request['image'.$i]->store('uploads','public');
                $requestData['image'.$i] = $path;
            }
        }
        
        $item->update($requestData);
        
        return redirect('dashboard/items/'.$item->group_id);
    }


    public function destory($id){
        $item = Item::findOrFail($id);
        $item->delete();
        return redirect('dashboard/items/'.$item->group_id);
    }
    
}
