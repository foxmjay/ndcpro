<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Redirect,Response,Config;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Collection;

use App\Item;
use App\Group;
use App\Menu;

class DashboardController extends Controller{


    public function home(){

        return view('backend.home');
    }

    public function search(Request $request){

        $keyword = $request->get('keyword');
        $type_search = $request->get('type_search'); 
        $checkbox = $request->get('checkbox_title'); 
        
        $sql="";
        if( $type_search == "items"){
            $result = Collection::make(new Item);
            $sql= "Select id, title, image1 as image from items";
        }elseif ($type_search == "groups"){
            $result = Collection::make(new Group);
            $sql= "Select id, title, image from groups ";
        }

        elseif( $type_search == "menus"){
            $result = Collection::make(new Menu);
            $sql= "Select id, title, NULL as image from menus ";
        }

        $filter = [];
        if (!empty($keyword) || $request->has('checkbox_new') ) {

            if($request->has('checkbox_title'))
              array_push($filter,"title like '%".$keyword."%'");

            if($request->has('checkbox_description'))
              array_push($filter,"description like '%".$keyword."%'");
            
            if($request->has('checkbox_mini_description'))
              array_push($filter,"side_description like '%".$keyword."%'");

            if($request->has('checkbox_tag'))
              array_push($filter,"tags like '%".$keyword."%'");

            if(count($filter)>0)
                $sql=$sql." WHERE ";

            $sql=$sql.implode(" OR ",$filter)." ORDER BY RAND();";

            $result = DB::select($sql);

        }

        return view('backend.home',compact('result','type_search'));
    }

    
}
