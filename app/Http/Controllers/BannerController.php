<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use App\Banner;
use App\Group;
use App\Tag;
use File;

class BannerController extends Controller{
    
    public function index(){
        $banners = Banner::all();
        return view('backend.banners.index',compact('banners'));
    }


    public function create(){
        $groups = Group::all();
        $tags = Tag::orderBy('name','asc')->get();

        return view('backend.banners.create',compact('groups','tags'));
    }

    public function store(Request $request){

        $this->validate($request, [
            'title' => 'nullable|string',
            'type' => 'nullable|string',
            'url' => 'nullable|string',
            'group_id' => 'nullable|numeric',
            'tags' => 'nullable|string',

        ]);
        $requestData = $request->all();

        if($request->hasFile('image')){
            $path = $request->image->store('uploads','public');
            $requestData['image'] = $path;
        }
        $requestData['tags']=';'.$requestData['tags'].';';

        
        Banner::create($requestData);
        
        return redirect('dashboard/banners/');
    }

    public function edit($id){
        $banner = Banner::findOrFail($id);
        $groups = Group::all();
        $tags = Tag::orderBy('name','asc')->get();
        return view('backend.banners.edit',compact('banner','groups','tags'));
    }
    

    public function update(Request $request, $id){

        $banner = Banner::findOrFail($id);

        $this->validate($request, [
            'title' => 'nullable|string',
            'type' => 'nullable|string',
            'url' => 'nullable|string',
            'group_id' => 'nullable|numeric',
            'tags' => 'nullable|string',

        ]);
        $requestData = $request->all();
        
        if($request->hasFile('image')){

            $oldImagePath=public_path()."/storage/".$banner->image;
            File::delete($oldImagePath);

            $path = $request->image->store('uploads','public');
            $requestData['image'] = $path;
        }
        $requestData['tags']=';'.$requestData['tags'].';';
        $banner->update($requestData);
        
        return redirect('dashboard/banners/');
    }


    public function destory($id){
        $banner = Banner::findOrFail($id);
    

        $banner->delete();
        return redirect('dashboard/banners/');
    }

}
