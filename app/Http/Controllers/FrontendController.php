<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

use App\Group;
use App\Item;
use App\Banner;
use App\Menu;
use App\Categorie;
use App\Tag;

class FrontendController extends Controller{
    

    public function index(){

        $tag= Tag::where('name','like','%mainbanner%')->first();
        $banners = Banner::where('tags','like','%;'.strval($tag->id).';%')->limit(8)->get();

        $tag= Tag::where('name','like','%smallbanner%')->first();
        $smallBanners = Banner::where('tags','like','%;'.strval($tag->id).';%')->limit(4)->get();

        $tag= Tag::where('name','like','%banner_left%')->first();
        $bannerLeft = Banner::where('tags','like','%;'.strval($tag->id).';%')->first();

        $tag= Tag::where('name','like','%banner_center%')->first();
        $bannerCenter = Banner::where('tags','like','%;'.strval($tag->id).';%')->first();

        $tag= Tag::where('name','like','%banner_right%')->first();
        $bannerRight = Banner::where('tags','like','%;'.strval($tag->id).';%')->first();

        
        //$bestCategories = Categorie::whereNull('parent_id')->orderByRaw("RAND()")->limit(6)->get();
        $partenaires = Group::where('type','Partner')->orderByRaw("RAND()")->limit(12)->get();

        //$foryou = Item::orderByRaw("RAND()")->limit(12)->get();

        /*$tag= Tag::where('name','like','%nouveau%')->first();
        $news = Item::where('tags','like','%;'.strval($tag->id).';%')->limit(6)->get();
        $tag= Tag::where('name','like','%promotion%')->first();
        $prom = Item::where('tags','like','%;'.strval($tag->id).';%')->limit(12)->get();
        $tag= Tag::where('name','like','%meilleur%')->first();
        $best = Item::where('tags','like','%;'.strval($tag->id).';%')->limit(6)->get();*/

        //$tag= Tag::where('name','like','%promotion%')->first();
        $promCategories = Categorie::whereNotNull('parent_id')->orderByRaw("RAND()")->limit(1)->get();
        $promotions = array();
        foreach($promCategories as $cat){
            #$data = Item::orderByRaw("RAND()")->where('tags','like','%;'.strval($tag->id).';%')->limit(5)->get();
            $data = Item::orderByRaw("RAND()")->where('promotion',1)->limit(5)->get();
            if (count($data) >=5){


                $left = $data->slice(0)->take(2);
                $right = $data->slice(3)->take(2);
                $bottom = $data->slice(6)->take(2);
                $special = $data->last();

                $cc = collect(['left' => $left, 'right' => $right, 'bottom'=>$bottom ,'special'=>$special ]);

                $promotions[$cat->title]=$cc;
            }else{

                $promotions = null;
            }
        }
        
        //$bestCategories = Categorie::whereNotNull('parent_id')->orderByRaw("RAND()")->limit(1)->get();
        /*$newProducts = array();
        foreach($bestCategories as $cat){
            $data = Item::orderByRaw("RAND()")->limit(7)->get();
            $newProducts[$cat->title]=$data;
        }*/
        //$newProducts =  Item::orderByRaw("RAND()")->limit(7)->get();
        $tag= Tag::where('name','like','%nouveau%')->first();
        $newProducts = Item::orderByRaw("RAND()")->where('tags','like','%;'.strval($tag->id).';%')->limit(7)->get();
        



        /*$schoolProducts = array();
        foreach($bestCategories as $cat){
            $data = Item::orderByRaw("RAND()")->limit(7)->get();
            $schoolProducts[$cat->title]=$data;
        }*/
       //$schoolProducts = Item::orderByRaw("RAND()")->limit(7)->get();
        $tag= Tag::where('name','like','%interessant%')->first();
        $interestingProducts = Item::orderByRaw("RAND()")->where('tags','like','%;'.strval($tag->id).';%')->limit(7)->get();


        /*$laptopProducts = array();
        foreach($bestCategories as $cat){
            $data = Item::orderByRaw("RAND()")->limit(7)->get();
            $laptopProducts[$cat->title]=$data;
        }*/
        //$categoriesProducts = Item::orderByRaw("RAND()")->limit(7)->get();
        $tag= Tag::where('name','like','%best_categories%')->first();
        $categoriesProducts = Item::orderByRaw("RAND()")->where('tags','like','%;'.strval($tag->id).';%')->limit(7)->get();



        return view('frontend.home',compact('banners','smallBanners','partenaires','promotions','newProducts','categoriesProducts','interestingProducts','bannerLeft','bannerCenter','bannerRight'));
    }

    public function item($id){
        //Session::forget('cart');
        //Session::forget('total_price');
        //$cart = Session::get('cart');
        //return $cart['product'];


        $product = Item::findOrFail($id);
        $groupDesc = Group::find($product->group_id);

        $related = Item::where('group_id',$product->group_id)->limit(16)->get();
        $recent = Item::orderByRaw("RAND()")->limit(16)->get();

        return view('frontend.item',compact('product','related','recent','groupDesc'));
    }

    public function list($id){

        $groupDesc = Group::find($id);

        //if(empty($groupDesc))
         //   return redirect('/');

        $products = Item::where('group_id',$id)->orderByRaw("RAND()")->paginate(21);        
        return view('frontend.list',compact('products','groupDesc'));
    }

    public function menutags($id){

        /*$tags = explode(',',$menu->tag);
        //$items = Collection::make(new Menu);
        $items = DB::table('items');
        foreach($tags as $tag){
            $items = $items->orWhere('tags','like','%'.$tag.'%');
        }

        $items = $items->get();*/
        $menu = Menu::findOrFail($id);
        $products = Item::where('menu','like','%'.$menu->title.'%')->paginate(21);
            
        return view('frontend.list',compact('products'));
    }

    public function tags($tag){

        /*$tags = explode(',',$menu->tag);
        //$items = Collection::make(new Menu);
        $items = DB::table('items');
        foreach($tags as $tag){
            $items = $items->orWhere('tags','like','%'.$tag.'%');
        }

        $items = $items->get();*/
        
        $products = Item::where('tags','like','%;'.$tag.';%')->limit(21)->get();
            
        return view('frontend.list',compact('products'));
    }

    public function contact(){

        return view('frontend.contact');
    }

    public function contact_message(){


        return view('frontend.message');
    }

    public function contact_produit(){


        return view('frontend.contact_produit');
    }

    public function add_produit(Request $request){

    
        if(Session::has('cart')){
            $cart = Session::get('cart');
            $price = Session::get('total_price');

            $item = Item::findOrFail($request->item_id);
            $exists=0;

            foreach($cart as $key => $ct){
                if($ct['product']->id == $item->id){
                    $ct['quantity']+=$request->quantity;
                    $cart[$key]=$ct;
                    $exists=1;
                    break;
                }
            }

            if($exists == 0)
                $cart->push(['product'=> $item, 'quantity'=>$request->quantity]);

            if($item->promotion)
                $price+=$item->price_promotion*$request->quantity;
            else
                $price+=$item->price*$request->quantity;
            Session::put('total_price', $price);


        }else{
            $item = Item::findOrFail($request->item_id);
            $cart=collect();
            $cart->push(['product' => $item, 'quantity' => $request->quantity]);
            Session::put('cart', $cart);
            $price=0;
            if($item->promotion)
                $price=$item->price_promotion*$request->quantity;
            else
                $price=$item->price*$request->quantity;
            Session::put('total_price', $price);

        }
        

        //return redirect('/item/'.$item_id);
        return redirect()->back();

    }


    public function remove_produit($item_id){

    
        if(Session::has('cart')){

            $cart = Session::get('cart');
            $price = Session::get('total_price');

            foreach($cart as $key => $value){
                if($value['product']->id == $item_id){
                    $cart->forget($key);
                    if($value['product']->promotion)
                        $price-=$value['product']->price_promotion*$value['quantity'];
                    else
                        $price-=$value['product']->price*$value['quantity'];

                    Session::put('total_price', $price);

                }
            }
            Session::put('cart', $cart);
        }
        

        return redirect()->back();
    }

    public function mobile_cart(){

        return view('frontend.mobile_cart');
    }

    /*public function contact(){
        return view('frontend.contact');
    }*/

    public function aboutus(){
        return view('frontend.aboutus');
    }

    public function onecar(){
        return view('frontend.onecar');
    }

    public function oneteeth(){
        return view('frontend.oneteeth');
    }

    public function onedoc(){
        return view('frontend.onedoc');
    }


    public function group($id){
        $group = Group::findOrFail($id);
        $items = Item::where('group_id',$group->id)->get();
        return view('frontend.group',compact('group','items'));
    }

    public function search(Request $request){

        $categorie = -1;
        $keyword = "";

        if($request->has('page')){  
            if(Session::has('keyword')){
                $keyword = Session::get('keyword');
                $categorie = Session::get('categorie');
            }
        }else{

            $keyword = $request->keyword;
            $categorie = $request->categorie;
            Session::put('keyword', $keyword);
            Session::put('categorie', $categorie);
        }


        $catname="";
        $menu=Menu::where('id',$categorie)->first();
        if($menu != null)
            $catname = $menu->title;
        
        if(intval($categorie) == -1 )
            $products = Item::where('title','Like','%'.$keyword.'%')->paginate(21);
        else{
            $products = Item::where('title','Like','%'.$keyword.'%')->where('menu','LIKE','%'.$catname.'%')->paginate(2);
        }
        return view('frontend.list',compact('products'));
    }


    public function searching(Request $request){
	
        #return URL::current();
        #return basename(URL::current());
        $keyword = explode('-',basename(request()->path()))[0];
        #return $keyword;

        $groupDesc = Group::where('title','LIKE','%'.$keyword.'%')->first();
        if(empty($groupDesc))
            return redirect('/');
        $products = Item::where('group_id',$groupDesc->id)->orderByRaw("RAND()")->paginate(21);        
        return view('frontend.list',compact('products','groupDesc'));
    }

    public function searchingCombo(Request $request){
	
        
        
        $keyword = explode('-',basename(request()->path()));
        

        //$groupDesc = Group::where('title','LIKE','%'.$keyword.'%')->first();
        //if(empty($groupDesc))
        //    return redirect('/');
        //$products = Item::where('group_id',$groupDesc->id)->orderByRaw("RAND()")->paginate(21);
        $groupDesc = null;
        $products = Item::where('title','Like','%'.$keyword[0].'%')
                          ->orWhere('title','LIKE','%'.$keyword[1].'%')
                          ->orWhere('menu','LIKE','%'.$keyword[0].'%')
                          ->orWhere('menu','LIKE','%'.$keyword[1].'%')
                          ->paginate(21);

        return view('frontend.list',compact('products','groupDesc'));
    }

    public function promotions(){
    
        $products = Item::orderByRaw("RAND()")->where('promotion',1)->paginate(21); 
        return view('frontend.list',compact('products'));
    }



    /*public function index(){
        $banners = Banner::all();
        return view('frontend.index',compact('banners'));
    }

    public function index2(){

        $news = Item::where('tags','like','%nouveau%')->limit(6)->get();
        $prom = Item::where('tags','like','%promotion%')->limit(6)->get();
        $best = Item::where('tags','like','%meilleur%')->limit(6)->get();
        $categories = Categorie::getCategorie(-1);
        $banners = Banner::all();
        return view('frontend.index2',compact('banners','news','prom','best','categories'));
    }*/


    /*public function item($id){
        $item = Item::findOrFail($id);
        $group = Group::findOrFail($item->group_id);

        $randomItems = Item::all();
        if(count($randomItems) > 4)
            $randomItems = $randomItems->random(4);


        return view('frontend.item',compact('item','group','randomItems'));
    }*/


    /*public function tag($id){
        $menu = Menu::findOrFail($id);
        //$tags = explode(',',$menu->tag);
        //$items = Collection::make(new Menu);
        //$items = DB::table('items');
        //foreach($tags as $tag){
        //    $items = $items->orWhere('tags','like','%'.$tag.'%');
       // }

        $items = $items->get()

        //$items = Item::where('menu','like','%'.$menu->title.'%')->get();
            
        //return view('frontend.group',compact('menu','items'));
    }*/

}

