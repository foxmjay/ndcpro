<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use App\Tag;

class TagController extends Controller{
    public function index(){
        $tags = Tag::all();
        return view('backend.tags.index',compact('tags'));
    }


    public function create(){
        return view('backend.tags.create');
    }


    public function store(Request $request){

        $this->validate($request, [
            'display' => 'nullable|string',
            'name' => 'nullable|string',

        ]);
        
        $requestData = $request->all();

        //$requestData['name']=str_replace(" ","_",$requestData['display']);
        
        Tag::create($requestData);
        
        return redirect('dashboard/tags/');
    }

    public function update(Request $request, $id){

        
        $tag = Tag::findOrFail($id);

        $this->validate($request, [
            'display' => 'nullable|string',
            'name' => 'nullable|string',

        ]);

        $requestData = $request->all();

        //$requestData['name']=str_replace(" ","_",$requestData['display']);
   
        $tag->update($requestData);
        
        return redirect('dashboard/tags/');
    }


    public function edit($id){
        $tag = Tag::findOrFail($id);
        return view('backend.tags.edit',compact('tag'));
    }


    public function destory($id){
        $tag = Tag::findOrFail($id);
        $tag->delete();
        return redirect('dashboard/tags/');
    }

}
