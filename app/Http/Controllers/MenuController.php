<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use App\Menu;
use App\Group;

class MenuController extends Controller{
    

    public function index($id){
        $menus = Collection::make(new Menu);
        $parent=$id;

        if($id < 0){
            $menus = Menu::where('parent_id',null)->get();
        }else{
            $menus = Menu::where('parent_id',$id)->get();
        }

        return view('backend.menus.index',compact('menus','parent'));
    }


    public function create($id){
        $parent=$id;
        $groups = Group::all();
        return view('backend.menus.create',compact('parent','groups'));
    }

    public function store(Request $request, $id){
        $parent=$id;

        $this->validate($request, [
            'title' => 'nullable|string',
            'type' => 'nullable|string',
            'url' => 'nullable|string',
            'group_id' => 'nullable|numeric',
            'tag' => 'nullable|string',
        ]);
        $requestData = $request->all();

        if($id < 0)
            $requestData['parent_id']=null;
        else
            $requestData['parent_id']=$id;
        
        Menu::create($requestData);
        
        return redirect('dashboard/menus/'.$id);
    }

    public function edit($id){
        $menu = Menu::findOrFail($id);
        $groups = Group::all();
        return view('backend.menus.edit',compact('menu','groups'));
    }
    

    public function update(Request $request, $id){

        $menu = Menu::findOrFail($id);

        $this->validate($request, [
            'title' => 'nullable|string',
            'type' => 'nullable|string',
            'url' => 'nullable|string',
            'group_id' => 'nullable|numeric',
            'tag' => 'nullable|string',

        ]);
        $requestData = $request->all();
        $parent = -1;
        if($menu->parent_id != null)
            $parent = $menu->parent_id;
            
        
        
        $menu->update($requestData);
        
        return redirect('dashboard/menus/'.$parent);
    }


    public function destory($id){
        $menu = Menu::findOrFail($id);
        $parent = -1;
        if($menu->parent_id != null)
            $parent = $menu->parent_id;

        $menu->delete();
        return redirect('dashboard/menus/'.$parent);
    }
    

}
