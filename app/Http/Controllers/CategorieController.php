<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;



use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use App\Categorie;
use App\Tag;

use File;

class CategorieController extends Controller
{
    public function index($id){

        $categories = Collection::make(new Categorie);
        $parent=$id;

        if($id < 0){
            $categories = Categorie::where('parent_id',null)->get();
        }else{
            $categories = Categorie::where('parent_id',$id)->get();
        }

        return view('backend.categories.index',compact('categories','parent'));
    }

    public function create($id){
        $parent=$id;
        $tags = Tag::all();

        return view('backend.categories.create',compact('parent','tags'));
    }

    public function store(Request $request, $id){
        $parent=$id;

        $this->validate($request, [
            'title' => 'nullable|string',
            'url' => 'nullable|string',
            'tag' => 'nullable|string',
        ]);

        

        $requestData = $request->all();

        if($request->hasFile('image_small')){
            $path = $request['image_small']->store('uploads','public');
            $requestData['image_small'] = $path;
        }

        if($request->hasFile('image_big')){
            $path = $request['image_big']->store('uploads','public');
            $requestData['image_big'] = $path;
        }


        if($id < 0)
            $requestData['parent_id']=null;
        else
            $requestData['parent_id']=$id;
        
        Categorie::create($requestData);
        
        return redirect('dashboard/categories/'.$id);
    }

    public function edit($id){
        $categorie = Categorie::findOrFail($id);
        $tags = Tag::all();
        return view('backend.categories.edit',compact('categorie','tags'));
    }

    public function update(Request $request, $id){

        $categorie = Categorie::findOrFail($id);

        $this->validate($request, [
            'title' => 'nullable|string',
            'url' => 'nullable|string',
            'tag' => 'nullable|string',

        ]);
        $requestData = $request->all();

        if($request->hasFile('image_small')){

            $oldImagePath=public_path()."/storage/".$categorie->image_small;
            File::delete($oldImagePath);

            $path = $request['image_small']->store('uploads','public');
            $requestData['image_small'] = $path;
        }

        if($request->hasFile('image_big')){

            $oldImagePath=public_path()."/storage/".$categorie->image_big;
            File::delete($oldImagePath);

            $path = $request['image_big']->store('uploads','public');
            $requestData['image_big'] = $path;
        }

        
        $parent = -1;
        if($categorie->parent_id != null)
            $parent = $categorie->parent_id;
            
        
        
        $categorie->update($requestData);
        
        return redirect('dashboard/categories/'.$parent);
    }

    public function destory($id){
        $categorie = Categorie::findOrFail($id);
        $parent = -1;
        if($categorie->parent_id != null)
            $parent = $categorie->parent_id;

        $categorie->delete();
        return redirect('dashboard/categories/'.$parent);
    }

    
}

