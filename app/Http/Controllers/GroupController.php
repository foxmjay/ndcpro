<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use App\Group;
use App\Item;
use File;

class GroupController extends Controller{

     
    public function index(){
        $groups = Group::all();
        return view('backend.groups.index',compact('groups'));
    }


    public function create(){
        return view('backend.groups.create');
    }

    public function store(Request $request){

        $this->validate($request, [
            'title' => 'nullable|string',
            'type' => 'nullable|string',
            'description' => 'nullable|string',
        ]);
        
        $requestData = $request->all();

        if($request->hasFile('image')){
            $path = $request->image->store('uploads','public');
            $requestData['image'] = $path;
        }
        
        Group::create($requestData);
        
        return redirect('dashboard/groups/');
    }

    public function edit($id){
        $group = Group::findOrFail($id);
        return view('backend.groups.edit',compact('group'));
    }
    

    public function update(Request $request, $id){

        $group = Group::findOrFail($id);

        $this->validate($request, [
            'title' => 'nullable|string',
            'type' => 'nullable|string',
            'description' => 'nullable|string',
            
        ]);
        $requestData = $request->all();

        if($request->hasFile('image')){

            $oldImagePath=public_path()."/storage/".$group->image;
            File::delete($oldImagePath);

            $path = $request->image->store('uploads','public');
            $requestData['image'] = $path;
        }

   
        $group->update($requestData);
        
        return redirect('dashboard/groups/');
    }


    public function destory($id){
        $group = Group::findOrFail($id);
        $items = Item::where('group_id',$id)->get();
        
        if(count($items) > 0){
           return  redirect('dashboard/groups')->with('error', 'The Group has elements , please delete them first.');
        }
        else{
            $group->delete();
            return redirect('dashboard/groups/')->with('success', 'Group Deleted.');
        }
    }
    

}
