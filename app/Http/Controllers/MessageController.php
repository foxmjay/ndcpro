<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Session;


use App\Jobs\SendEmailJob;
use App\Message;
use App\Group;
use App\MessageItem;
use App\Item;

class MessageController extends Controller{
    

    public function index(){
        $messages = Message::orderBy('created_at','DESC')->get();
        return view('backend.messages.index',compact('messages'));
    }


    /*public function create($id){
        $parent=$id;
        $groups = Group::all();
        return view('backend.messages.create',compact('parent','groups'));
    }*/

    public function store(Request $request){
        

        $this->validate($request, [
            'nom' => 'string',
            'prenom' => 'string',
            'email' => 'email',
            'tel' => 'string',
            'sujet' => 'string',
            'message' => 'string',
            'type' => 'nullable|string',
        ]);
        $requestData = $request->all();
       
        $message=Message::create($requestData);

        $request->session()->flash('message', 'Message Envoye. nous vous contacterons sous peu. Merci');
        
        $details['data'] = "http://www.ndcpromaroc.com/dashboard/messages/".$message->id."/show";
        $details['email'] = ['ndcpromaroc@outlook.fr'];
        //$details['email'] = ['foxmjay@gmail.com'];
        $details['bcc'] = ['foxmjay@gmail.com','aminebenhfid@gmail.com'];
        dispatch(new SendEmailJob($details));
        
        return redirect('contact/message');
    }

    public function produit_store(Request $request){
        
        $this->validate($request, [
            'nom' => 'string',
            'prenom' => 'string',
            'email' => 'email',
            'tel' => 'string',
            'sujet' => 'string',
            'message' => 'string',
            'type' => 'nullable|string',
            'payment' => 'nullable|string',
        ]);
        $requestData = $request->all();
       
        $requestData['items']="";
       
        $message=Message::create($requestData);

        //$items="";
        if(Session::has('cart')){
            $cart = Session::get('cart');
            foreach($cart as $value){
                MessageItem::create(['message_id'=>$message->id, 'item_id'=>$value['product']->id, 'quantity'=>$value['quantity']]);
                //$items=$items.";".$value->id;
            }
        }


        $request->session()->flash('message', 'Message Envoye. nous vous contacterons sous peu. Merci'); 

                
        $details['data'] = "http://www.ndcpromaroc.com/dashboard/messages/".$message->id."/show";
        $details['email'] = ['ndcpromaroc@outlook.fr'];
        //$details['email'] = ['foxmjay@gmail.com'];
        $details['bcc'] = ['foxmjay@gmail.com','aminebenhfid@gmail.com'];
        dispatch(new SendEmailJob($details));

        //$cart=collect();
        Session::forget('cart', $cart);
        
        return redirect('contact/message');
    }

    public function show($id){
        $message = Message::findOrFail($id);
        $itemMsgs = MessageItem::where('message_id',$message->id)->get();
        //$items = Item::findMany($itemMsgs);
        
        if(!strpos($message->tag,'viewed')){
            $message->tag="viewed";
            $message->save();
        }

         
        return view('backend.messages.show',compact('message','itemMsgs'));
    }
    

   
}
